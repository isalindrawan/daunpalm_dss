<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>
		<?php echo $title ?>
	</title>
	<!-- Favicon-->
	<link rel="icon" href="<?php echo base_url()?>assets/favicon.ico" type="image/x-icon">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet">
	<!-- Animation Css -->
	<link href="<?php echo base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php echo base_url()?>assets/css/themes/all-themes.css" rel="stylesheet">

	<!-- Sweet Alert CSS -->
	<link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">
	<!-- JQuery DataTable Css -->
	<link href="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
	<!-- Bootstrap Material Datetime Picker Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
	 rel="stylesheet">
	<!-- Bootstrap Select Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

	<!-- Morris Css -->
	<link href="<?php echo base_url()?>assets/plugins/morrisjs/morris.css" rel="stylesheet" />

	<?php if (strpos($_SERVER['REQUEST_URI'], 'transaction')): ?>
	<!-- My Cashier CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/myStyle.css">
	<?php endif ?>

	<style>
		@media all {
			.page-break {
				display: none;
			}
		}

		@media print {
			.page-break {
				display: block;
				page-break-before: always;
			}
		}

	</style>
</head>

<body class="theme-blue-grey">
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #Overlay For Sidebars -->
	<!-- Top Bar -->
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
				 aria-expanded="false"></a>
				<a href="javascript:void(0);" class="bars"></a>
				<a class="navbar-brand" href="<?php echo base_url()?>"><b>DAUN PALM</b> | Food and Beverages</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
				
					<li><a href="<?php echo base_url('cashier')?>"><i class="material-icons">store</i></a></li>
					<li><a href="<?php echo base_url('logout')?>"><i class="material-icons">exit_to_app</i></a></li>
					<!-- User Menu -->
					<!-- <li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
							<i class="material-icons">account_circle</i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
							<li role="seperator" class="divider"></li>
							<li><a href="<?php echo base_url('logout')?>"><i class="material-icons">input</i>Sign Out</a></li>
						</ul>
					</li> -->
					<!-- #User Menu -->
				</ul>
			</div>
		</div>
	</nav>
	<!-- #Top Bar -->
	<section>
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- SIDE MENU -->
			<div class="menu">
				<ul class="list">
					<!-- MENU HEADER / SEPARATOR -->
					<?php if ($this->session->userdata('user_type') === "Admin"):?>
					<li class="header">MAIN</li>
					<!-- DASHBOARD -->
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'dashboard')) echo 'active';?>">
						<a href="<?php echo base_url('dashboard')?>">
							<i class="material-icons">dashboard</i>
							<span>Dashboard</span>
						</a>
					</li>
					<?php endif; ?>
					<!-- MANAGE -->
					<li class="header">MANAGEMENT</li>
					<?php if ($this->session->userdata('user_type') === "Admin"):?>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'employee')) echo 'active';?>">
						<a href="<?php echo base_url('employee')?>">
							<i class="material-icons">people</i><span>Employee</span>
						</a>
					</li>
					<?php endif; ?>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'menu') || strpos($_SERVER['REQUEST_URI'], 'sold') || strpos($_SERVER['REQUEST_URI'], 'category')) echo 'active';?>">
						<a href="javascript:void(0)" class="menu-toggle">
							<i class="material-icons">restaurant_menu</i>
							<span>Menu</span>
						</a>
						<ul class="ml-menu">
							<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'menu')) echo 'active';?>">
								<a href="<?php echo base_url('menu')?>">
									<span>List</span>
								</a>
							</li>
							<?php if ($this->session->userdata('user_type') === "Admin"):?>
							<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'category')) echo 'active';?>">
								<a href="<?php echo base_url('category')?>">
									<span>Category</span>
								</a>
							</li>
							<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'sold')) echo 'active';?>">
								<a href="<?php echo base_url('sold')?>">
									<span>Sales</span>
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</li>
					<?php if ($this->session->userdata('user_type') === "Admin"):?>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'report')) echo 'active';?>">
						<a href="<?php echo base_url('report')?>">
							<i class="material-icons">assignment</i>
							<span>Report</span>
						</a>
					</li>
					<?php endif; ?>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'reservation')) echo 'active';?>">
						<a href="<?php echo base_url('reservation')?>">
							<i class="material-icons">event</i>
							<span>Reservation</span>
						</a>
					</li>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'transaction') || strpos($_SERVER['REQUEST_URI'], 'transaction/sales') || strpos($_SERVER['REQUEST_URI'], 'transaction/purchases')) echo 'active';?>">
						<a href="javascript:void(0)" class="menu-toggle">
							<i class="material-icons">local_atm</i>
							<span>Transaction</span>
						</a>
						<ul class="ml-menu">

							
							<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'sales')) echo 'active';?>">
								<a href="<?php echo base_url('transaction/sales')?>">
									<span>Sales</span>
								</a>
							</li>

							<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'purchases')) echo 'active';?>">
								<a href="<?php echo base_url('transaction/purchases')?>">
									<span>Purchases</span>
								</a>
							</li>
						</ul>
					</li>

					<!-- SYSTEM -->

					<?php if ($this->session->userdata('user_type') === "Admin"):?>
					<li class="header">SYSTEM</li>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'activity')) echo 'active';?>">
						<a href="<?php echo base_url('activity')?>">
							<i class="material-icons">history</i>
							<span>Activity Log</span>
						</a>
					</li>
					<li class="<?php if (strpos($_SERVER['REQUEST_URI'], 'account')) echo 'active';?>">
						<a href="<?php echo base_url('account')?>">
							<i class="material-icons">account_circle</i>
							<span>User</span>
						</a>
					</li>
					<?php endif; ?>
				</ul>
			</div>
		</aside>
		<!-- #Left Sidebar -->
	</section>
