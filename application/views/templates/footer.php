        
        <!-- My Purchases JS -->
        <?php if (strpos($_SERVER['REQUEST_URI'], 'transaction')):?>
        <script src="<?php echo base_url()?>assets/js/myPurchases.js"></script>
        
        <?php endif; ?>

        <!-- Chart Plugins Js -->
        <script src="<?php echo base_url()?>assets/plugins/chartjs/Chart.bundle.js"></script>
        
        <!-- Bootstrap Core Js -->
        <script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/node-waves/waves.js"></script>

        <!-- Sweet Alert Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>

        <!-- Jquery Validation Plugin Css -->
        <script src="<?php echo base_url()?>assets/plugins/jquery-validation/jquery.validate.js"></script>

        <!-- JQuery Steps Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/jquery-steps/jquery.steps.js"></script>

        <!-- Custom Js -->
        <script src="<?php echo base_url()?>assets/js/pages/forms/form-validation.js"></script>

        <!-- My Form Element Js -->
        <script src="<?php echo base_url()?>assets/js/myFormElement.js"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

        <!-- Select Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Input Mask Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

        <!-- Autosize Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/autosize/autosize.js"></script>

        <!-- Moment Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/momentjs/moment.js"></script>

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Chart JS -->
        <script src="<?php echo base_url()?>assets/plugins/chartjs/Chart.bundle.js"></script>

        <!-- Morris Plugin Js -->
        <script src="<?php echo base_url()?>assets/plugins/raphael/raphael.min.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/morrisjs/morris.js"></script>
        
        <!-- Custom Js -->
        <script src="<?php echo base_url()?>assets/js/admin.js"></script>
        
    </body>
</html>
