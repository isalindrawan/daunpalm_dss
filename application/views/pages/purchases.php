<?php if ($this->session->flashdata('create')):?>

<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {
        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>

<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {
        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">
        
        <?php if ($this->session->userdata('user_type') === "User"): ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW TRANSACTION
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <form id="form_advanced_validation" action="<?php echo base_url('transaction/post_create')?>" name="formdata" method="post">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Total</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="total" type="text" name="total" class="form-control" required readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Paid</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="paid" type="text" name="paid" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Change</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="change" type="text" name="change" class="form-control" required readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button id="submit-button" class="btn bg-pink waves-effect" type="submit" id="submit" disabled>SUBMIT</button>
                                        <a id="clear-button" class="btn bg-grey waves-effect" disabled>DISCARD</a>

                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Item</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="item" type="text" name="item" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Quantity</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="quantity" type="text" name="quantity" class="form-control" placeholder="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Price</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="price" type="text" name="price" class="form-control" placeholder="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <table id="purchases-list" class="span12 table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="align-left" style="width: 10%">Action</th>
                                                    <th class="align-left" style="width: 40%">Item</th>
                                                    <th class="align-left" style="width: 10%">Price</th>
                                                    <th class="align-left" style="width: 20%">Quantity</th>
                                                    <th class="align-left" style="width: 20%">Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>PURCHASES HISTORY
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="transactiontable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Total</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif ?>
        <?php if ($this->session->userdata('user_type') === "Admin"): ?>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-blue">
                        <div class="panel-heading" role="tab" id="chart-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#chart" aria-expanded="true" aria-controls="chart">
                                    <i class="material-icons">insert_chart</i> MONTHLY SPENDING
                                </a>
                            </h4>
                        </div>
                        <div id="chart" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="chart-heading">
                            <div class="panel-body">
                                <canvas id="monthly_chart" height="120"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel-group" id="accordion_9" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-blue">
                        <div class="panel-heading" role="tab" id="headingOne_9">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseOne_9" aria-expanded="true" aria-controls="collapseOne_9">
                                    <i class="material-icons">bookmark</i> SPENDING SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne_9" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_9">
                            <div class="panel-body">
                                <span>TODAY</span>
                                <span class="pull-right">Rp. <b><?php echo $today_spending?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right">Rp. <b><?php echo $yesterday_spending?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right">Rp. <b><?php echo $last_3_spending?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right">Rp. <b><?php echo $last_week_spending?></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue">
                        <div class="panel-heading" role="tab" id="headingTwo_9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseTwo_9" aria-expanded="false"
                                   aria-controls="collapseTwo_9">
                                    <i class="material-icons">bookmark</i> TRANSACTION SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_9">
                            <div class="panel-body">
                                <span>TODAY</span>
                                <span class="pull-right"><b><?php echo $today_transaction?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right"><b><?php echo $yesterday_transaction?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_3_transaction?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_week_transaction?></b></span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="panel panel-col-teal">
                        <div class="panel-heading" role="tab" id="headingThree_9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseThree_9" aria-expanded="false"
                                   aria-controls="collapseThree_9">
                                    <i class="material-icons">bookmark</i> SALES SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_9">
                            <div class="panel-body">

                                <span>TODAY</span>
                                <span class="pull-right"><b><?php echo $today_menu_sales?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right"><b><?php echo $yesterday_menu_sales?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_3_menu_sales?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_week_menu_sales?></b></span>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">

                <div class="row clearfix">  
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">        
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-orange">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">SPENDING</div>
                                <div class="number"><?php echo $spending?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-teal">
                                <i class="material-icons">receipt</i>
                            </div>
                            <div class="content">
                                <div class="text">TRANSACTION</div>
                                <div class="number"><?php echo $transaction?></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-pink">
                                <i class="material-icons">event_busy</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>CANCELLED</b></div>
                                <div class="number"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-blue-grey">
                                <i class="material-icons">money_off</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>SALARY TO PAY</b></div>
                                <div class="number"></h5></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW TRANSACTION
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <form id="form_advanced_validation" action="<?php echo base_url('transaction/post_create')?>" name="formdata" method="post">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Total</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="total" type="text" name="total" class="form-control" required readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Paid</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="paid" type="text" name="paid" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <label class="form-label">Change</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="change" type="text" name="change" class="form-control" required readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button id="submit-button" class="btn bg-pink waves-effect" type="submit" id="submit" disabled>SUBMIT</button>
                                        <a id="clear-button" class="btn bg-grey waves-effect" disabled>DISCARD</a>

                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Item</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="item" type="text" name="item" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Quantity</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="quantity" type="text" name="quantity" class="form-control" placeholder="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <label class="form-label">Price</label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input id="price" type="text" name="price" class="form-control" placeholder="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <table id="purchases-list" class="span12 table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="align-left" style="width: 10%">Action</th>
                                                    <th class="align-left" style="width: 40%">Item</th>
                                                    <th class="align-left" style="width: 10%">Price</th>
                                                    <th class="align-left" style="width: 20%">Quantity</th>
                                                    <th class="align-left" style="width: 20%">Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>PURCHASES HISTORY
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="transactiontable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Total</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>

<?php if ($this->session->userdata('user_type') === "Admin"): ?>
<script src="<?php echo base_url()?>assets/js/mySpending.js"></script>
<?php endif; ?>

<script type="text/javascript">
$(function() {
    $('#transactiontable').DataTable({
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [0, 'desc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/transaction_api/get_json_data_purchases')?>",
            "type": "POST"
        },
        "columnDefs": [{
            "targets": 4,
            "className": "text-center",
            "orderable": false
        }]
    });
});
</script>