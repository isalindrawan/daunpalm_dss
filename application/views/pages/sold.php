<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="dailysales-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#dailysales" aria-expanded="false" aria-controls="dailysales">
                                <i class="material-icons">assignment</i> DAILY SALES
                            </a>
                            
                        </div>
                        <div id="dailysales" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="dailysales-heading">
                            <div class="panel-body">
                                
                                <!-- <div class="col-lg-4">
                                    <canvas id="bar_chart" height="autosize"></canvas>
                                </div> -->
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                        <canvas id="daily_chart" height="80"></canvas>
                                    </div>
                                </div>

                                <br> <br>

                                <div class="row clearfix">

                                    <div class="col-lg-3">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-blue">
                                                        <i class="material-icons">receipt</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">SALES MADE</div>
                                                        <div class="number"><?php echo $total_daily_transaction?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-light-green">
                                                        <i class="material-icons">local_atm</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">INCOME RP.</div>
                                                        <div class="number"><?php echo $total_daily_income?></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-pink">
                                                        <i class="material-icons">restaurant_menu</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">MENU SOLD</div>
                                                        <div class="number"><?php echo $total_daily_menu?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-deep-orange">
                                                        <i class="material-icons">trending_up</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">TOTAL SOLD</div>
                                                        <div class="number"><?php echo $total_daily?></div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="daily">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Sold</th>
                                                        <th>Total</th>
                                                        <th>Last Date</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Sold</th>
                                                    <th>Total</th>
                                                    <th>Last Date</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="monthlysales-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#monthlysales" aria-expanded="true" aria-controls="monthlysales">
                                <i class="material-icons">assignment</i>MONTHLY SALES
                            </a>
                            
                        </div>
                        <div id="monthlysales" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="monthlysales-heading">
                            <div class="panel-body">

                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                        <canvas id="monthly_chart" height="80"></canvas>
                                    </div>
                                </div>

                                <br> <br>

                                <div class="row clearfix">

                                    <div class="col-lg-3">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-blue">
                                                        <i class="material-icons">receipt</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">SALES MADE</div>
                                                        <div class="number"><?php echo $total_monthly_transaction?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-light-green">
                                                        <i class="material-icons">local_atm</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">INCOME RP.</div>
                                                        <div class="number"><?php echo $total_monthly_income?></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-pink">
                                                        <i class="material-icons">restaurant_menu</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">MENU SOLD</div>
                                                        <div class="number"><?php echo $total_monthly_menu?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-deep-orange">
                                                        <i class="material-icons">trending_up</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">TOTAL SOLD</div>
                                                        <div class="number"><?php echo $total_monthly?></div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="monthly">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Sold</th>
                                                        <th>Total</th>
                                                        <th>Last Date</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Sold</th>
                                                    <th>Total</th>
                                                    <th>Last Date</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-col-blue">
                        <div class="panel-heading" role="tab" id="allsales-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#allsales" aria-expanded="true" aria-controls="allsales">
                                <i class="material-icons">assignment</i>ALL SALES
                            </a>
                            
                        </div>
                        <div id="allsales" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="allsales-heading">
                            <div class="panel-body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
                                        <canvas id="all_chart" height="80"></canvas>
                                    </div>
                                </div>

                                <br> <br>

                                <div class="row clearfix">

                                    <div class="col-lg-3">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-blue">
                                                        <i class="material-icons">receipt</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">SALES MADE</div>
                                                        <div class="number"><?php echo $total_transaction?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-light-green">
                                                        <i class="material-icons">local_atm</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">INCOME RP.</div>
                                                        <div class="number"><?php echo $total_income?></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-pink">
                                                        <i class="material-icons">restaurant_menu</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">MENU SOLD</div>
                                                        <div class="number"><?php echo $total_menu?></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="info-box hover-expand-effect">
                                                    <div class="icon bg-deep-orange">
                                                        <i class="material-icons">trending_up</i>
                                                    </div>
                                                    <div class="content">
                                                        <div class="text">TOTAL SOLD</div>
                                                        <div class="number"><?php echo $total?></div>
                                                    </div>
                                                </div>
                                            </div>    
                                        </div>
                                    </div>

                                    <div class="col-lg-9">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="all">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Sold</th>
                                                        <th>Total</th>
                                                        <th>Last Date</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Sold</th>
                                                    <th>Total</th>
                                                    <th>Last Date</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php echo base_url()?>assets/js/mySalesChart.js" type="text/javascript"></script>

<script type="text/javascript">
$(function() {

    $('#all').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [4, 'desc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/sold_api/get_json_data/all')?>",
            "type": "POST"
        }
    });

    $('#monthly').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [4, 'desc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/sold_api/get_json_data/monthly')?>",
            "type": "POST"
        }
    });

    $('#daily').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [4, 'desc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/sold_api/get_json_data/daily')?>",
            "type": "POST"
        }
    });
});

</script>