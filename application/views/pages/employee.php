<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('delete')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({

            title: "Confirm",
            text: "Are You Sure ? You will not be able to recover this data.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {

                var nama = "<?php echo $this->session->flashdata('nama')?>";
                var alamat = "<?php echo $this->session->flashdata('alamat')?>";
                var pegawai = "<?php echo $this->session->flashdata('pegawai')?>";

                window.location.href = "<?php echo base_url('employee/delete')?>" + "/" + nama + "/" + alamat + "/" + pegawai + "/confirmed";

            } else {

                swal({

                    title: "Cancelled",
                    text: "Operation aborted",
                    type: "error"

                }, function isConfirm() {

                    if (isConfirm) {
                        window.location.href = "<?php echo base_url('employee')?>";
                    }
                });
            }
        });
    });

</script>

<?php elseif ($this->session->flashdata('deleted')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been deleted.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>


<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-pink">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>EMPLOYEE</b></div>
                        <div class="number"><?php echo $total?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-teal">
                        <i class="material-icons">check</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>ACTIVE</b></div>
                        <div class="number"><?php echo $active?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-orange">
                        <i class="material-icons">close</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>DEACTIVE</b></div>
                        <div class="number"><?php echo $deactive?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-blue-grey">
                        <i class="material-icons">money_off</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>SALARY TO PAY</b></div>
                        <div class="number"><h5>Rp. <?php echo $salary['salary']?></h5></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW EMPLOYEE
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <form id="form_advanced_validation" action=" <?php echo base_url('employee/create')?> " method="post">
                            
                                    <input type="hidden" name="employee_id" value="<?php echo ++$employee?>" class="form-control">
                                    <input type="hidden" name="name_id" value="<?php echo ++$name?>" class="form-control">
                                    <input type="hidden" name="address_id" value="<?php echo ++$address?>" class="form-control">
                                    
                                        <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <label for="name">NAME</label>
                                                <br><br>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" name="first_name" class="form-control" placeholder="First Name"  name="minmaxlength" minlength="5" maxlength="25" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" name="minmaxlength" minlength="5" maxlength="25">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row clearfix">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <label for="address">ADDRESS</label>
                                                                <br><br>
                                                                <input type="text" name="address" class="form-control" placeholder="Street" name="minmaxlength" minlength="10" maxlength="50" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name="city" class="form-control" placeholder="City" name="minmaxlength" minlength="4" maxlength="25" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="text" name="stateprovince" class="form-control" placeholder="State/Province" name="minmaxlength" minlength="5" maxlength="30" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    
                                            <br>

                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <label for="details">DETAILS</label>
                                                <br><br>
                                                
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">phone_iphone</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" name="contact" class="form-control" placeholder="Contact" name="minmaxlength" minlength="11" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">work</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" name="position" class="form-control" placeholder="Position" name="minmaxlength" minlength="5" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">attach_money</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" name="salary" class="form-control" placeholder="Salary" name="minmaxvalue" min="100000" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label for="since">DATE</label>
                                                <div class="row clearfix">
                                                    <br>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text"  name="start" class="datepicker form-control" placeholder="Start Date" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    
                                    <button class="btn bg-pink waves-effect" type="submit">SUBMIT</button>
                                    <a id="reset" class="btn bg-grey waves-effect">DISCARD</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>EMPLOYEE DATA
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="pegawaitable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Contact</th>
                                                <th>Position</th>
                                                <th>Salary</th>
                                                <th>Start</th>
                                                <th>End</th>
                                                <th class="align-center">Status</th>
                                                <!-- <th class="align-center">Status</th> -->
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Contact</th>
                                            <th>Position</th>
                                            <th>Salary</th>
                                            <th>Start</th>
                                            <th>End</th>
                                            <th class="align-center">Status</th>
                                            <!-- <th class="align-center">Status</th> -->
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script type="text/javascript">
    $(function() {

        $('#pegawaitable').DataTable({

            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'asc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/employee_api/get_json_data')?>",
                "type": "POST"
            },

            "columnDefs": [

                {
                    "targets": 7,
                    "className": "text-center"
                },

                {
                    "targets": 8,
                    "className": "text-center",
                    "orderable": false
                }
            ]
        });

        $('#reset').on('click', function() {

            $('input[name=first_name]').val("");
            $('input[name=first_name]').attr("placeholder", "First Name");
            $('input[name=last_name]').val("");
            $('input[name=last_name]').attr("placeholder", "Last Name");
            $('input[name=address]').val("");
            $('input[name=address]').attr("placeholder", "Street");
            $('input[name=city]').val("");
            $('input[name=city]').attr("placeholder", "City");
            $('input[name=stateprovince]').val("");
            $('input[name=stateprovince]').attr("placeholder", "State/Province");
            $('input[name=contact]').val("");
            $('input[name=contact]').attr("placeholder", "Contact");
            $('input[name=position]').val("");
            $('input[name=position]').attr("placeholder", "Position");
            $('input[name=salary]').val("");
            $('input[name=salary]').attr("placeholder", "Salary");
            $('input[name=start]').val("");
            $('input[name=start]').attr("placeholder", "Start Date");

        });

    });
</script>