<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="header bg-blue-grey">
                <h2>INVOICE</h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo base_url('transaction/transsales')?>">BACK</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <p>ID : <?php echo $detail['ID_TJ'] ?></p>
                        <p>Date : <?php echo $detail['Waktu'] ?></p>
                        <p>Server : <?php echo $detail['Nama_Depan'] ?></p>
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
                    <div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12">
                        <p>Table : <?php echo $detail['Meja'] ?></p>
                        <p>Customer : <?php echo $detail['Customer'] ?></p>
                    </div>
                </div>
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <td><b>Item</b></td>
                            <td><b>Price</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Subtotal</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($list as $row):?>
                        <tr>
                            <td><?php echo $row['Nama']?></td>
                            <td><?php echo $row['Harga']?></td>
                            <td><?php echo $row['Quantity']?></td>
                            <td><?php echo $row['Subtotal']?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
                
                <br>
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs"></div>
                    
                    <div class="col-lg-6 col-md-6 hidden-sm hidden-xs"></div>
                    <div class="col-lg-2 col-lg-2 col-sm-12 col-xs-12">
                        <p><b>Total : </b><?php echo $detail['Total'] ?></p>
                        <p><b>Paid : </b><?php echo $detail['Bayar'] ?></p>
                        <p><b>Changes : </b><?php echo $detail['Kembali'] ?></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>