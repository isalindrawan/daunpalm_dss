<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('delete')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({

            title: "Confirm",
            text: "Are You Sure ? You will not be able to recover this data.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {
                var id = "<?php echo $this->session->flashdata('id')?>";

                window.location.href = "<?php echo base_url('menu/delete')?>" + "/" + id + "/confirmed";

            } else {

                swal({

                    title: "Cancelled",
                    text: "Operation aborted",
                    type: "error"

                }, function isConfirm() {

                    if (isConfirm) {
                        window.location.href = "<?php echo base_url('menu')?>";
                    }
                });
            }
        });
    });
</script>

<?php elseif ($this->session->flashdata('deleted')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success!",
            text: "Data has been deleted.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-blue">
                        <i class="material-icons">restaurant_menu</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>TOTAL MENU</b></div>
                        <div class="number"><?php echo $total?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-pink">
                        <i class="material-icons">favorite</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>BEST MONTHLY</b></div>
                        <div class="text"><h5><?php echo $best['Nama']?></h5></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-green">
                        <i class="material-icons">check</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>AVAILABLE</b></div>
                        <div class="number"><?php echo $available?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-deep-orange">
                        <i class="material-icons">close</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>UNAVAILABLE</b></div>
                        <div class="number"><?php echo $unavailable?></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW MENU
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <form id="form_advanced_validation" action=" <?php echo base_url('menu/create')?> " method="post">
                                    
                                    <input type="hidden" name="create_id" value="<?php echo ++$id?>" class="form-control" placeholder="">
                                    <input type="hidden" name="create_status" value="Unavailable" class="form-control" placeholder="">
                                    <label for="name">Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="create_name" class="form-control" placeholder="" name="minmaxlength" minlength="5" maxlength="20" required>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                        <div class="col-md-6">
                                            <label for="price">Price</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="create_price" class="form-control" placeholder="" name="minmaxvalue" min="5000" max="999000" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="category">Category</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <select name="create_category"  class="form-control show-tick">
                                                        <?php foreach ($category as $row) {
                                                        
                                                        echo '<option value="'.$row['ID_Kategori'].'">'.$row['Nama_Kategori'].'</option>';
                                                        }?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <br>
                                    
                                    <button class="btn bg-pink waves-effect" type="submit" value="execute">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>MENU DATA
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="menutable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th class="align-center">Status</th>
                                                <?php if ($this->session->userdata('user_type') === "Admin"):?>
                                                <th class="align-center">Action</th>
                                                <?php endif;?>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Price</th>
                                            <th class="align-center">Status</th>
                                            <?php if ($this->session->userdata('user_type') === "Admin"):?>
                                            <th class="align-center">Action</th>
                                            <?php endif;?>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>
<script type="text/javascript">
$(function() {
    $('#menutable').DataTable({
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [0, 'asc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/menu_api/get_json_data')?>",
            "type": "POST"
        },
        "columnDefs": [{
            "targets": 4,
            "className": "text-center",
        }, 

        <?php if ($this->session->userdata('user_type') === "Admin"):?>
        {
            "targets": 5,
            "className": "text-center",
            "orderable": false
        }

        <?php endif;?>
        
        ]
    })
});
</script>