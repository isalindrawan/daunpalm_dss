<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('deleted')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success!",
            text: "Data has been deleted.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->
        <ol class="breadcrumb align-right">
            <li>
                <a href="<?php echo base_url()?>">
                    <i class="material-icons">home</i>Home
                </a>
            </li>
            <li>
                <a href="<?php echo base_url('menu')?>">
                    <i class="material-icons">restaurant_menu</i>Menu
                </a>
            </li>
            <li>
                <a href="javascript:void(0)">
                    <i class="material-icons">kitchen</i>Ingredients
                </a>
            </li>
        </ol>
        <div class="row clearfix">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">assignment</i> CURRENT INGREDIENTS
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <table id="current-table" class="span12 table table-responsive">
                                    <thead>
                                        <tr>
                                            <th class="align-center" style="width: 10%">Action</th>
                                            <th class="align-left" style="width: 90%">Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($ingredient as $row):?>
                                        <tr id="<?php echo $row['ID_Inventory']?>">
                                            <td class="align-center">
                                                <a class="btn btn-xs bg-blue-grey waves-effect" href="<?php echo base_url('menu/delete_ingredients').'/'.$row['ID_Bahan'].'/'.$row['FO_ID_Menu']?>" >
                                                    <i class="material-icons">clear</i>
                                                </a>
                                            </td>
                                            <td><?php echo $row['Item']?></td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                                <a class="btn bg-pink waves-effect" href="<?php echo base_url('menu')?>">DONE</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">add</i> ADD TO INGREDIENTS
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <form action="<?php echo base_url('menu/update_ingredients').'/'.$menu['ID_Menu']?>" method="post" ">
                                    <input type="hidden" name="men_id" value="<?php echo $menu['ID_Menu'] ?>">
                                    <table id="ingredients-table" class="span12 table table-responsive">
                                        <thead>
                                            <tr>
                                                <th class="align-center" style="width: 10%">Action</th>
                                                <th class="align-left" style="width: 30%">ID</th>
                                                <th class="align-left" style="width: 60%">Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <button class="btn bg-pink waves-effect" type="submit" id="submit">SUBMIT</button>
                                    <a id="clear" class="btn bg-grey waves-effect">CLEAR</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue-grey">
                        <h2>
                        INVENTORY
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="inventory-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#inventory-table').DataTable({

            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'desc']
            ],

            "ajax": {

                "url": "<?php echo base_url('datatable_api/inventory_api/get_json_data_name')?>",
                "type": "POST"
            },

            "columnDefs": [

                {
                    "targets": 2,
                    "className": "text-center"
                },

                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<button class="btn bg-pink btn-xs waves-effect"><i class="material-icons">add</i></button>',
                    "className": "text-center"
                }
            ]
        });

        $('#ingredients-table').on('click', '.delete', function() {

            $(this).closest('tr').remove();
        });

        $('#clear').on('click', function() {

            $("#ingredients-table tbody tr").remove();
        });

        $('#inventory-table tbody').on('click', 'button', function() {

            var data = table.row($(this).parents('tr')).data();

            if (document.getElementById("current-table").rows.namedItem(data[0])) {

                swal({
                    title: "",
                    text: "Ingredients already added.",
                    type: "warning",
                    timer: 1000,
                    showConfirmButton: false
                });

            } else {

                $('#ingredients-table tbody').append(

                    '<tr><td class="align-center"><a class="btn btn-xs bg-pink waves-effect delete"><i class="material-icons">clear</i></a></td><td><input type="hidden" name="in_id[]" value="' + data[0] + '">' + data[0] + '</td><td><input type="hidden" name="in_name[]" value="' + data[1] + '">' + data[1] + '</td></tr>'
                );
            }
        });
    });
</script>