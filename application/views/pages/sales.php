<section class="content">
    <div class="container-fluid">
        
        <?php if ($this->session->userdata('user_type') === "Admin"):?>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="chart-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#chart" aria-expanded="true" aria-controls="chart">
                                    <i class="material-icons">insert_chart</i> MONTHLY INCOME
                                </a>
                            </h4>
                        </div>
                        <div id="chart" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="chart-heading">
                            <div class="panel-body">
                                <canvas id="monthly_chart" height="142"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="panel-group" id="accordion_9" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-teal">
                        <div class="panel-heading" role="tab" id="headingOne_9">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseOne_9" aria-expanded="true" aria-controls="collapseOne_9">
                                    <i class="material-icons">bookmark</i> INCOME SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne_9" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_9">
                            <div class="panel-body">
                                <span>TODAY</span>
                                <span class="pull-right">Rp. <b><?php echo $today_income?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right">Rp. <b><?php echo $yesterday_income?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right">Rp. <b><?php echo $last_3_income?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right">Rp. <b><?php echo $last_week_income?></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-teal">
                        <div class="panel-heading" role="tab" id="headingTwo_9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseTwo_9" aria-expanded="false"
                                   aria-controls="collapseTwo_9">
                                    <i class="material-icons">bookmark</i> TRANSACTION SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_9">
                            <div class="panel-body">
                                <span>TODAY</span>
                                <span class="pull-right"><b><?php echo $today_transaction?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right"><b><?php echo $yesterday_transaction?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_3_transaction?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_week_transaction?></b></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-teal">
                        <div class="panel-heading" role="tab" id="headingThree_9">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseThree_9" aria-expanded="false"
                                   aria-controls="collapseThree_9">
                                    <i class="material-icons">bookmark</i> SALES SUMMARY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_9">
                            <div class="panel-body">

                                <span>TODAY</span>
                                <span class="pull-right"><b><?php echo $today_menu_sales?></b></span>

                                <hr>

                                <span>YESTERDAY</span>
                                <span class="pull-right"><b><?php echo $yesterday_menu_sales?></b></span>

                                <hr>

                                <span>LAST 3 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_3_menu_sales?></b></span>

                                <hr>

                                <span>LAST 7 DAYS</span>
                                <span class="pull-right"><b><?php echo $last_week_menu_sales?></b></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="row clearfix">

            <?php if ($this->session->userdata('user_type') === "Admin"):?>
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">

                <div class="row clearfix">  
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">        
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-orange">
                                <i class="material-icons">attach_money</i>
                            </div>
                            <div class="content">
                                <div class="text">INCOME</div>
                                <div class="number"><?php echo $income?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-teal">
                                <i class="material-icons">receipt</i>
                            </div>
                            <div class="content">
                                <div class="text">TRANSACTION</div>
                                <div class="number"><?php echo $transaction?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-pink">
                                <i class="material-icons">restaurant_menu</i>
                            </div>
                            <div class="content">
                                <div class="text">MENU SOLD</div>
                                <div class="number"><?php echo $menu_sales?></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-blue-grey">
                                <i class="material-icons">money_off</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>SALARY TO PAY</b></div>
                                <div class="number"></h5></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <?php endif; ?>

            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                    <i class="material-icons">assignment</i> SALES DATA
                                </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="transactiontable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Customer</th>
                                                <th>Date</th>
                                                <th>Total</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Customer</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script src="<?php echo base_url()?>assets/js/myIncome.js"></script>

<script type="text/javascript">
    $(function() {
        $('#transactiontable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'desc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/transaction_api/get_json_data_sales')?>",
                "type": "POST"
            },

            "columnDefs": [{
                "targets": 5,
                "className": "text-center",
                "orderable": false
            }]
        });
    });
</script>