<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue-grey">
                        <h2>
                        RESERVATION
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <form id="form_advanced_validation" action="<?php echo base_url('reservation/post_update') ?>" method="post">
                                    
                                    <input type="hidden" name="id" value="<?php echo $reservation['ID_Reservasi']?>" class="form-control">

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="<?php echo $reservation['Nama']?>" value="<?php echo $reservation['Nama']?>" name="minmaxlength" maxlength="30" readonly>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="res_date" class="datetimepicker form-control" placeholder="<?php echo $reservation['Tgl_Res']?>" value="<?php echo $reservation['Tgl_Res']?>" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">schedule</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="due_date" class="datetimepicker form-control" placeholder="<?php echo $reservation['Tgl_Kunjungan']?>" value="<?php echo $reservation['Tgl_Kunjungan']?>" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">group</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="persons" class="form-control" placeholder="<?php echo $reservation['Jumlah']?>" value="<?php echo $reservation['Jumlah']?>" name="minmaxvalue" min="1" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event_note</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea name="notes" rows="4" class="form-control no-resize"><?php echo $reservation['Catatan']?></textarea>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">info_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <select name="status" class="form-control show-tick">
                                                <?php if ($reservation['Status'] === "Upcoming"):?>
                                                <option value="<?php echo $reservation['Status']?>"><?php echo $reservation['Status']?></option>
                                                <option value="Cancelled">Cancelled</option>
                                                <option value="Finished">Finished</option>
                                                <?php elseif($reservation['Status'] === "Cancelled"):?>
                                                <option value="<?php echo $reservation['Status']?>"><?php echo $reservation['Status']?></option>
                                                <option value="Upcoming">Upcoming</option>
                                                <option value="Finished">Finished</option>
                                                <?php elseif($reservation['Status'] === "Finished"):?>
                                                <option value="<?php echo $reservation['Status']?>"><?php echo $reservation['Status']?></option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <button class="btn bg-pink waves-effect" type="submit" value="submit">UPDATE</button>
                                    <a onclick="discarding()" class="btn bg-grey waves-effect">CANCEL</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" charset="utf-8" async defer>

    function discarding() {

        swal({

            title: "Are you sure ?",
            text: "Changes that has been made will not be save.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, discard it",
            cancelButtonText: "No, continue editing",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {

                window.location.href = "<?php echo base_url('reservation')?>";

            } else {

                swal.close()
            }
        });
    }
</script>