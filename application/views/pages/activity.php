<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="card">
                <div class="header bg-blue-grey">
                    <h2>
                    ACTIVITY LOG
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="activitytable">
                            <thead>
                                <tr>
                                    <th>Activity ID</th>
                                    <th>Username</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Detail</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Activity ID</th>
                                    <th>Username</th>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Detail</th>
                                    <th>Status</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>
<script type="text/javascript">
$(function() {
    $('#activitytable').DataTable({
        "autoWidth": true,
        "processing": true,
        "serverSide": true,
        "order": [
            [2, 'desc']
        ],
        "ajax": {
            "url": "<?php echo base_url('datatable_api/activity_api/get_json_data')?>",
            "type": "POST"
        }
    })
});
</script>