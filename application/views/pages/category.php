<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue-grey">
                        <h2>
                        NEW CATEGORY
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <form id="form_advanced_validation" action=" <?php echo base_url('category/create')?> " method="post">
                                    
                                    <input type="hidden" name="id" value="<?php echo ++$id?>" class="form-control">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Category Name" name="minmaxlength" minlength="4" maxlength="30" required>
                                        </div>
                                    </div>
                                    <button class="btn bg-pink waves-effect" type="submit" value="submit">ADD</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue-grey">
                        <h2>
                        CATEGORY DATA
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="categorytable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Total</th>
                                        <th class="align-center">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Total</th>
                                    <th class="align-center">Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script type="text/javascript">
    $(function() {
        $('#categorytable').DataTable({
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'asc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/category_api/get_json_data')?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": 3,
                "className": "text-center",
                "orderable": false
            }]
        })
    });
</script>