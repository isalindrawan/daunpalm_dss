<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-pink">
                        <i class="material-icons">shopping_cart</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>TOTAL ITEM</b></div>
                        <div class="number"><?php echo $total?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-green">
                        <i class="material-icons">restore</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>LAST UPDATE</b></div>
                        <div class="text"><h4><?php echo $last?></h4></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-yellow">
                        <i class="material-icons">warning</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>LOW ON STOCK</b></div>
                        <div class="number"><?php echo $low?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-deep-orange">
                        <i class="material-icons">remove_shopping_cart</i>
                    </div>
                    <div class="content">
                        <div class="text"><b>OUT OF STOCK</b></div>
                        <div class="number"><?php echo $stock?></div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW ITEM
                            </a>
                            
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <form action="<?php echo base_url('inventory/create')?>" method="post">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                            
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input id="item" type="text" class="form-control" placeholder="Item">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input id="qty" type="text" class="form-control" placeholder="Quantity">
                                                </div>
                                            </div>

                                            <button id="submit-button" class="btn bg-pink waves-effect text-center" type="submit" value="submit" disabled>SUBMIT</button>
                                            <a id="clear-button" class="btn bg-grey waves-effect text-center">DISCARD</a>
                                        </div>

                                        <div class="col-lg-8 md-8 col-sm-12 col-xs-12">
                                            <table id="list" class="span12 table table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th class="align-center" style="width: 10%">Action</th>
                                                        <th class="align-left" style="width: 70%">Item</th>
                                                        <th class="align-left" style="width: 20%">Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>INVENTORY DATA
                            </a>
                            
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="inventorytable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Pcs</th>
                                                <th>Last Update</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Pcs</th>
                                                <th>Last Update</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Examples -->
</section>
<script type="text/javascript">
    $(function() {

        $('#inventorytable').DataTable({

            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'desc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/inventory_api/get_json_data')?>",
                "type": "POST"
            },

            "columnDefs": [

                {
                    "targets": 4,
                    "className": "text-center"
                }
            ]
        })

    });
</script>