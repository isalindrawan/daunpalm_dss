<section class="content">
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue-grey">
                        <h2>
                        UPDATE MENU - <?php echo $menu['ID_Menu']?>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo base_url('menu')?>">Discard</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form id="form_advanced_validation" action=" <?php echo base_url('menu/update')?> " method="post">
                            <input type="hidden" name="id" value="<?php echo $menu['ID_Menu']?>" class="form-control" placeholder="<?php echo $menu['ID_Menu']?>">
                            <label for="name">Name</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" name="name" class="form-control" placeholder="<?php echo $menu['Nama']?>" name="minmaxlength" minlength="5" maxlength="20" required>
                                </div>
                            </div>
                            
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label for="price">Price</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="price" class="form-control" placeholder="<?php echo $menu['Harga']?>" name="minmaxvalue" min="5000" max="999000" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <label for="category">Category</label>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <select name="category"  class="form-control show-tick">
                                                <?php foreach ($category as $row) {
                                                echo '<option value="'.$row['ID_Kategori'].'">'.$row['Nama_Kategori'].'</option>';
                                                }?>
                                            </select>
                                        </div>
                                        <div class="help-info">Select New Category</div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <button class="btn bg-pink waves-effect" type="submit" value="execute">SUBMIT</button>
                            <a onclick="discarding()" class="btn bg-grey waves-effect">DISCARD</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Vertical Layout -->
    </div>
</section>

<script type="text/javascript" charset="utf-8" async defer>

    function discarding() {

        swal({

            title: "Are you sure ?",
            text: "Changes that has been made will not be save.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, discard it",
            cancelButtonText: "No, continue editing",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {

                window.location.href = "<?php echo base_url('menu')?>";

            } else {

                swal.close()
            }
        });
    }
</script>