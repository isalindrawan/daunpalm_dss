<section class="content">
	<div class="container-fluid">


		<div class="row clearfix">


			<div class="panel-group" id="accordion_9" role="tablist" aria-multiselectable="true">
				<div class="panel panel-col-pink">
					<div class="panel-heading" role="tab" id="headingOne_9">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseOne_9" aria-expanded="true"
							 aria-controls="collapseOne_9">
								<i class="material-icons">bookmark</i> SEARCH REPORT
							</a>
						</h4>
					</div>
					<div id="collapseOne_9" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_9">
						<div class="panel-body">
							<br>
							<form id="form_advanced_validation" action="" method="post">

								<div class="row clearfix">

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<label for="category">Start Date</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">today</i>
											</span>
											<div class="form-line">
												<input id="start" type="text" name="start_date" class="datepicker form-control" placeholder="Date" required>
											</div>
										</div>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<label for="category">End Date</label>
										<div class="input-group">
											<span class="input-group-addon">
												<i class="material-icons">today</i>
											</span>
											<div class="form-line">
												<input id="end" type="text" name="end_date" class="datepicker form-control" placeholder="Date" required>
											</div>
										</div>
									</div>
								</div>

								<br>

								<button id="button_submit" class="btn bg-pink waves-effect" type="submit" value="execute">SUBMIT</button>
							</form>
						</div>
					</div>
				</div>
				<div class="panel panel-col-blue-grey">
					<div class="panel-heading" role="tab" id="headingTwo_9">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_9" href="#collapseTwo_9"
							 aria-expanded="false" aria-controls="collapseTwo_9">
								<i class="material-icons">bookmark</i> TRANSACTION SUMMARY
							</a>
						</h4>
					</div>
					<div id="collapseTwo_9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_9">
						<div class="panel-body">
							<form method="post" action="#" id="printJS-form">
								<button class="btn btn-lg bg-blue-grey" type="button" onclick="printJS('printJS-form', 'html')">
									PRINT
								</button>
								<button class="btn btn-lg bg-blue-grey" type="button" onclick="pdf()">
									EXPORT PDF
								</button>

								<br><br>
								
								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>RESERVATION :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th class="align-left" style="width: 80%"></th>
													<th class="align-left" style="width: 5%"></th>
													<th class="align-left" style="width: 15%"></th>
												</tr>
											</thead>

											<tbody>

												<tr>
													<td>Total</td>
													<td></td>
													<td id="Res_Total"></td>
												</tr>

												<tr>
													<td>Done</td>
													<td></td>
													<td id="Res_Done"></td>
												</tr>

												<tr>
													<td>Cancelled</td>
													<td></td>
													<td id="Res_Cancelled"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>INCOME :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th class="align-left" style="width: 80%"></th>
													<th class="align-left" style="width: 5%"></th>
													<th class="align-left" style="width: 15%"></th>
												</tr>
											</thead>

											<tbody>

												<tr>
													<td>Menu Sold</td>
													<td></td>
													<td id="In_Menu_Sold"></td>
												</tr>

												<tr>
													<td>Total Sold</td>
													<td></td>
													<td id="In_Total_Sold"></td>
												</tr>

												<tr>
													<td>Sales Transaction</td>
													<td></td>
													<td id="In_Sales_Trans"></td>
												</tr>
												<tr>
													<td>Total Income</td>
													<td>Rp. </td>
													<td id="In_Total"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>SPENDING :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th class="align-left" style="width: 80%"></th>
													<th class="align-left" style="width: 5%"></th>
													<th class="align-left" style="width: 15%"></th>
												</tr>
											</thead>

											<tbody>

												<tr>
													<td>Transaction</td>
													<td></td>
													<td id="Spend_Trans"></td>
												</tr>
												<tr>
													<td>Item</td>
													<td></td>
													<td id="Spend_Item"></td>
												</tr>
												<tr>
													<td>Quantity</td>
													<td></td>
													<td id="Spend_Qty"></td>
												</tr>
												<tr>
													<td>Total</td>
													<td>Rp. </td>
													<td id="Spend_Purchase"></td>
												</tr>
												<tr>
													<td>Salary</td>
													<td>Rp. </td>
													<td id="Spend_Salary"></td>
												</tr>
												<tr>
													<td>Total Spending</td>
													<td>Rp. </td>
													<td id="Spend_Total"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>PROFIT :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th class="align-left" style="width: 80%"></th>
													<th class="align-left" style="width: 5%"></th>
													<th class="align-left" style="width: 15%"></th>
												</tr>
											</thead>

											<tbody>

												<tr>
													<td>Total</td>
													<td>Rp. </td>
													<td id="Profit"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>

								<hr><br>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>RESERVATION DETAILS :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<br>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="res_detail_table" class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th>Name</th>
													<th>Book Date</th>
													<th>Due Date</th>
													<th>Status</th>
												</tr>
											</thead>

											<tbody>
											</tbody>
										</table>
									</div>
								</div>

								<hr><br>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>SALES TRANSACTION DETAILS :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<br>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="trans_sales_table" class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th>ID</th>
													<th>Customer</th>
													<th>Date</th>
													<th>Total</th>
												</tr>
											</thead>

											<tbody>
											</tbody>
										</table>
									</div>
								</div>

								<hr><br>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>PURCHASES TRANSACTION DETAILS :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<br>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="trans_purchases_table" class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th>ID</th>
													<th>Name</th>
													<th>Date</th>
													<th>Total</th>
												</tr>
											</thead>

											<tbody>
											</tbody>
										</table>
									</div>
								</div>

								<hr><br>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>MENU SALES DETAIL :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<br>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="menu_sales_table" class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th>ID</th>
													<th>Menu</th>
													<th>Quantity</th>
													<th>Subtotal</th>
													<th>Last Date</th>
												</tr>
											</thead>

											<tbody>
											</tbody>
										</table>
									</div>
								</div>

								<hr><br>

								<div class="row clearfix">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
										<p><b>PURCHASES ITEM DETAIL :</b></p>
									</div>

									<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
									<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
								</div>

								<br>

								<div class="row clearfix">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<table id="purchases_item_table" class="span12 table table-striped table-responsive" style="border: none">

											<thead>
												<tr>
													<th>Item</th>
													<th>Price</th>
													<th>Quantity</th>
													<th>Subtotal</th>
													<th>Date</th>
												</tr>
											</thead>

											<tbody>
											</tbody>
										</table>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- #END# Basic Examples -->
	</div>
</section>

<script src="<?php echo base_url()?>assets/js/print.min.js"></script>

<script>

	function pdf() {
		var start = document.getElementById("start").value;
		var end = document.getElementById("end").value;
		window.location.href = 'http://localhost/daunpalm_dss/generate/pdf/' + start + '/' + end;
	}

	$(document).ready(function () {

		// process the form
		$('form').submit(function (event) {

			var start = document.getElementById("start").value;
			var end = document.getElementById("end").value;

			$('#res_detail_table').find("tr:not(:first)").remove();
			$('#trans_sales_table').find("tr:not(:first)").remove();
			$('#trans_purchases_table').find("tr:not(:first)").remove();
			$('#menu_sales_table').find("tr:not(:first)").remove();
			$('#purchases_item_table').find("tr:not(:first)").remove();

			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_report/' + start + '/' + end, function (
				data) {

				document.getElementById('Res_Total').innerHTML = data.Res_Total;
				document.getElementById('Res_Done').innerHTML = data.Res_Done;
				document.getElementById('Res_Cancelled').innerHTML = data.Res_Cancelled;
				document.getElementById('In_Menu_Sold').innerHTML = data.In_Menu_Sold;
				document.getElementById('In_Total_Sold').innerHTML = data.In_Total_Sold;
				document.getElementById('In_Sales_Trans').innerHTML = data.In_Sales_Trans;
				document.getElementById('In_Total').innerHTML = data.In_Total;
				document.getElementById('Spend_Item').innerHTML = data.Spend_Item;
				document.getElementById('Spend_Qty').innerHTML = data.Spend_Qty;
				document.getElementById('Spend_Trans').innerHTML = data.Spend_Trans;
				document.getElementById('Spend_Purchase').innerHTML = data.Spend_Purchase;
				document.getElementById('Spend_Salary').innerHTML = data.Spend_Salary;
				document.getElementById('Spend_Total').innerHTML = data.Spend_Total;
				document.getElementById('Profit').innerHTML = data.Profit;
			});


			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_res_detail/' + start + '/' + end, function (
				data) {

				data.forEach(function (arrayItem) {

					$('#res_detail_table').find('tbody').append("<tr><td>" + arrayItem.Nama + "</td><td>" + arrayItem.Tgl_Res +
						"</td><td>" + arrayItem.Tgl_Kunjungan + "</td><td>" + arrayItem.Status + "</td></tr>");
				});
			});

			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_trans_sales/' + start + '/' + end,
				function (data) {

					data.forEach(function (arrayItem) {

						$('#trans_sales_table').find('tbody').append("<tr><td>" + arrayItem.ID + "</td><td>" + arrayItem.Customer +
							"</td><td>" + arrayItem.Date + "</td><td>" + arrayItem.Total + "</td></tr>");
					});
				});

			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_trans_purchases/' + start + '/' + end,
				function (data) {

					data.forEach(function (arrayItem) {

						$('#trans_purchases_table').find('tbody').append("<tr><td>" + arrayItem.ID + "</td><td>" + arrayItem.Name +
							"</td><td>" + arrayItem.Date + "</td><td>" + arrayItem.Total + "</td></tr>");
					});
				});

			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_menu_sales/' + start + '/' + end, function (
				data) {

				data.forEach(function (arrayItem) {

					$('#menu_sales_table').find('tbody').append("<tr><td>" + arrayItem.ID + "</td><td>" + arrayItem.Menu +
						"</td><td>" + arrayItem.Quantity + "</td><td>" + arrayItem.Subtotal + "</td><td>" + arrayItem.Date +
						"</td></tr>");
				});
			});

			$.getJSON('http://localhost/daunpalm_dss/datatable_api/report_api/get_purchase_item/' + start + '/' + end,
				function (data) {

					data.forEach(function (arrayItem) {

						$('#purchases_item_table').find('tbody').append("<tr><td>" + arrayItem.Item + "</td><td>" + arrayItem.Harga +
							"</td><td>" + arrayItem.Quantity + "</td><td>" + arrayItem.Subtotal + "</td><td>" + arrayItem.Date +
							"</td></tr>");
					});
				});

			event.preventDefault();
		});

	});

</script>
