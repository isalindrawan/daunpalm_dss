<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>DaunPalm | Sign In</title>
	<!-- Favicon-->
	<link rel="icon" href="<?php echo base_url()?>assets/favicon.ico" type="image/x-icon">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet">
	<!-- Animation Css -->
	<link href="<?php echo base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<!-- Sweet Alert CSS -->
	<link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body class="login-page">
	<div class="login-box">
		<div class="logo">
			<a href="javascript:void(0);">Daun<b>Palm</b></a>
			<small>Food and Beverages - Information System Management</small>
		</div>
		<div class="card">
			<div class="body">
				<form id="form_advanced_validation" action=" <?php echo base_url('forgot/check_security')?> " method="post">
					<div class="msg">Enter your security question and answer reset password</div>
					<h5 class="align-left">Security Question</h5>
					<div class="form-group">
						<div class="form-line">
                            <?php if($this->session->userdata('question') == 1 ):?>
                            <h5>What is your pet name ?</h5>
                            <?php elseif($this->session->userdata('question == 2') == 2):?>
                            <h5>What is your mother name ?</h5>
                            <?php elseif($this->session->userdata('question == 3') == 2):?>
                            <h5>What is your favorite sport</h5>
                            <?php elseif($this->session->userdata('question == 4') == 2):?>
                            <h5>Who is your first love ?</h5>
                            <?php elseif($this->session->userdata('question == 5') == 2):?>
                            <h5>What is your hobby ?</h5>
                            <?php endif;?>
							
						</div>
					</div>

					<h5 class="align-left">Answer</h5>
					<div class="form-group">
						<div class="form-line">
							<input type="text" name="answer" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-8 p-t-5">
						</div>
						<div class="col-xs-4">
							<button class="btn btn-block bg-pink waves-effect" type="submit">NEXT</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Bootstrap Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Waves Effect Plugin Js -->
	<script src="<?php echo base_url()?>assets/plugins/node-waves/waves.js"></script>

	<!-- Sweet Alert Plugin Js -->
	<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>

	<!-- Jquery Validation Plugin Css -->
	<script src="<?php echo base_url()?>assets/plugins/jquery-validation/jquery.validate.js"></script>

	<!-- Custom Js -->
	<script src="<?php echo base_url()?>assets/js/pages/forms/form-validation.js"></script>

	<!-- Custom Js -->
	<script src="<?php echo base_url()?>assets/js/admin.js"></script>
	<script src="<?php echo base_url()?>assets/js/pages/examples/sign-in.js"></script>
</body>

</html>
