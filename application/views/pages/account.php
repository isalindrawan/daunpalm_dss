<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('delete')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({

            title: "Confirm",
            text: "Are You Sure ? You will not be able to recover this data.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {

                var nama = "<?php echo $this->session->flashdata('nama')?>";
                var alamat = "<?php echo $this->session->flashdata('alamat')?>";
                var pegawai = "<?php echo $this->session->flashdata('pegawai')?>";

                window.location.href = "<?php echo base_url('employee/delete')?>" + "/" + nama + "/" + alamat + "/" + pegawai + "/confirmed";

            } else {

                swal({

                    title: "Cancelled",
                    text: "Operation aborted",
                    type: "error"

                }, function isConfirm() {

                    if (isConfirm) {
                        window.location.href = "<?php echo base_url('employee')?>";
                    }
                });
            }
        });
    });

</script>

<?php elseif ($this->session->flashdata('deleted')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been deleted.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>


<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                <i class="material-icons">add_box</i> NEW ACCOUNT
                            </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <br>
                                <?php if (empty($pegawai)): ?>
                                <h4 class="text-center">Create Not Allowed, No Active Employee Found</h4>
                                <br>
                                <?php else: ?>
                                <form id="form_advanced_validation" action="<?php echo base_url('account/create')?>" method="post">

                                    <input type="hidden" name="id" class="form-control" value="<?php echo ++$id?>" readonly>

                                    <h5 class="align-left">Employee</h5>
                                    <div class="form-group">
                                        <select name="id_peg" class="form-control show-thick">
                                            <?php foreach ($pegawai as $row):?>
                                            <option value="<?php echo $row['ID_Peg'] ?>"><?php echo $row['Nama_Depan']?> <?php echo $row['Nama_Belakang']?></option>
                                            <?php endforeach?>
                                        </select>
                                    </div>
                                    <h5 class="align-left">Username</h5>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="username" class="form-control" name="minmaxlength" minlength="6" maxlength="16" required>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                            <h5 class="align-left">Password</h5>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input id="password" type="password" name="password" class="form-control" name="minmaxlength" minlength="6" maxlength="16" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                            <h5 class="align-left">Password Recovery</h5>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input id="password" type="password" name="recovery" class="form-control" name="minmaxlength" minlength="6" maxlength="16" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"> 
                                            <h5 class="align-left">Type</h5>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select name="type" class="form-control show-thick">
                                                        <option value="Admin">Admin</option>
                                                        <option value="User">User</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h5 class="align-left">Security Question</h5>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="security" class="form-control show-thick">
                                                <option value="1">What is your pet name ?</option>
                                                <option value="2">What is your mother name ?</option>
                                                <option value="3">What is your favorite sport ?</option>
                                                <option value="4">Who is your first love ?</option>
                                                <option value="5">What is your hobby ?</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h5 class="align-left">Answer</h5>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="answer" class="form-control" required>
                                        </div>
                                    </div>
                                    
                                    <button class="btn bg-pink waves-effect" type="submit">SUBMIT</button>
                                    <a id="reset" class="btn bg-grey waves-effect">DISCARD</a>
                                </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                <i class="material-icons">assignment</i>ACCOUNT DATA
                            </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="accounttable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Username</th>
                                                <th>Type</th>
                                                <th>Last</th>
                                                <th class="align-center">Status</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Type</th>
                                            <th>Last</th>
                                            <th class="align-center">Status</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script type="text/javascript">
    $(function() {

        $('#accounttable').DataTable({
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'asc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/account_api/get_json_data')?>",
                "type": "POST"
            },

            "columnDefs": [

                {
                    "targets": 5,
                    "className": "text-center"
                },

                {
                    "targets": 6,
                    "className": "text-center",
                    "orderable": false
                }
            ]
        });

        $('#reset').on('click', function() {

            $('input[name=first_name]').val("");
            $('input[name=first_name]').attr("placeholder", "First Name");
            $('input[name=last_name]').val("");
            $('input[name=last_name]').attr("placeholder", "Last Name");
            $('input[name=address]').val("");
            $('input[name=address]').attr("placeholder", "Street");
            $('input[name=city]').val("");
            $('input[name=city]').attr("placeholder", "City");
            $('input[name=stateprovince]').val("");
            $('input[name=stateprovince]').attr("placeholder", "State/Province");
            $('input[name=contact]').val("");
            $('input[name=contact]').attr("placeholder", "Contact");
            $('input[name=position]').val("");
            $('input[name=position]').attr("placeholder", "Position");
            $('input[name=salary]').val("");
            $('input[name=salary]').attr("placeholder", "Salary");
            $('input[name=start]').val("");
            $('input[name=start]').attr("placeholder", "Start Date");

        });

    });
</script>