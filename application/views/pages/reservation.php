<?php if ($this->session->flashdata('create')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been added.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('update')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been updated.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('delete')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({

            title: "Confirm",
            text: "Are You Sure ? You will not be able to recover this data.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false,
            closeOnCancel: false

        }, function(isConfirm) {

            if (isConfirm) {
                var id_res = "<?php echo $this->session->flashdata('id_res')?>";

                window.location.href = "<?php echo base_url('reservation/delete')?>" + "/" + id_res + "/confirmed";

            } else {

                swal({

                    title: "Cancelled",
                    text: "Operation aborted",
                    type: "error"

                }, function isConfirm() {

                    if (isConfirm) {
                        window.location.href = "<?php echo base_url('employee')?>";
                    }
                });
            }
        });
    });
</script>

<?php elseif ($this->session->flashdata('deleted')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Success",
            text: "Data has been deleted.",
            type: "success"
        });
    });
</script>

<?php elseif ($this->session->flashdata('error')):?>
    
<script type="text/javascript" charset="utf-8" async defer>
    $(document).ready(function() {

        swal({
            title: "Failed",
            text: "Error occurred, cannot complete this operation.",
            type: "error"
        });
    });
</script>

<?php endif;?>

<section class="content">
    <div class="container-fluid">
        <!-- Basic Examples -->

        <div class="row clearfix">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="add-heading">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" href="#add" aria-expanded="false" aria-controls="add">
                                    <i class="material-icons">add_box</i> NEW RESERVATION
                                </a>
                            </h4>
                        </div>
                        <div id="add" class="panel-collapse collapse" role="tabpanel" aria-labelledby="add-heading">
                            <div class="panel-body">
                                <form id="form_advanced_validation" action="<?php echo base_url('reservation/create') ?>" method="post">
                                    
                                    <input type="hidden" name="res_id" value="<?php echo ++$res?>" class="form-control">
                                    <input type="hidden" name="peg_id" value="<?php echo $this->session->userdata('user_fo_id')?>" class="form-control">

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Name" name="minmaxlength" maxlength="30" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="due_date" class="datetimepicker form-control" placeholder="Date" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">group</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="persons" class="form-control" placeholder="Persons" name="minmaxlength" maxlength="3" required>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event_note</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea rows="4" name="notes" class="form-control no-resize" placeholder="Additional Notes"></textarea>
                                        </div>
                                    </div>

                                    <button class="btn bg-pink waves-effect" type="submit" value="submit">ADD</button>
                                    <a class="btn bg-grey waves-effect" href="">CANCEL</a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-col-blue-grey">
                        <div class="panel-heading" role="tab" id="datatable-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#datatable" aria-expanded="true" aria-controls="datatable">
                                    <i class="material-icons">assignment</i> RESERVATION DATA
                                </a>
                            </h4>
                        </div>
                        <div id="datatable" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="datatable-heading">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="reservationtable">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Persons</th>
                                                <th>Status</th>
                                                <th class="align-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Persons</th>
                                            <th>Status</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                        </tfoot>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                    
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">        
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-orange">
                                <i class="material-icons">event_note</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>PENDING</b></div>
                                <div class="number"><?php echo $pending?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-teal">
                                <i class="material-icons">event_available</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>DONE</b></div>
                                <div class="number"><?php echo $done?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-pink">
                                <i class="material-icons">event_busy</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>CANCELLED</b></div>
                                <div class="number"><?php echo $cancel?></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-lg-12 col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box hover-expand-effect">
                            <div class="icon bg-blue-grey">
                                <i class="material-icons">money_off</i>
                            </div>
                            <div class="content">
                                <div class="text"><b>SALARY TO PAY</b></div>
                                <div class="number"></h5></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script type="text/javascript">
    $(function() {

        $('#reservationtable').DataTable({

            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "order": [
                [0, 'desc']
            ],
            "ajax": {
                "url": "<?php echo base_url('datatable_api/reservation_api/get_json_data')?>",
                "type": "POST"
            },

            "columnDefs": [{
                "targets": 5,
                "className": "text-center",
                "orderable": false
            }]
        })

    });
</script>