<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>
		Daun Palm | Food and Beverages
	</title>
	
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	
</head>

<body class="theme-blue-grey">
	<div class="container-fluid">
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="card">
				<div class="header bg-blue-grey">
					<h3>
						Daun Palm | Food and Beverages - Cafe Report
					</h3>
				</div>

                <br>

				<div class="body">
					<form method="post" action="#" id="printJS-form">

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>RESERVATION :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th class="align-left" style="width: 80%"></th>
											<th class="align-left" style="width: 5%"></th>
											<th class="align-left" style="width: 15%"></th>
										</tr>
									</thead>

									<tbody>

										<tr>
											<td>Total</td>
											<td></td>
											<td id="Res_Total"><?php echo $general['Res_Total']?></td>
										</tr>

										<tr>
											<td>Done</td>
											<td></td>
											<td id="Res_Done"><?php echo $general['Res_Done']?></td>
										</tr>

										<tr>
											<td>Cancelled</td>
											<td></td>
											<td id="Res_Cancelled"><?php echo $general['Res_Cancelled']?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>INCOME :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th class="align-left" style="width: 80%"></th>
											<th class="align-left" style="width: 5%"></th>
											<th class="align-left" style="width: 15%"></th>
										</tr>
									</thead>

									<tbody>

										<tr>
											<td>Menu Sold</td>
											<td></td>
											<td id="In_Menu_Sold"><?php echo $general['In_Menu_Sold']?></td>
										</tr>

										<tr>
											<td>Total Sold</td>
											<td></td>
											<td id="In_Total_Sold"><?php echo $general['In_Total_Sold']?></td>
										</tr>

										<tr>
											<td>Sales Transaction</td>
											<td></td>
											<td id="In_Sales_Trans"><?php echo $general['In_Sales_Trans']?></td>
										</tr>
										<tr>
											<td>Total Income</td>
											<td>Rp. </td>
											<td id="In_Total"><?php echo $general['In_Total']?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>SPENDING :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th class="align-left" style="width: 80%"></th>
											<th class="align-left" style="width: 5%"></th>
											<th class="align-left" style="width: 15%"></th>
										</tr>
									</thead>

									<tbody>

										<tr>
											<td>Transaction</td>
											<td></td>
											<td id="Spend_Trans"><?php echo $general['Spend_Trans']?></td>
										</tr>
										<tr>
											<td>Item</td>
											<td></td>
											<td id="Spend_Item"><?php echo $general['Spend_Item']?></td>
										</tr>
										<tr>
											<td>Quantity</td>
											<td></td>
											<td id="Spend_Qty"><?php echo $general['Spend_Qty']?></td>
										</tr>
										<tr>
											<td>Total</td>
											<td>Rp. </td>
											<td id="Spend_Purchase"><?php echo $general['Spend_Purchase']?></td>
										</tr>
										<tr>
											<td>Salary</td>
											<td>Rp. </td>
											<td id="Spend_Salary"><?php echo $general['Spend_Salary']?></td>
										</tr>
										<tr>
											<td>Total Spending</td>
											<td>Rp. </td>
											<td id="Spend_Total"><?php echo $general['Spend_Total']?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>PROFIT :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th class="align-left" style="width: 80%"></th>
											<th class="align-left" style="width: 5%"></th>
											<th class="align-left" style="width: 15%"></th>
										</tr>
									</thead>

									<tbody>

										<tr>
											<td>Total</td>
											<td>Rp. </td>
											<td id="Profit"><?php echo $general['Profit']?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<hr><br>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>RESERVATION DETAILS :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<br>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table id="res_detail_table" class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th>Name</th>
											<th>Book Date</th>
											<th>Due Date</th>
											<th>Status</th>
										</tr>
									</thead>

									<tbody>
									<?php foreach($res_detail as $row):?>
									<tr>
										<td><?php echo $row['Nama']?></td>
										<td><?php echo $row['Tgl_Res']?></td>
										<td><?php echo $row['Tgl_Kunjungan']?></td>
										<td><?php echo $row['Status']?></td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>

						<hr><br>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>SALES TRANSACTION DETAILS :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<br>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table id="trans_sales_table" class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th>ID</th>
											<th>Customer</th>
											<th>Date</th>
											<th>Total</th>
										</tr>
									</thead>

									<tbody>
									<?php foreach($trans_sales as $row):?>
									<tr>
										<td><?php echo $row['ID']?></td>
										<td><?php echo $row['Customer']?></td>
										<td><?php echo $row['Date']?></td>
										<td><?php echo $row['Total']?></td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>

						<hr><br>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>PURCHASES TRANSACTION DETAILS :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<br>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table id="trans_purchases_table" class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th>ID</th>
											<th>Name</th>
											<th>Date</th>
											<th>Total</th>
										</tr>
									</thead>

									<tbody>
									<?php foreach($trans_purchases as $row):?>
									<tr>
										<td><?php echo $row['ID']?></td>
										<td><?php echo $row['Name']?></td>
										<td><?php echo $row['Date']?></td>
										<td><?php echo $row['Total']?></td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>

						<hr><br>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>MENU SALES DETAIL :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<br>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table id="menu_sales_table" class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th>ID</th>
											<th>Menu</th>
											<th>Quantity</th>
											<th>Subtotal</th>
											<th>Last Date</th>
										</tr>
									</thead>

									<tbody>
									<?php foreach($menu_sales as $row):?>
									<tr>
										<td><?php echo $row['ID']?></td>
										<td><?php echo $row['Menu']?></td>
										<td><?php echo $row['Quantity']?></td>
										<td><?php echo $row['Subtotal']?></td>
										<td><?php echo $row['Date']?></td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>

						<hr><br>

						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<p><b>PURCHASES ITEM DETAIL :</b></p>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12"></div>
							<div class="col-lg-2 col-lg-2 col-sm-3 col-xs-12"></div>
						</div>

						<br>

						<div class="row clearfix">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<table id="purchases_item_table" class="span12 table table-striped table-responsive" style="border: none">

									<thead>
										<tr>
											<th>Item</th>
											<th>Price</th>
											<th>Quantity</th>
											<th>Subtotal</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($purchase_item as $row):?>
									<tr>
										<td><?php echo $row['Item']?></td>
										<td><?php echo $row['Harga']?></td>
										<td><?php echo $row['Quantity']?></td>
										<td><?php echo $row['Subtotal']?></td>
										<td><?php echo $row['Date']?></td>
									</tr>
									<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->
	</div>

</body>

</html>
