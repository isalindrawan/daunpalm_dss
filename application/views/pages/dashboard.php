<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-light-blue">
                        <i class="material-icons">access_alarm</i>
                    </div>
                    <div class="content">
                        <div class="text"><?php echo $date ?></div>
                        <div class="number"><?php echo $time ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-green">
                        <i class="material-icons">check</i>
                    </div>
                    <div class="content">
                        <div class="text">AVAILABLE MENU</div>
                        <div class="number"><?php echo $available?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-deep-orange">
                        <i class="material-icons">close</i>
                    </div>
                    <div class="content">
                        <div class="text">UNAVAILABLE MENU</div>
                        <div class="number"><?php echo $unavailable?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box hover-expand-effect">
                    <div class="icon bg-orange">
                        <i class="material-icons">event_note</i>
                    </div>
                    <div class="content">
                        <div class="text">PENDING EVENTS</div>
                        <div class="number"><?php echo $pending?></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-col-pink">
                        <div class="panel-heading" role="tab" id="chart-heading">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" href="#chart" aria-expanded="true" aria-controls="chart">
                                    <i class="material-icons">insert_chart</i> MONTHLY INCOME / SPENDING
                                </a>
                            </h4>
                        </div>
                        <div id="chart" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="chart-heading">
                            <div class="panel-body">
                                <canvas id="dashboard_chart" height="118"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-pink">
                        <div class="font-bold m-b--35">MONTHLY SUMMARY</div>
                        <br>
                        <ul class="dashboard-stat-list">
                            <li>
                                SALES MADE
                                <span class="pull-right"><b><?php echo $total_monthly_transaction?></b> <small>SALES</small></span>
                            </li>
                            <li>
                                MENU SOLD
                                <span class="pull-right"><b><?php echo $total_monthly_menu?></b> <small>MENU</small></span>
                            </li>
                            <li>
                                TOTAL SOLD
                                <span class="pull-right"><b><?php echo $total_monthly?></b> <small>ITEMS</small></span>
                            </li>
                            <li>
                                INCOME
                                <span class="pull-right"><b><?php echo $income ?></b> <small>RUPIAH</small></span>
                            </li>
                            <li>
                                SPENDING
                                <span class="pull-right"><b><?php echo $spending ?></b> <small>RUPIAH</small></span>
                            </li>
                            <li>
                                PROFIT
                                <span class="pull-right"><b><?php echo $income-($spending + $salary['salary']) ?></b> <small>RUPIAH</small></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- Latest Social Trends -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-cyan">
                        <div class="m-b--35 font-bold">10 BEST SELLER MENU THIS MONTH</div>
                        <ul class="dashboard-stat-list">

                            <?php foreach ($top_menu as $value):?>
                            <li># <?php echo $value['Nama'] ?></li>
                            <?php endforeach?>  
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #END# Latest Social Trends -->
            <!-- Answered Tickets -->
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="card">
                    <div class="header">
                        <h2>EVENTS</h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Booked</th>
                                        <th>Due Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $temp = 1 ?>
                                    <?php foreach ($reservation as $value): ?>
                                    <tr>
                                        <td><?php echo $temp ?></td>
                                        <td><?php echo $value['Nama'] ?></td>
                                        <?php if ($value['Status'] === "Upcoming"): ?>
                                        <td><span class="label bg-orange"><?php echo $value['Status'] ?></span></td>
                                        <?php endif ?>

                                        <?php if ($value['Status'] === "Cancelled"): ?>
                                        <td><span class="label bg-pink"><?php echo $value['Status'] ?></span></td>
                                        <?php endif ?>

                                        <?php if ($value['Status'] === "Finished"): ?>
                                        <td><span class="label bg-teal"><?php echo $value['Status'] ?></span></td>
                                        <?php endif ?>
                                        
                                        <td><?php echo $value['Tgl_Res'] ?></td>
                                        <td><?php echo $value['Tgl_Kunjungan'] ?></td>
                                    </tr>
                                    <?php $temp++ ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Answered Tickets -->
        </div>
        <!-- #END# Basic Examples -->
    </div>
</section>

<script src="<?php echo base_url()?>assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
<script src="<?php echo base_url()?>assets/js/myDashboardChart.js"></script>

<script>
    
    $(function(){
        $(".sparkline").each(function () {
            var $this = $(this);
            $this.sparkline('html', $this.data());
        });
    });

</script>