<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>DaunPalm | Cash Register</title>
	<!-- Favicon-->
	<link rel="icon" href="<?php echo base_url()?>assets/favicon.ico" type="image/x-icon">
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<!-- Jquery Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Waves Effect Css -->
	<link href="<?php echo base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet">
	<!-- Animation Css -->
	<link href="<?php echo base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php echo base_url()?>assets/css/themes/all-themes.css" rel="stylesheet">

	<!-- Sweet Alert CSS -->
	<link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet">

	<!-- Bootstrap Material Datetime Picker Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
	 rel="stylesheet">
	<!-- Bootstrap Select Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">

	<!-- My Cashier CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/myStyle.css">

	<!-- My Cashier JS -->
	<script src="<?php echo base_url()?>assets/js/myCashierScript.js" type="text/javascript" charset="utf-8" async defer></script>
</head>

<body class="theme-blue-grey">

	<?php if ($this->session->flashdata('success')):?>

	<script type="text/javascript" charset="utf-8" async defer>
		$(document).ready(function () {

			swal({
				title: "Success",
				text: "Transaction Success.",
				type: "success"
			});
		});

	</script>

	<?php endif;?>
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>
	<!-- #Overlay For Sidebars -->
	<!-- Top Bar -->
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
				 aria-expanded="false"></a>
				<a href="javascript:void(0);" class="bars"></a>
				<a class="navbar-brand" href="<?php echo base_url()?>"><b>DAUN PALM</b> | Food and Beverages</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">

					<li><a href="<?php echo base_url('menu')?>"><i class="material-icons">apps</i></a></li>
					<li><a href="<?php echo base_url('logout')?>"><i class="material-icons">exit_to_app</i></a></li>

				</ul>
			</div>
		</div>
	</nav>
	<!-- #Top Bar -->
	<section id="search-filter">
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- SIDE MENU -->

			<div class="menu">
				<ul class="list">
					<!-- MENU HEADER / SEPARATOR -->

					<?php $temp = NULL; ?>

					<?php foreach ($menu as $list):?>

					<?php if($temp !== $list['Head']):?>
					<li class="header">
						<span>
							<h5>
								<?php echo strtoupper($list['Head'])?>
							</h5>
						</span>
					</li>
					<?php endif?>

					<li>

						<?php if ($list['Status'] === "Unavailable"): ?>

						<a>
							<i class="material-icons">lens</i>
							<span class="name" style="color:red">
								<?php echo $list['Nama']?></span>
						</a>

						<?php endif ?>

						<?php if ($list['Status'] === "Available"): ?>

						<a onclick="add('<?php echo $list['ID_Menu']?>', '<?php echo $list['Nama']?>', '<?php echo $list['Harga']?>')">
							<i class="material-icons">lens</i>
							<span class="name">
								<?php echo $list['Nama']?></span>
						</a>

						<?php endif ?>
					</li>

					<?php $temp = $list['Head'] ?>

					<?php endforeach?>

					<li>
						<a>
							<i></i>
							<span></span>
						</a>
					</li>

					<li>
						<a>
							<i></i>
							<span></span>
						</a>
					</li>

					<li>
						<a>
							<i></i>
							<span></span>
						</a>
					</li>

				</ul>
			</div>
			<!-- Footer -->
			<div class="legal">
				<div class="form-group">
					<div class="form-line">
						<input type="text" class="form-control fuzzy-search" placeholder="Search">
					</div>
				</div>
			</div>
			<!-- #Footer -->
		</aside>
		<!-- #Left Sidebar -->
	</section>
	<section class="content">
		<div class="container-fluid">
			<div class="row clearfix">
				<form id="form_advanced_validation" action="<?php echo base_url('cashier/post_create') ?>" name="formdata" method="post">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">
								<div class="row clearfix">
									<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
										<label class="form-label">Customer</label>
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="customer" class="form-control" required>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
										<label class="form-label">Table</label>
										<div class="form-group">
											<div class="form-line">
												<input type="text" name="table" class="form-control" required>
											</div>
										</div>
									</div>
								</div>

								<div class="row clearfix">
									<div class="col-lg-12">
										<label class="form-label">Total</label>
										<div class="form-group">
											<div class="form-line">
												<input id="total" type="text" name="money_total" class="form-control" placeholder="0" readonly>
											</div>
										</div>
									</div>
									<div class="col-lg-12">
										<label class="form-label">Paid</label>
										<div class="form-group">
											<div class="form-line">
												<input id="paid" type="text" name="money_paid" class="form-control" placeholder="0" required>
											</div>
										</div>
									</div>
									<div class="col-lg-12">
										<label class="form-label">Changes</label>
										<div class="form-group">
											<div class="form-line">
												<input id="change" type="text" name="money_change" class="form-control" placeholder="0" readonly>
											</div>
										</div>
									</div>
								</div>
								<button class="btn bg-pink waves-effect" type="submit" id="submit">SUBMIT</button>
								<a class="btn bg-grey waves-effect" href="<?php echo base_url('cashier')?>">DISCARD</a>
							</div>
						</div>
					</div>
					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="card">
							<div class="body">

								<table id="order-list" class="span12 table table-responsive">
									<thead>
										<tr>
											<th class="align-left" style="width: 20%"></th>
											<th class="align-left" style="width: 50%"></th>
											<th class="align-center" style="width: 10%"></th>
											<th class="align-center" style="width: 20%"></th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</section>

	<script type="text/javascript">
		$(document).ready(function () {

			var searchFilter = new List('search-filter', {
				valueNames: ['name']
			});
		});

	</script>

	<script src="<?php echo base_url()?>assets/plugins/list-js/list.min.js" type="text/javascript"></script>
