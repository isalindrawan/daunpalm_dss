<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>DaunPalm | Cash Register</title>

	<!-- Jquery Core Js -->
	<script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core Css -->
	<link href="<?php echo base_url()?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom Css -->
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?php echo base_url()?>assets/css/themes/all-themes.css" rel="stylesheet">

	<!-- My Cashier CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/myStyle.css">

	<!-- My Cashier JS -->
	<script src="<?php echo base_url()?>assets/js/myCashierScript.js" type="text/javascript" charset="utf-8" async defer></script>
</head>

<body id="print" class="theme-blue-grey">

	<?php $this->session->set_flashdata('success', 'success');?>
	<div class="container-fluid">
		<div class="row clearfix">

			<div class="col-lg-2"></div>
			<div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
				<div class="card">
					<div class="body">

						<table id="order-list" class="span12 table table-responsive">
							<thead>
								<tr>
									<th class="align-left" style="width: 70%"></th>
									<th class="align-center" style="width: 10%"></th>
									<th class="align-center" style="width: 20%"></th>
								</tr>
							</thead>
							<tbody>
								<?php for($i = 0; $i < $total; $i++):?>
								<tr>
									<td>
										<?php echo $dataName[$i]?>
									</td>
									<td>
										<?php echo $dataQty[$i]?>
									</td>
									<td>
										<?php echo $dataSub[$i]?>
									</td>
								</tr>
								<?php endfor;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-2"></div>
		</div>

		<div clas="row clearfix">
			<div class="col-lg-2"></div>
			<div class="col-lg-8 col-md-4 col-sm-12 col-xs-12">
				<div class="card">
					<div class="body">
						<table id="order-list" class="span12 table table-responsive">
							<thead>
								<tr>
									<th class="align-left" style="width: 50%"></th>
									<th class="align-center" style="width: 30%"></th>
									<th class="align-center" style="width: 20%"></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td>Customer</td>
									<td>
										<?php echo $info['Customer']?>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>Table</td>
									<td>
										<?php echo $info['Meja']?>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>Total</td>
									<td>
										<?php echo $info['Total']?>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>Paid</td>
									<td>
										<?php echo $info['Bayar']?>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>Chages</td>
									<td>
										<?php echo $info['Kembali']?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-2"></div>
		</div>
	</div>

	<script src="<?php echo base_url('assets/js/printThis.js')?>"></script>

	<script type="text/javascript">
		$(document).ready(function () {

			// $("#print").printThis({
			// 	header: null,
			// 	footer: null,
			// });
            window.print();
            
			var inter = setInterval(function () {
				window.location.href = "http://localhost/daunpalm_dss/cashier";
			}, 500);

		});

	</script>

	<script src="<?php echo base_url()?>assets/plugins/list-js/list.min.js" type="text/javascript"></script>
