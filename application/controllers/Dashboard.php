<?php
	
	class Dashboard extends CI_Controller
	{
		public function __construct()
		{
			
			parent::__construct();
			
			if ($this->session->userdata('session_status') !== "Approved") {
				
				redirect(base_url());
			}

			if ($this->session->userdata('user_type') !== "Admin") {
				
				redirect(base_url('cashier'));
			}

			$this->load->model('CRUD_Sold');
			$this->load->model('CRUD_Widget_Purchases');
			$this->load->model('CRUD_Widget_Sales');
			$this->load->model('CRUD_Employee');
			$this->load->model('CRUD_Menu');
			$this->load->model('CRUD_Reservation');
			$this->load->model('CRUD_Dashboard');
			// $this->load->model('CRUD_Report');
			
			// $this->CRUD_Report->generate_report();
			
			$this->CRUD_Reservation->auto_update();
		}

		public function index()
		{

			$header['title'] = "DaunPalm | Dashboard";

			$data['date'] = $this->CRUD_Dashboard->get_current_date();
			$data['time'] = $this->CRUD_Dashboard->get_current_time();

			$data['total_monthly'] = $this->CRUD_Sold->get_monthly_sales();
			$data['total_monthly_menu'] = $this->CRUD_Sold->get_monthly_menu_sales();
			$data['total_monthly_transaction'] = $this->CRUD_Sold->get_monthly_transaction();

			$data['income'] = $this->CRUD_Widget_Sales->get_income();
			$data['spending'] = $this->CRUD_Widget_Purchases->get_spending();
			$data['salary'] = $this->CRUD_Employee->get_salary();

			$data['available'] = $this->CRUD_Menu->get_available();
			$data['unavailable'] = $this->CRUD_Menu->get_unavailable();
			$data['pending'] = $this->CRUD_Reservation->get_pending();

			$data['top_menu'] = $this->CRUD_Sold->get_monthly_datasets();
			$data['reservation'] = $this->CRUD_Reservation->read_all();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/dashboard', $data);
			$this->load->view('templates/footer');
		}
	}