<?php
	class Sold extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			} else {

				if ($this->session->userdata('user_type') !== "Admin") {

					header("location:javascript://history.go(-1)");
				}
			}

			$this->load->model('CRUD_Sold');
		}

		public function index()
		{

			$header['title'] = "DaunPalm | Sales";
			
			$data['total'] = $this->CRUD_Sold->get_sales();
			$data['total_menu'] = $this->CRUD_Sold->get_menu_sales();
			$data['total_transaction'] = $this->CRUD_Sold->get_transaction();
			// $data['total_income'] = $this->CRUD_Sold->get_income();

			$data['total_daily'] = $this->CRUD_Sold->get_daily_sales();
			$data['total_daily_menu'] = $this->CRUD_Sold->get_daily_menu_sales();
			$data['total_daily_transaction'] = $this->CRUD_Sold->get_daily_transaction();
			// $data['total_daily_income'] = $this->CRUD_Sold->get_daily_income();

			$data['total_monthly'] = $this->CRUD_Sold->get_monthly_sales();
			$data['total_monthly_menu'] = $this->CRUD_Sold->get_monthly_menu_sales();
			$data['total_monthly_transaction'] = $this->CRUD_Sold->get_monthly_transaction();
			// $data['total_monthly_income'] = $this->CRUD_Sold->get_monthly_income();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/sold', $data);
			$this->load->view('templates/footer');
		}
	}