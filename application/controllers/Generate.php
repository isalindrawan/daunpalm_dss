<?php

	class Generate extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->model('CRUD_Report');
		}

		public function pdf($start, $end){

			$data['general'] = $this->get_report($start, $end);
			$data['res_detail'] = $this->get_res_detail($start, $end);
			$data['trans_sales'] = $this->get_trans_sales($start, $end);
			$data['trans_purchases'] = $this->get_trans_purchases($start, $end);
			$data['menu_sales'] = $this->get_menu_sales($start, $end);
			$data['purchase_item'] = $this->get_purchase_item($start, $end);

			//$this->load->view('pages/export',$data);
            $this->load->library('pdf');
        
            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->filename = "report.pdf";
			$this->pdf->load_view('pages/export', $data);
			
			
		}

		public function get_report($start, $end)
        {
            $list = $this->CRUD_Report->generate_report($start, $end);

            return $list;
        }
        
		public function get_res_detail($start, $end)
		{

			$list = $this->CRUD_Report->get_res($start, $end);
        	return $list;
        }
        
        public function get_trans_sales($start, $end)
		{

			$list = $this->CRUD_Report->get_trans_sales($start, $end);
        	return $list;
		}

		public function get_trans_purchases($start, $end)
		{

			$list = $this->CRUD_Report->get_trans_purchases($start, $end);
        	return $list;
		}

		public function get_menu_sales($start, $end)
		{

			$list = $this->CRUD_Report->get_menu_sales($start, $end);
        	return $list;
		}

		public function get_purchase_item($start, $end)
		{

			$list = $this->CRUD_Report->get_purchase_item($start, $end);
        	return $list;
		}
		
	}
