<?php

	class Forgot extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();

			$this->load->model('CRUD_Account');

			if ($this->session->userdata('session_status') === "Approved") {

				redirect(base_url('dashboard'));
			}

			$this->load->model('authentication');
		}

		public function index()
		{

			$this->load->view('pages/forgot');
        }

        public function security()
		{

			$this->load->view('pages/security');

		}

		public function reset()
		{

			$this->load->view('pages/reset');
			//echo '<script type="text/javascript">alert("'.$this->session->userdata('question').'")</script>';
		}

		public function recovery()
		{

			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			
			$authenticate = $this->authentication->check_recovery($username, $password);


			if ($authenticate != 0) {
				
                redirect('forgot/reset');
			
			} else {

				$authenticate = $this->authentication->get_user_details($username);

				if ($authenticate != 0) {

					$session_data = array(
					
						'temp_user' => $username,
						'question' => $authenticate['Security']
					);
		
					$this->session->set_userdata($session_data);
					
					unset($_POST);
	
					redirect('forgot/security');
				
				} else {

					$this->session->set_flashdata('not found', 'not found');

					redirect('login');
				}
				
			}
		}

		public function check_security()
		{

			$username = $this->session->userdata('temp_user');
			//$security = $this->input->post('security');
			$answer = $this->input->post('answer');

			$authenticate = $this->authentication->check_security($username, $answer);

			if ($authenticate != 0) {
				
                redirect('forgot/reset');
			
			} else {

				
				$this->session->set_flashdata('recovery', 'recovery');
				redirect('login');
			}
		}

		public function reset_pass() {

			$username = $this->session->userdata('temp_user');
			$password = md5($this->input->post('password'));

			$authenticate = $this->authentication->update_password($username, $password);
				
			$this->session->set_flashdata('recovery_success', 'recovery_success');
			redirect('login');

		}
	}