<?php 

class Cashier extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('session_status') !== "Approved") {

			redirect(base_url());
		
		} else {

			if ($this->session->userdata('user_role') !== "Kasir") {

				header("location:javascript://history.go(-1)");
			}
		}

		$this->load->model('CRUD_Register');
	}

	public function index()
	{

		unset($_POST);

		$data['menu'] = $this->CRUD_Register->read_Menu();
		$data['category'] = $this->CRUD_Register->read_Category();

		$this->load->view('register/cashregister', $data);
		$this->load->view('templates/footer');
	}

	public function post_create()
	{

		$data = array(

				'Customer' => $this->input->post('customer'),
				'Meja' => $this->input->post('table'),
				'Total' => $this->input->post('money_total'),
				'Bayar' => $this->input->post('money_paid'),
				'Kembali' => $this->input->post('money_change'),
		);

		$dataID = $this->input->post('id');
		$dataName = $this->input->post('name');
		$dataQty = $this->input->post('quantity');
		$dataSub = $this->input->post('subtotal');

		$totaldata = count($dataID);

		$print['info'] = $data;
		$print['dataID'] = $dataID;
		$print['dataName'] = $dataName;
		$print['dataQty'] = $dataQty;
		$print['dataSub'] = $dataSub;
		$print['total'] = $totaldata;

		$this->CRUD_Register->create($data, $dataID, $dataQty, $dataSub);

		$this->session->set_flashdata('success', 'success');

		unset($_POST);
		
		$this->load->view('register/receipt', $print);
	}

	public function receipt()
	{
		$this->load->view('register/receipt');
	}
}