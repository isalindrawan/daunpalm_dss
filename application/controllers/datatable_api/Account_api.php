<?php

	class Account_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			
			$this->load->model('DT_Account');
		}

		public function get_json_data()
		{

			$list = $this->DT_Account->get_datatables();
			$data = array();


			// <a class="btn bg-blue-grey btn-xs waves-effect" href="'.base_url('account/edit').'/'.$value->ID_Account.'"><i class="material-icons">edit</i></a>

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Account;
				$row[] = $value->Nama_Depan;
				$row[] = $value->Username;
				$row[] = $value->Tipe;
				$row[] = $value->Last;

				if($value->Status == 'Active')
					$row[] = '<span class="label bg-teal">'.$value->Status.'</span>';

				else
					$row[] = '<span class="label bg-pink">'.$value->Status.'</span>';

				if ($value->Status === "Active") {
					
					$option_status = "Deactive";
				}

				if ($value->Tipe === "Admin") {
					
					$option_tipe = "User";
				}

				if ($this->session->userdata('user_type') === "Admin") {
					
					$row[] = '

					<a class="btn bg-pink btn-xs waves-effect" data-toggle="modal" data-target="#'.$value->ID_Account.'"><i class="material-icons">visibility</i></a>

					<a class="btn bg-blue-grey btn-xs waves-effect" data-toggle="modal" data-target="#edit'.$value->ID_Account.'"><i class="material-icons">edit</i></a>
					
					
					<div class="modal fade" id="edit'.$value->ID_Account.'" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="smallModalLabel">ACCOUNT EDIT</h4>
								</div>

								<form id="form_advanced_validation" action="'.base_url('account/update').'" method="post">
								
									<div class="modal-body">
										<div class="row clearfix">
											<div class="col-lg-12">	

												<input type="hidden" name="id" class="form-control" value="'.$value->ID_Account.'" readonly>

												<h5 class="align-left">Name</h5>
				                                <div class="form-group">
				                                    <div class="form-line">
				                                        <input type="text" class="form-control" value="'.$value->Nama_Depan.' '.$value->Nama_Belakang.'" readonly>
				                                    </div>
				                                </div>
				                                <br><br>
				                                <h5 class="align-left">Username</h5>
				                                <div class="form-group">
				                                    <div class="form-line">
				                                        <input type="text" name="username" class="form-control" placeholder="'.$value->Username.'" value="'.$value->Username.'" readonly>
				                                    </div>
				                                </div>
				                                <br><br>
				                                <h5 class="align-left">Password</h5>
				                                <div class="form-group">
				                                    <div class="form-line">
				                                        <input type="password" name="password" class="form-control" placeholder="'.$value->Password.'" value="'.$value->Password.'" readonly>
				                                    </div>
				                                </div>
				                                <br><br>
				                                <h5 class="align-left">Type</h5>
				                                <div class="form-group">
				                                    <div class="form-line">
				                                        <select name="type" class="form-control show-thick">
				                                        	<option value="'.$value->Tipe.'">'.$value->Tipe.'</option>
				                                        	<option value="'.$option_tipe.'">'.$option_tipe.'</option>
				                                        </select>
				                                    </div>
				                                </div>
				                                <br><br>
				                                <h5 class="align-left">Status</h5>
				                                <div class="form-group">
				                                    <div class="form-line">
				                                        <select name="status" class="form-control show-thick">
				                                        	<option value="'.$value->Status.'">'.$value->Status.'</option>
				                                        	<option value="'.$option_status.'">'.$option_status.'</option>
				                                        </select>
				                                    </div>
				                                </div>
				                            </div>
										</div>
									</div>

									<div class="align-center modal-footer">
										<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
										<a class="btn btn-link waves-effect" data-dismiss="modal">DISCARD</a>
									</div>
								</form>
							</div>
						</div>
					</div>

					<script src="'.base_url().'assets/js/pages/forms/form-validation.js"></script>
					<script src="'.base_url().'assets/js/admin.js"></script>

					<div class="modal fade" id="'.$value->ID_Account.'" tabindex="-1" role="dialog">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title" id="smallModalLabel">ACCOUNT DETAILS</h4>
								</div>
								
								<div class="modal-body">
									<div class="row clearfix">
										<div class="col-lg-12">	
											<h5 class="align-left">Name</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Nama_Depan.' '.$value->Nama_Belakang.'" disabled>
			                                    </div>
			                                </div>
			                                <br><br>
			                                <h5 class="align-left">Username</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Username.'" disabled>
			                                    </div>
			                                </div>
			                                <br><br>
			                                <h5 class="align-left">Password</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Password.'" disabled>
			                                    </div>
			                                </div>
			                                <br><br>
			                                <h5 class="align-left">Type</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Tipe.'" disabled>
			                                    </div>
			                                </div>
			                                <br><br>
			                                <h5 class="align-left">Status</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Status.'" disabled>
			                                    </div>
			                                </div>
			                                <br><br>
			                                <h5 class="align-left">Last Active</h5>
			                                <div class="form-group">
			                                    <div class="form-line">
			                                        <input type="text" class="form-control" value="'.$value->Last.'" disabled>
			                                    </div>
			                                </div>
			                            </div>
									</div>
								</div>

								<div class="align-center modal-footer">
									<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
								</div>
							</div>
						</div>
					</div>
	        		';
				}

				$data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Account->count_all(),
                "recordsFiltered" => $this->DT_Account->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}
	}
