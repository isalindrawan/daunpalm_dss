<?php

	class Activity_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			
			$this->load->model('DT_Activity');
		}

		public function get_json_data()
		{

			$list = $this->DT_Activity->get_datatables();
			$data = array();

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Activity;
				$row[] = $value->User;
				$row[] = $value->Tgl;
				$row[] = $value->Tipe;
				$row[] = $value->Detail;
				$row[] = $value->Status;

				$data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Activity->count_all(),
                "recordsFiltered" => $this->DT_Activity->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}
	}
