<?php

	class Category_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			
			$this->load->model('DT_Category');
		}

		public function get_json_data()
		{

			$list = $this->DT_Category->get_datatables();
			$data = array();

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Kategori;
				$row[] = $value->Nama;
				$row[] = $value->Total;

				$row[] = '

				<a class="btn bg-pink btn-xs waves-effect" data-toggle="modal" data-target="#'.$value->ID_Kategori.'"><i class="material-icons">edit</i></a>

				<div class="modal fade" id="'.$value->ID_Kategori.'" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="smallModalLabel">Inventory Update</h4>
							</div>
							<form id="form_advanced_validation" class="form-horizontal" action="http://localhost/daunpalm_dss/category/update" method="post">
								<div class="modal-body">
									<input type="hidden" name="id" value="'.$value->ID_Kategori.'" class="form-control">
									<div class="row clearfix">
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 form-control-label">
											<label for="name_label">Name</label>
										</div>
										<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
											<div class="form-group">
												<div class="form-line">
													<input type="text" name="name" class="form-control" placeholder="'.$value->Nama.'" name="minmaxlength" minlength="4" required>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="align-center modal-footer">
									<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">DISCARD</button>
									<button type="submit" class="btn btn-link waves-effect data-dismiss="modal"">UPDATE</button>
								</div>
							</form>
						</div>
					</div>
				</div>

        		';

				$data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Category->count_all(),
                "recordsFiltered" => $this->DT_Category->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}
	}
