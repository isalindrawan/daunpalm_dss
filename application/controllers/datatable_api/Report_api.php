<?php

	class Report_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->model('CRUD_Report');
		}


        public function get_report($start, $end)
        {
            $list = $this->CRUD_Report->generate_report($start, $end);

            echo json_encode($list, true);
        }
        
		public function get_res_detail($start, $end)
		{

			$list = $this->CRUD_Report->get_res($start, $end);
        	echo json_encode($list, true);
        }
        
        public function get_trans_sales($start, $end)
		{

			$list = $this->CRUD_Report->get_trans_sales($start, $end);
        	echo json_encode($list, true);
		}

		public function get_trans_purchases($start, $end)
		{

			$list = $this->CRUD_Report->get_trans_purchases($start, $end);
        	echo json_encode($list, true);
		}

		public function get_menu_sales($start, $end)
		{

			$list = $this->CRUD_Report->get_menu_sales($start, $end);
        	echo json_encode($list, true);
		}

		public function get_purchase_item($start, $end)
		{

			$list = $this->CRUD_Report->get_purchase_item($start, $end);
        	echo json_encode($list, true);
		}
	}
