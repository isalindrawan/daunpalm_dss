<?php

	class Employee_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->model('DT_Employee');
		}

		public function get_json_data()
		{

			$list = $this->DT_Employee->get_datatables();
			$data = array();

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Peg;
				$row[] = $value->Nama_Depan;
				$row[] = $value->Tlp;
				$row[] = $value->Posisi;
				$row[] = $value->Gaji;
				$row[] = $value->Masuk;
				$row[] = $value->Keluar;

				if($value->Status == 'Active') {

					$row[] = '<span class="label bg-teal">'.$value->Status.'</span>';

					$row[] = '
				
						<a class="btn bg-blue btn-xs waves-effect" data-toggle="modal" data-target="#'.$value->ID_Peg.'"><i class="material-icons">visibility</i></a>
						<div class="modal fade" id="'.$value->ID_Peg.'" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="defaultModalLabel">'.$value->ID_Peg.'</h4>
									</div>
									<div class="modal-body">
										
										<br><br>
										
										<input type="hidden" name="employee_id" value="'.$value->ID_Peg.'" class="form-control">
										
										<div class="align-left row clearfix">
											<div class="col-md-6">
												<label for="first_name">First Name</label>
												<div class="form-group">
													<div class="form-line">
														<input type="text" name="first_name" class="form-control" value="'.$value->Nama_Depan.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<label for="last_name">Last Name</label>
												<div class="form-group">
													<div class="form-line">
														<input type="text" name="last_name" class="form-control" value="'.$value->Nama_Belakang.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br>

										<div class="align-left  row clearfix">
											<div class="col-md-12">
												<div class="form-group">
													<div class="form-line">
														<label for="address">Address</label>
														<input type="text" name="address" class="form-control" value="'.$value->Jalan.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br>
										
										<div class="align-left row clearfix">
											<div class="col-md-6">
												<div class="form-group">
													<div class="form-line">
														<label for="city">City</label>
														<input type="text" name="city" class="form-control" value="'.$value->Kota.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<div class="form-line">
														<label for="stateprovince">State/Province</label>
														<input type="text" name="stateprovince" class="form-control" value="'.$value->Provinsi.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br><br>
										
										<div class="align-left row clearfix">
											
											<!-- <label for="contact">Contact</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">phone_iphone</i>
												</span>
												<div class="form-line demo-masked-input">
													<input type="text" name="contact" class="form-control mobile-phone-number" value="'.$value->Tlp.'" placeholder="+__ (___) ___-___-___" disabled>
												</div>
											</div>
											<!-- <label for="position">Position</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">work</i>
												</span>
												<div class="form-line">
													<input type="text" name="position" class="form-control" value="'.$value->Posisi.'" disabled>
												</div>
											</div>
											<!-- <label for="salary">Salary</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">attach_money</i>
												</span>
												<div class="form-line">
													<input type="text" name="salary" class="form-control" value="'.$value->Gaji.'" disabled>
												</div>
											</div>
										</div>

										<br><br>
										
										<div class="align-left  row clearfix">
											<div class="col-sm-6">
												<label for="since">Start Date</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">date_range</i>
													</span>
													<div class="form-line">
														<input type="text"  name="start" class="datepicker form-control" value="'.$value->Masuk.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<label for="since">End Date</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">date_range</i>
													</span>
													<div class="form-line">
														<input type="text" name="end" class="datepicker form-control" value="'.$value->Keluar.'" disabled>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="align-center modal-footer">
										<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
									</div>
								</div>
							</div>
						</div>
						
						<a class="btn bg-pink btn-xs waves-effect" data-toggle="modal" data-target="#edit'.$value->ID_Peg.'"><i class="material-icons">edit</i></a>

						<div class="modal fade" id="edit'.$value->ID_Peg.'" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="defaultModalLabel">'.$value->ID_Peg.'</h4>
									</div>

									<form id="form_modal" action="'.base_url('employee/update').'" method="post">

										<div class="modal-body">
											
											<br><br>
											
											<input type="hidden" name="employee_id" value="'.$value->ID_Peg.'" class="form-control">
											<input type="hidden" name="current_status" value="'.$value->Status.'" class="form-control">
											
											<div class="align-left row clearfix">
												<div class="col-md-6">
													<label for="first_name">First Name</label>
													<div class="form-group">
														<div class="form-line">
															<input type="text" name="first_name" class="form-control" value="'.$value->Nama_Depan.'" name="minmaxlength" minlength="5" maxlength="25">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<label for="last_name">Last Name</label>
													<div class="form-group">
														<div class="form-line">
															<input type="text" name="last_name" class="form-control" value="'.$value->Nama_Belakang.'" name="minmaxlength" minlength="5" maxlength="25" required>
														</div>
													</div>
												</div>
											</div>

											<br>

											<div class="align-left  row clearfix">
												<div class="col-md-12">
													<div class="form-group">
														<div class="form-line">
															<label for="address">Address</label>
															<input type="text" name="address" class="form-control" value="'.$value->Jalan.'" name="minmaxlength" minlength="10" maxlength="50" required>
														</div>
													</div>
												</div>
											</div>

											<br>
											
											<div class="align-left row clearfix">
												<div class="col-md-6">
													<div class="form-group">
														<div class="form-line">
															<label for="city">City</label>
															<input type="text" name="city" class="form-control" value="'.$value->Kota.'" name="minmaxlength" minlength="4" maxlength="25" required>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="form-line">
															<label for="stateprovince">State/Province</label>
															<input type="text" name="stateprovince" class="form-control" value="'.$value->Provinsi.'" name="minmaxlength" minlength="5" maxlength="30" required>
														</div>
													</div>
												</div>
											</div>

											<br><br>
											
											<div class="align-left row clearfix">
												
												<!-- <label for="contact">Contact</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone_iphone</i>
													</span>
													<div class="form-line demo-masked-input">
														<input type="text" name="contact" class="form-control mobile-phone-number" value="'.$value->Tlp.'" placeholder="Contact" name="minmaxlength" minlength="11" required required>
													</div>
												</div>
												<!-- <label for="position">Position</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">work</i>
													</span>
													<div class="form-line">
														<input type="text" name="position" class="form-control" value="'.$value->Posisi.'" name="minmaxlength" minlength="5" required>
													</div>
												</div>
												<!-- <label for="salary">Salary</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">attach_money</i>
													</span>
													<div class="form-line">
														<input type="text" name="salary" class="form-control" value="'.$value->Gaji.'" name="minmaxvalue" minvalue="100000" required>
													</div>
												</div>
											</div>

											<br><br>
											
											<div class="align-left  row clearfix">
												<div class="col-sm-6">
													<label for="since">Start Date</label>
													<div class="input-group">
														<span class="input-group-addon">
															<i class="material-icons">date_range</i>
														</span>
														<div class="form-line">
															<input type="text"  name="start" class="datepicker form-control" value="'.$value->Masuk.'" readonly>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<label for="status">Status</label>
													<div class="form-group form-float">
														<div class="form-line">
															<select name="status"  class="form-control show-tick">
																<option selected="selected" value="Active">Active</option>
																<option value="Deactive">Deactive</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="align-center modal-footer">
											<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
											<a class="btn btn-link waves-effect" data-dismiss="modal">DISCARD</a>
										</div>
									</form>
								</div>
							</div>
						</div>

						<a class="btn  bg-blue-grey btn-xs waves-effect" href="'.base_url('employee/delete').'/'.$value->ID_Nama.'/'.$value->ID_Alamat.'/'.$value->ID_Peg.'/'.'"><i class="material-icons">delete</i></a>

						<script type="text/javascript">
						
							$(document).ready(function() {
								
								$("#form_modal").validate({

									highlight: function (input) {
										$(input).parents(".form-line").addClass("error");
									},
									unhighlight: function (input) {
										$(input).parents(".form-line").removeClass("error");
									},
									errorPlacement: function (error, element) {
										$(element).parents(".form-group").append(error);
									}
								});

								$.AdminBSB.input.activate();

							});

						</script>
						';

				}

				else {

					$row[] = '<span class="label bg-pink">'.$value->Status.'</span>';

					$row[] = '
				
						<a class="btn bg-blue btn-xs waves-effect" data-toggle="modal" data-target="#'.$value->ID_Peg.'"><i class="material-icons">visibility</i></a>
						<div class="modal fade" id="'.$value->ID_Peg.'" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="defaultModalLabel">'.$value->ID_Peg.'</h4>
									</div>
									<div class="modal-body">
										
										<br><br>
										
										<input type="hidden" name="employee_id" value="'.$value->ID_Peg.'" class="form-control">
										
										<div class="align-left row clearfix">
											<div class="col-md-6">
												<label for="first_name">First Name</label>
												<div class="form-group">
													<div class="form-line">
														<input type="text" name="first_name" class="form-control" value="'.$value->Nama_Depan.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<label for="last_name">Last Name</label>
												<div class="form-group">
													<div class="form-line">
														<input type="text" name="last_name" class="form-control" value="'.$value->Nama_Belakang.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br>

										<div class="align-left  row clearfix">
											<div class="col-md-12">
												<div class="form-group">
													<div class="form-line">
														<label for="address">Address</label>
														<input type="text" name="address" class="form-control" value="'.$value->Jalan.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br>
										
										<div class="align-left row clearfix">
											<div class="col-md-6">
												<div class="form-group">
													<div class="form-line">
														<label for="city">City</label>
														<input type="text" name="city" class="form-control" value="'.$value->Kota.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<div class="form-line">
														<label for="stateprovince">State/Province</label>
														<input type="text" name="stateprovince" class="form-control" value="'.$value->Provinsi.'" disabled>
													</div>
												</div>
											</div>
										</div>

										<br><br>
										
										<div class="align-left row clearfix">
											
											<!-- <label for="contact">Contact</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">phone_iphone</i>
												</span>
												<div class="form-line demo-masked-input">
													<input type="text" name="contact" class="form-control mobile-phone-number" value="'.$value->Tlp.'" placeholder="+__ (___) ___-___-___" disabled>
												</div>
											</div>
											<!-- <label for="position">Position</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">work</i>
												</span>
												<div class="form-line">
													<input type="text" name="position" class="form-control" value="'.$value->Posisi.'" disabled>
												</div>
											</div>
											<!-- <label for="salary">Salary</label> -->
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">attach_money</i>
												</span>
												<div class="form-line">
													<input type="text" name="salary" class="form-control" value="'.$value->Gaji.'" disabled>
												</div>
											</div>
										</div>

										<br><br>
										
										<div class="align-left  row clearfix">
											<div class="col-sm-6">
												<label for="since">Start Date</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">date_range</i>
													</span>
													<div class="form-line">
														<input type="text"  name="start" class="datepicker form-control" value="'.$value->Masuk.'" disabled>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<label for="since">End Date</label>
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">date_range</i>
													</span>
													<div class="form-line">
														<input type="text" name="end" class="datepicker form-control" value="'.$value->Keluar.'" disabled>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="align-center modal-footer">
										<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
									</div>
								</div>
							</div>
						</div>
						
						<a class="btn bg-pink btn-xs waves-effect" data-toggle="modal" data-target="#edit'.$value->ID_Peg.'"><i class="material-icons">edit</i></a>

						<div class="modal fade" id="edit'.$value->ID_Peg.'" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="defaultModalLabel">'.$value->ID_Peg.'</h4>
									</div>

									<form id="form_modal" action="'.base_url('employee/update').'" method="post">

										<div class="modal-body">
											
											<br><br>
											
											<input type="hidden" name="employee_id" value="'.$value->ID_Peg.'" class="form-control">
											<input type="hidden" name="current_status" value="'.$value->Status.'" class="form-control">
											
											<div class="align-left row clearfix">
												<div class="col-md-6">
													<label for="first_name">First Name</label>
													<div class="form-group">
														<div class="form-line">
															<input type="text" name="first_name" class="form-control" value="'.$value->Nama_Depan.'" name="minmaxlength" minlength="5" maxlength="25">
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<label for="last_name">Last Name</label>
													<div class="form-group">
														<div class="form-line">
															<input type="text" name="last_name" class="form-control" value="'.$value->Nama_Belakang.'" name="minmaxlength" minlength="5" maxlength="25" required>
														</div>
													</div>
												</div>
											</div>

											<br>

											<div class="align-left  row clearfix">
												<div class="col-md-12">
													<div class="form-group">
														<div class="form-line">
															<label for="address">Address</label>
															<input type="text" name="address" class="form-control" value="'.$value->Jalan.'" name="minmaxlength" minlength="10" maxlength="50" required>
														</div>
													</div>
												</div>
											</div>

											<br>
											
											<div class="align-left row clearfix">
												<div class="col-md-6">
													<div class="form-group">
														<div class="form-line">
															<label for="city">City</label>
															<input type="text" name="city" class="form-control" value="'.$value->Kota.'" name="minmaxlength" minlength="4" maxlength="25" required>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<div class="form-line">
															<label for="stateprovince">State/Province</label>
															<input type="text" name="stateprovince" class="form-control" value="'.$value->Provinsi.'" name="minmaxlength" minlength="5" maxlength="30" required>
														</div>
													</div>
												</div>
											</div>

											<br><br>
											
											<div class="align-left row clearfix">
												
												<!-- <label for="contact">Contact</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone_iphone</i>
													</span>
													<div class="form-line demo-masked-input">
														<input type="text" name="contact" class="form-control mobile-phone-number" value="'.$value->Tlp.'" placeholder="Contact" name="minmaxlength" minlength="11" required required>
													</div>
												</div>
												<!-- <label for="position">Position</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">work</i>
													</span>
													<div class="form-line">
														<input type="text" name="position" class="form-control" value="'.$value->Posisi.'" name="minmaxlength" minlength="5" required>
													</div>
												</div>
												<!-- <label for="salary">Salary</label> -->
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">attach_money</i>
													</span>
													<div class="form-line">
														<input type="text" name="salary" class="form-control" value="'.$value->Gaji.'" name="minmaxvalue" minvalue="100000" required>
													</div>
												</div>
											</div>

											<br><br>
											
											<div class="align-left  row clearfix">
												<div class="col-sm-6">
													<label for="since">Start Date</label>
													<div class="input-group">
														<span class="input-group-addon">
															<i class="material-icons">date_range</i>
														</span>
														<div class="form-line">
															<input type="text"  name="start" class="datepicker form-control" value="'.$value->Masuk.'" readonly>
														</div>
													</div>
												</div>

												<div class="col-md-6">
													<label for="status">Status</label>
													<div class="form-group form-float">
														<div class="form-line">
															<select name="status"  class="form-control show-tick">
																<option value="Active">Active</option>
																<option selected="selected" value="Deactive">Deactive</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="align-center modal-footer">
											<button type="submit" class="btn btn-link waves-effect">UPDATE</button>
											<a class="btn btn-link waves-effect" data-dismiss="modal">DISCARD</a>
										</div>
									</form>
								</div>
							</div>
						</div>

						<a class="btn  bg-blue-grey btn-xs waves-effect" href="'.base_url('employee/delete').'/'.$value->ID_Nama.'/'.$value->ID_Alamat.'/'.$value->ID_Peg.'/'.'"><i class="material-icons">delete</i></a>

						<script type="text/javascript">
						
							$(document).ready(function() {
								
								$("#form_modal").validate({

									highlight: function (input) {
										$(input).parents(".form-line").addClass("error");
									},
									unhighlight: function (input) {
										$(input).parents(".form-line").removeClass("error");
									},
									errorPlacement: function (error, element) {
										$(element).parents(".form-group").append(error);
									}
								});

								$.AdminBSB.input.activate();

							});

						</script>
						';
				}

				

				$data[] = $row;					
			}

			$output = array(
				
				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Employee->count_all(),
                "recordsFiltered" => $this->DT_Employee->count_filtered(),
                "data" => $data,
            );
        	
        	//output to json format
        	echo json_encode($output, true);
		}
	}