<?php

	class Reservation_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->model('DT_Reservation');
		}

		public function get_json_data()
		{

			$list = $this->DT_Reservation->get_datatables();
			$data = array();

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Reservasi;
				$row[] = $value->Nama;
				$row[] = $value->Tgl_Kunjungan;
				$row[] = $value->Jumlah;

				if ($value->Status === 'Finished') {

					$row[] = '<span class="label bg-teal">'.$value->Status.'</span>';
				
				} elseif ($value->Status === 'Upcoming') {

					$row[] = '<span class="label bg-orange">'.$value->Status.'</span>';

				} if ($value->Status === 'Cancelled') {

					$row[] = '<span class="label bg-pink">'.$value->Status.'</span>';
				}
				

				$row[] = '

				<a class="btn bg-blue btn-xs waves-effect" data-toggle="modal" data-target="#'.$value->ID_Reservasi.'"><i class="material-icons">visibility</i></a>

				<div class="modal fade" id="'.$value->ID_Reservasi.'" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="smallModalLabel">RESERVATION - '.$value->ID_Reservasi.'</h4>
							</div>
							
							<div class="modal-body">

								<input type="hidden" name="id_category" value="'.$value->ID_Reservasi.'" class="form-control">
								<div class="row clearfix">
									<div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" value="'.$value->Nama.'" placeholder="Name" name="minmaxlength" maxlength="30" disabled>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">today</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="res_date" class="datepicker form-control" value="'.$value->Tgl_Res.'" placeholder="Date" disabled>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">schedule</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="due_date" class="datepicker form-control" value="'.$value->Tgl_Kunjungan.'"  placeholder="Date" disabled>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">group</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="persons" class="form-control" value="'.$value->Jumlah.'"  placeholder="Persons" name="minmaxlength" maxlength="3" disabled>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event_note</i>
                                        </span>
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" disabled>'.$value->Catatan.'</textarea>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">info_outline</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="persons" class="form-control" value="'.$value->Status.'"  placeholder="Persons" name="minmaxlength" maxlength="3" disabled>
                                        </div>
                                    </div>
								</div>
							</div>

							<div class="align-center modal-footer">
								<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
							</div>
						</div>
					</div>
				</div>

                <a class="btn bg-pink btn-xs waves-effect" href="'.base_url('reservation/update').'/'.$value->ID_Reservasi.'"><i class="material-icons">edit</i></a>

                <a class="btn bg-blue-grey btn-xs waves-effect" href="'.base_url('reservation/delete').'/'.$value->ID_Reservasi.'"><i class="material-icons">delete</i></a>
        		';

				$data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Reservation->count_all(),
                "recordsFiltered" => $this->DT_Reservation->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}
	}
