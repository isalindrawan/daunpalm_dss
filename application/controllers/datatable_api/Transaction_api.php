<?php

	class Transaction_api extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();

			$this->load->model('CRUD_Transaction');
			$this->load->model('DT_Transaction_Purchases');
			$this->load->model('DT_Transaction_Sales');
			$this->load->model('CRUD_Widget_Sales');
			$this->load->model('CRUD_Widget_Purchases');
			$this->load->model('CRUD_Dashboard');
		}

		public function get_json_data_purchases()
		{

			$list = $this->DT_Transaction_Purchases->get_datatables();

			$data = array();

			foreach ($list as $value) {

				 $row = array();

				 $row[] = $value->ID_TB;
				 $row[] = $value->Nama_Depan;
				 $row[] = $value->Tgl;
				 $row[] = $value->Total;

				 $row[] = '<a class="btn bg-pink btn-xs waves-effect" href="'.base_url('transaction/purchases/detail').'/'.$value->ID_TB.'"><i class="material-icons">visibility</i></a>';

				 $data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Transaction_Purchases->count_all(),
                "recordsFiltered" => $this->DT_Transaction_Purchases->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}

		public function get_json_data_sales()
		{

			$list = $this->DT_Transaction_Sales->get_datatables();

			$data = array();

			foreach ($list as $value) {

				 $row = array();

				 $row[] = $value->ID_TJ;
				 $row[] = $value->Nama_Depan;
				 $row[] = $value->Customer;
				 $row[] = $value->Tgl;
				 $row[] = $value->Total;

				 $row[] = '<a class="btn bg-pink btn-xs waves-effect" href="'.base_url('transaction/sales/detail').'/'.$value->ID_TJ.'"><i class="material-icons">visibility</i></a>';

				 $data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Transaction_Sales->count_all(),
                "recordsFiltered" => $this->DT_Transaction_Sales->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}

		public function get_json_data_monthly_chart() {

			$list = $this->CRUD_Widget_Sales->read_monthly_income_dataset();
			$data = array();

			foreach ($list as $value) {
				
				$data[] = $value;
			}

			echo json_encode($data);
		}

		public function get_json_data_monthly_chart_spending() {

			$list = $this->CRUD_Widget_Purchases->read_monthly_purchases_dataset();
			$data = array();

			foreach ($list as $value) {
				
				$data[] = $value;
			}

			echo json_encode($data);
		}

		public function get_json_data_dashboard_chart() {

			$list = $this->CRUD_Dashboard->read_income_spending_datasets();
			$data = array();

			foreach ($list as $value) {
				
				$data[] = $value;
			}

			echo json_encode($data);
		}
	}
