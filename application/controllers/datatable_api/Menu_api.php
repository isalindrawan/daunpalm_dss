<?php

	class Menu_api extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->model('DT_Menu');
		}

		public function get_json_data()
		{

			$list = $this->DT_Menu->get_datatables();
			$data = array();

			foreach($list as $value) {

				$row = array();
				$row[] = $value->ID_Menu;
				$row[] = $value->Nama;
				$row[] = $value->Nama_Kategori;
				$row[] = $value->Harga;

				if($value->Status == 'Available')
					$row[] = '<span class="label bg-teal">'.$value->Status.'</span>';
				else
					$row[] = '<span class="label bg-pink">'.$value->Status.'</span>';

				if ($this->session->userdata('user_type') === "Admin") {

					if ($value->Status == 'Unavailable') {
						
						$row[] = '
						<a class="btn bg-blue btn-xs waves-effect" href="'.base_url('menu/update_status/').'Available/'.$value->ID_Menu.'"><i class="material-icons">autorenew</i></a>	
						<a class="btn bg-pink btn-xs waves-effect" href="'.base_url('menu/edit').'/'.$value->ID_Menu.'"><i class="material-icons">edit</i></a>
						<a class="btn bg-blue-grey btn-xs waves-effect" href="'.base_url('menu/delete').'/'.$value->ID_Menu.'"><i class="material-icons">delete</i></a>';
					
					} else {

						$row[] = '
						<a class="btn bg-grey btn-xs waves-effect" href="'.base_url('menu/update_status/').'Unavailable/'.$value->ID_Menu.'"><i class="material-icons">autorenew</i></a>	
						<a class="btn bg-pink btn-xs waves-effect" href="'.base_url('menu/edit').'/'.$value->ID_Menu.'"><i class="material-icons">edit</i></a>
						<a class="btn bg-blue-grey btn-xs waves-effect" href="'.base_url('menu/delete').'/'.$value->ID_Menu.'"><i class="material-icons">delete</i></a>';
					}
				}

				$data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Menu->count_all(),
                "recordsFiltered" => $this->DT_Menu->count_filtered(),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}
	}
