<?php

	class Sold_api extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
		}

		public function get_json_data($temp = 'all')
		{

			$this->load->model('DT_Sales');
			$list = $this->DT_Sales->get_datatables($temp);

			$data = array();

			foreach ($list as $value) {

				 $row = array();

				 $row[] = $value->ID_Menu;
				 $row[] = $value->Nama;
				 $row[] = $value->Quantity;
				 $row[] = $value->Subtotal;
				 $row[] = $value->Tgl;

				 $data[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
                "recordsTotal" => $this->DT_Sales->count_all(),
                "recordsFiltered" => $this->DT_Sales->count_filtered($temp),
                "data" => $data,
            );

        	//output to json format
        	echo json_encode($output, true);
		}

		public function get_chart_data()
		{

			$this->load->model('CRUD_Sold');
			$list = $this->CRUD_Sold->get_datasets();

			$data = array();

			foreach ($list as $value) {

				 $data[] = $value;
			}

        	print json_encode($data);
		}

		public function get_daily_chart_data()
		{

			$this->load->model('CRUD_Sold');
			$list = $this->CRUD_Sold->get_daily_datasets();

			$data = array();

			foreach ($list as $value) {

				 $data[] = $value;
			}

        	print json_encode($data);
		}

		public function get_monthly_chart_data()
		{

			$this->load->model('CRUD_Sold');
			$list = $this->CRUD_Sold->get_monthly_datasets();

			$data = array();

			foreach ($list as $value) {

				 $data[] = $value;
			}

        	print json_encode($data);
		}
	}
