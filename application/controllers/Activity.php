<?php

	class Activity extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			} else {

				if ($this->session->userdata('user_type') !== "Admin") {

					header("location:javascript://history.go(-1)");
				}
			}

			// $this->load->model('CRUD_Category');
		}

		public function index()
		{

			$header['title'] = "DaunPalm | Activity";

			$this->load->view('templates/header', $header);
			$this->load->view('pages/activity');
			$this->load->view('templates/footer');
		}
	}
