<?php

	class Category extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			} else {

				if ($this->session->userdata('user_type') !== "Admin") {

					header("location:javascript://history.go(-1)");
				}
			}

			$this->load->model('CRUD_Category');
		}

		public function index()
		{

			$header['title'] = "DaunPalm | Category";

			$data['id'] = $this->CRUD_Category->get_id();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/category', $data);
			$this->load->view('templates/footer');
		}

		public function create()
		{

			$data = array(

				'ID_Kategori' => $this->input->post('id'),
				'Nama' => $this->input->post('name')
			);

			$status = $this->CRUD_Category->create($data);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				redirect('category');
			}

			$this->session->set_flashdata('create', 'create');

			unset($_POST);

			redirect('category');
		}
		
		public function update()
		{

			$data = array(

				'ID_Kategori' => $this->input->post('id'),
				'Nama' => $this->input->post('name'),
			);

			$status = $this->CRUD_Category->update($data);

			if ($status === "Failed") {

				$this->session->set_flashdata('error', 'error');
 				redirect('category');
			}

			$this->session->set_flashdata('update', 'update');

			unset($_POST);
			
 			redirect('category');
		}
	}
