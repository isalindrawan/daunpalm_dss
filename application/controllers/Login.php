<?php

	class Login extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();

			$this->load->model('CRUD_Account');

			if ($this->session->userdata('session_status') === "Approved") {

				redirect(base_url('dashboard'));
			}

			$this->load->model('authentication');
		}

		public function index()
		{

			$this->load->view('pages/login');
		}

		public function authenticate()
		{

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$password = md5($password);
			//echo '<script type="text/javascript">alert("'.$username.'/'.$password.'")</script>';
			
			$authenticate = $this->authentication->check_login($username, $password);

			if ($authenticate != 0) {
				
				if ($authenticate['Status'] === "Deactive" || $authenticate['Stat'] === "Deactive") {

					$this->session->set_flashdata('denied', 'denied');

					unset($_POST);

					redirect('login');
				
				} else {

					$session_data = array(
					
						'user_id' => $authenticate['ID_Account'],
						'Username' => $authenticate['Username'],
						'user_password' => $authenticate['Password'],
						'user_type' => $authenticate['Tipe'],
						'user_status' => $authenticate['Status'],
						'user_fo_id' => $authenticate['FO_ID_Peg'],
						'user_role' => $authenticate['Posisi'],
						'session_status' => "Approved"
					);

					$this->session->set_userdata($session_data);

					$this->CRUD_Account->update($authenticate['ID_Account']);
					
					if ($authenticate['Tipe'] === "User") {

						if ($authenticate['Posisi'] === "Kasir") {
							
							unset($_POST);

							redirect('cashier');
						
						} else {

							unset($_POST);

							redirect('menu');
						}
					
					} elseif ($authenticate['Tipe'] === "Admin") {

						unset($_POST);

						redirect('dashboard');
					}
				}
			
			} else {

				$this->session->set_flashdata('denied', 'denied');

				unset($_POST);

				redirect('login');
			}
		}
	}