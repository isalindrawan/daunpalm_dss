<?php 

	class Employee extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			} else {

				if ($this->session->userdata('user_type') !== "Admin") {

					header("location:javascript://history.go(-1)");
				}
			}
			
			$this->load->model('CRUD_Employee');
		}

		public function index()
		{
			unset($_POST);

			$header['title'] = "DaunPalm | Employee";

			$data['employee'] = $this->CRUD_Employee->get_peg_id();
			$data['address'] = $this->CRUD_Employee->get_alamat_id();
			$data['name'] = $this->CRUD_Employee->get_nama_id();
			$data['total'] = $this->CRUD_Employee->get_total();
			$data['active'] = $this->CRUD_Employee->get_active();
			$data['deactive'] = $this->CRUD_Employee->get_deactive();
			$data['salary'] = $this->CRUD_Employee->get_salary();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/employee', $data);
			$this->load->view('templates/footer');
		}

		private function get_current_date()
		{

			return date('Y-m-d');
		}

		public function create()
		{

			$name = array(

				'ID_Nama' => $this->input->post('name_id'),
				'Nama_Depan' => $this->input->post('first_name'),
				'Nama_Belakang' => $this->input->post('last_name')
			);

			$address = array(

				'ID_Alamat' => $this->input->post('address_id'),
				'Jalan' => $this->input->post('address'),
				'Kota' => $this->input->post('city'),
				'Provinsi' => $this->input->post('stateprovince')
			);

			$end = NULL;

			$status = "Active";

			$employee = array(

				'ID_Peg' => $this->input->post('employee_id'),
				'Tlp' => $this->input->post('contact'),
				'Gaji' => $this->input->post('salary'),
				'Posisi' => $this->input->post('position'),
				'Masuk' => $this->input->post('start'),
				'Keluar' => $end,
				'Status' => $status,
				'FO_ID_Nama' => $this->input->post('name_id'),
				'FO_ID_Alamat' => $this->input->post('address_id'),
			);

			$status = $this->CRUD_Employee->create($name, $address, $employee);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');
				
				unset($_POST);

				redirect('employee');
			}

			$this->session->set_flashdata('create', 'create');
			
			unset($_POST);

			redirect('employee');
		}

		public function update()
		{

			$end = NULL;
			$status = NULL;

			$nama = array(

				'Nama_Depan' => $this->input->post('first_name'),
				'Nama_Belakang' => $this->input->post('last_name')
			);

			$alamat = array(

				'Jalan' => $this->input->post('address'),
				'Kota' => $this->input->post('city'),
				'Provinsi' => $this->input->post('stateprovince')
			);

			if ($this->input->post('status') == "Keep") {
				
				$status = $this->input->post('current_status');
				$end = NULL;
			
			} else if ($this->input->post('status') == "Active") {

				$status = 'Active';
				$end = NULL;
			
			} else {

				$status = 'Deactive';
				$end = $this->get_current_date();
			}

			$pegawai = array(

				'Tlp' => $this->input->post('contact'),
				'Gaji' => $this->input->post('salary'),
				'Posisi' => $this->input->post('position'),
				'Masuk' => $this->input->post('start'),
				'Keluar' => $end,
				'Status' => $status
			);

			$where = array(
				'ID_Peg' => $this->input->post('employee_id'),
				'ID_Nama' => $this->input->post('name_id'),
				'ID_Alamat' => $this->input->post('address_id'),
			);

			$status = $this->CRUD_Employee->update($nama, $alamat, $pegawai, $where);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');
				
				unset($_POST);

				redirect('employee');
			}

			$this->session->set_flashdata('update', 'update');

			unset($_POST);

 			redirect('employee');
		}

		public function delete($nama = NULL, $alamat = NULL, $pegawai = NULL, $status = NULL)
		{

			if (empty($nama) || empty($alamat) || empty($pegawai)) {

				redirect('Error_404');
			
			} elseif (empty($status)) {

				$this->session->set_flashdata('delete', 'delete');
				$this->session->set_flashdata('nama', $nama);
				$this->session->set_flashdata('alamat', $alamat);
				$this->session->set_flashdata('pegawai', $pegawai);

				redirect('employee');
			
			} elseif ($status == 'confirmed') {

				$query_status = $this->CRUD_Employee->delete($nama, $alamat, $pegawai);
				
				if ($query_status === "Failed") {
					
					$this->session->set_flashdata('error', 'error');

					unset($_POST);
					
					redirect('employee');
				}

				$this->session->set_flashdata('deleted', 'deleted');

				unset($_POST);

				redirect('employee');
			}
		}
	}