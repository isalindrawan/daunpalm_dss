<?php

class Report extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('session_status') !== "Approved") {

			redirect(base_url());
	
		}

		if ($this->session->userdata('user_type') !== "Admin") {
				
			header("location:javascript://history.go(-1)");
		}

		$this->load->model('CRUD_Report');
	}

	public function index()
	{

		$data['Hist'] = $this->CRUD_Report->get_report_hist();

		$header['title'] = "DaunPalm | Report";

		$this->load->view('templates/header', $header);
		$this->load->view('pages/report', $data);
		$this->load->view('templates/footer');
	}
}