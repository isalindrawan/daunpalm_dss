<?php

	class Reservation extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			}
			
			$this->load->model('CRUD_Reservation');
		}

		public function index()
		{

			$header['title'] = "DaunPalm | Reservation";
			$data['res'] = $this->CRUD_Reservation->get_id_res();
			$data['done'] = $this->CRUD_Reservation->get_done();
			$data['pending'] = $this->CRUD_Reservation->get_pending();
			$data['cancel'] = $this->CRUD_Reservation->get_cancel();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/reservation', $data);
			$this->load->view('templates/footer');
		}

		private function get_current_timestamp()
		{

			return date('Y-m-d H:i:s');
		}

		public function create()
		{

			$date = $this->get_current_timestamp();

			$res = array(

				'ID_Reservasi' => $this->input->post('res_id'),
				'Nama' => $this->input->post('name'),
				'Tgl_Res' => $date,
				'Tgl_Kunjungan' => $this->input->post('due_date'),
				'Jumlah' => $this->input->post('persons'),
				'Catatan' => $this->input->post('notes'),
				'Status' => "Upcoming",
				'FO_ID_Peg' => $this->input->post('peg_id')
			);

			$status = $this->CRUD_Reservation->create($res);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				unset($_POST);

				redirect('reservation');
			}

			$this->session->set_flashdata('create', 'create');

			unset($_POST);

			redirect('reservation');
		}

		public function update($id = NULL)
		{

			if (empty($id)) {
				
				redirect('error_404');
			}

			$header['title'] = "DaunPalm | Edit Reservation";
			$data['reservation'] = $this->CRUD_Reservation->read($id);

			$this->load->view('templates/header', $header);
			$this->load->view('pages/reservation_edit', $data);
			$this->load->view('templates/footer');
		}

		public function post_update()
		{

			$res = array(

				'Nama' => $this->input->post('name'),
				'Tgl_Res' => $this->input->post('res_date'),
				'Tgl_Kunjungan' => $this->input->post('due_date'),
				'Jumlah' => $this->input->post('persons'),
				'Catatan' => $this->input->post('notes'),
				'Status' => $this->input->post('status')
			);

			$where = array(
				
				'ID_Reservasi' => $this->input->post('id')
			);

			$status = $this->CRUD_Reservation->update($res, $where);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				unset($_POST);

				redirect('reservation');
			}

			$this->session->set_flashdata('update', 'update');

			unset($_POST);

			redirect('reservation');
		}

		public function delete($id_res = NULL, $status = NULL)
		{

			if (empty($id_res)) {

				redirect('error_404');
			
			} elseif (empty($status)) {

				$this->session->set_flashdata('delete', 'delete');
				$this->session->set_flashdata('id_res', $id_res);

				redirect('reservation');
			
			} elseif ($status == 'confirmed') {

				$status = $this->CRUD_Reservation->delete($id_res);

				if ($status === "Failed") {
					
					$this->session->set_flashdata('error', 'error');

					unset($_POST);

					redirect('reservation');
				}

				$this->session->set_flashdata('deleted', 'deleted');

				unset($_POST);
				
				redirect('reservation');
			}
		}
	}
