<?php 
	
	class Transaction extends CI_Controller
	{
		
		public function __construct()
		{
			parent::__construct();
			
			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			}

			$this->load->model("CRUD_Transaction");
			$this->load->model("CRUD_Widget_Sales");
			$this->load->model("CRUD_Widget_Purchases");
			$this->load->model("CRUD_Employee");
			$this->load->model("CRUD_Sold");			
		}

		public function post_create()
		{

			$detail_id = $this->CRUD_Transaction->read_ID_DT();

			$item = $this->input->post('item_list');
			$price = $this->input->post('item_price');
			$quantity = $this->input->post('item_quantity');
			$subtotal = $this->input->post('item_subtotal');

			$total = count($item);

			$detail = array(
				'ID_DT' => ++$detail_id,
				'Total' => $this->input->post('total'),
				'Bayar' => $this->input->post('paid'),
				'Kembali' => $this->input->post('change'),
				'Tipe' => "Beli", 
			);

			$status = $this->CRUD_Transaction->create($detail, $item, $price, $quantity, $subtotal, $total);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				unset($_POST);

				redirect('transaction/purchases');
			}

			$this->session->set_flashdata('create', 'create');

			unset($_POST);
			
			redirect('transaction/purchases');
		}

		public function purchases($value = NULL, $id = NULL)
		{

			if (empty($value)) {
				
				$header['title'] = "DaunPalm | Purchases";
				$data['param'] = 'purchases';

				$data['today_spending'] = $this->CRUD_Widget_Purchases->get_today_spending();
				$data['yesterday_spending'] = $this->CRUD_Widget_Purchases->get_yesterday_spending();
				$data['last_3_spending'] = $this->CRUD_Widget_Purchases->get_last_spending();
				$data['last_week_spending'] = $this->CRUD_Widget_Purchases->get_week_spending();
				$data['today_transaction'] = $this->CRUD_Widget_Purchases->get_today_transaction();
				$data['yesterday_transaction'] = $this->CRUD_Widget_Purchases->get_yesterday_transaction();
				$data['last_3_transaction'] = $this->CRUD_Widget_Purchases->get_last_transaction();
				$data['last_week_transaction'] = $this->CRUD_Widget_Purchases->get_week_transaction();

				$data['spending'] = $this->CRUD_Widget_Purchases->get_spending();
				$data['transaction'] = $this->CRUD_Widget_Purchases->get_transaction();

				$this->load->view('templates/header', $header);
				$this->load->view('pages/purchases', $data);
				$this->load->view('templates/footer');

			} else if ($value === 'detail' && !empty($id)) {

				$this->load->model('CRUD_Transaction');

				$header['title'] = "DaunPalm | Invoice";
				$data['list'] = $this->CRUD_Transaction->read_trans_purchases_list($id);
				$data['detail'] = $this->CRUD_Transaction->read_trans_purchases_detail($id);

				$this->load->view('templates/header', $header);
				$this->load->view('pages/purchases_detail', $data);
				$this->load->view('templates/footer');
			
			} else {

				redirect('error_404');
			}
		}

		public function sales($value = NULL, $id = NULL)
		{

			// if ($this->session->userdata('user_type') !== "Admin") {
				
			// 	header("location:javascript://history.go(-1)");
			// }

			if (empty($value)) {
				
				$header['title'] = "DaunPalm | Sales";
				$data['param'] = "sales";
				$data['today_income'] = $this->CRUD_Widget_Sales->get_today_income();
				$data['yesterday_income'] = $this->CRUD_Widget_Sales->get_yesterday_income();
				$data['last_3_income'] = $this->CRUD_Widget_Sales->get_last_income();
				$data['last_week_income'] = $this->CRUD_Widget_Sales->get_week_income();
				$data['today_transaction'] = $this->CRUD_Widget_Sales->get_today_transaction();
				$data['yesterday_transaction'] = $this->CRUD_Widget_Sales->get_yesterday_transaction();
				$data['last_3_transaction'] = $this->CRUD_Widget_Sales->get_last_transaction();
				$data['last_week_transaction'] = $this->CRUD_Widget_Sales->get_week_transaction();
				$data['today_menu_sales'] = $this->CRUD_Widget_Sales->get_today_menu_sales();
				$data['yesterday_menu_sales'] = $this->CRUD_Widget_Sales->get_yesterday_menu_sales();
				$data['last_3_menu_sales'] = $this->CRUD_Widget_Sales->get_last_menu_sales();
				$data['last_week_menu_sales'] = $this->CRUD_Widget_Sales->get_week_transaction();

				$data['income'] = $this->CRUD_Widget_Sales->get_income();
				$data['transaction'] = $this->CRUD_Widget_Sales->get_transaction();
				$data['menu_sales'] = $this->CRUD_Sold->get_monthly_sales();

				$this->load->view('templates/header', $header);
				$this->load->view('pages/sales', $data);
				$this->load->view('templates/footer');

			} else if ($value === 'detail' && !empty($id)) {

				$this->load->model('CRUD_Transaction');

				$header['title'] = "DaunPalm | Invoice";
				$data['list'] = $this->CRUD_Transaction->read_trans_sales_list($id);
				$data['detail'] = $this->CRUD_Transaction->read_trans_sales_detail($id);

				$this->load->view('templates/header', $header);
				$this->load->view('pages/sales_detail', $data);
				$this->load->view('templates/footer');
			
			} else {

				redirect('error_404');
			}
		}
	}