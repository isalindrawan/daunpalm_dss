<?php

	class Account extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			} else {

				if ($this->session->userdata('user_type') !== "Admin") {

					header("location:javascript://history.go(-1)");
				}
			}

			$this->load->model('CRUD_Account');

		}

		public function index()
		{

			$header['title'] = "DaunPalm | Account";
			$data['pegawai'] = $this->CRUD_Account->get_pegawai();
			$data['id'] = $this->CRUD_Account->get_id();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/account', $data);
			$this->load->view('templates/footer');
		}

		public function create()
		{
			
			$data = array(
				
				'ID_Account' => $this->input->post('id'),
				'Username' => $this->input->post('username'),
				'Password' => md5($this->input->post('password')),
				'Recovery' => md5($this->input->post('recovery')), 
				'Security' => $this->input->post('security'),
				'Answer' => $this->input->post('answer'),
				'Tipe' => $this->input->post('type'),
				'Status' => "Active",
				'Last' => NULL,
				'FO_ID_Peg' => $this->input->post('id_peg'),
			);

			$status = $this->CRUD_Account->create($data);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				redirect('account');
			}

			$this->session->set_flashdata('create', 'create');
			redirect('account');
		}

		public function update()
		{

			$id = $this->input->post('id');

			$data = array(
				
				'Tipe' => $this->input->post('type'), 
				'Status' => $this->input->post('status'), 
			);

			$status = $this->CRUD_Account->update($id, $data);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				redirect('account');
			}

			$this->session->set_flashdata('update', 'update');

			redirect('account');
		}
	}
