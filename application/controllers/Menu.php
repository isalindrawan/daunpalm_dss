<?php

	class Menu extends CI_Controller
	{

		public function __construct()
		{

			parent::__construct();

			if ($this->session->userdata('session_status') !== "Approved") {

				redirect(base_url());
		
			}
			
			$this->load->model('CRUD_Menu');
		}


		public function index()
		{

			$header['title'] = "DaunPalm | Menu";

			$data['total'] = $this->CRUD_Menu->get_total();
			$data['best'] = $this->CRUD_Menu->get_best_seller();
			$data['available'] = $this->CRUD_Menu->get_available();
			$data['unavailable'] = $this->CRUD_Menu->get_unavailable();
			$data['id'] = $this->CRUD_Menu->get_menu_id();
			$data['category'] = $this->CRUD_Menu->read_Category();

			$this->load->view('templates/header', $header);
			$this->load->view('pages/menu', $data);
			$this->load->view('templates/footer');
		}

		public function edit($id = NULL)
		{

			if (empty($id)) {
				redirect('error_404');
			}

			$header['title'] = "DaunPalm | Edit Menu";

			$data['category'] = $this->CRUD_Menu->read_Category();
			$data['menu'] = $this->CRUD_Menu->read_Menu($id);

			$this->load->view('templates/header', $header);
			$this->load->view('pages/menu_edit', $data);
			$this->load->view('templates/footer');
		}

		public function create()
		{
			
			$data = array(

				'ID_Menu' => $this->input->post('create_id'),
				'Nama' => $this->input->post('create_name'),
				'Harga' => $this->input->post('create_price'),
				'Status' => $this->input->post('create_status'),
				'FO_ID_Kategori' => $this->input->post('create_category')
			);

			$status = $this->CRUD_Menu->create_Menu($data);

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');
				
				unset($_POST);

				redirect('menu');	
			}

			$this->session->set_flashdata('create', 'create');

			unset($_POST);

			redirect('menu');
		}

		public function update()
		{

			$data = array(

				'Nama' => $this->input->post('name'),
				'Harga' => $this->input->post('price'),
				'FO_ID_Kategori' => $this->input->post('category')
			);

			$where = array(

				'ID_Menu' => $this->input->post('id')
			);

			$status = $this->CRUD_Menu->update_Menu($data, $where, 'Menu');

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

				unset($_POST);

 				redirect('menu');
			}

			$this->session->set_flashdata('update', 'update');

			unset($_POST);

 			redirect('menu');
		}

		public function update_status($status = NULL, $id = NULL)
		{

			if ($status == NULL || $id == NULL) {
				
				$this->session->set_flashdata('error', 'error');

				redirect('menu');
			
			}

			$data = array(

				'Status' => $status,
			);

			$where = array(

				'ID_Menu' => $id,
			);

			$status = $this->CRUD_Menu->update_Status($data, $where, 'Menu');

			if ($status === "Failed") {
				
				$this->session->set_flashdata('error', 'error');

 				redirect('menu');
			}

			$this->session->set_flashdata('update', 'update');

 			redirect('menu');
		}

		public function delete($id = NULL, $status = NULL )
		{

			if (empty($id) && empty($status)) {

				redirect('error_404');
			
			} elseif (empty($status)) {

				$this->session->set_flashdata('delete', 'delete');
				$this->session->set_flashdata('id', $id);

				redirect('menu');

			} elseif ($status == 'confirmed') {

				$status = $this->CRUD_Menu->delete_Menu($id);

				if ($status === "Failed") {

					$this->session->set_flashdata('error', 'error');

					unset($_POST);

					redirect('menu');
				}

				$this->session->set_flashdata('deleted', 'deleted');

				unset($_POST);

				redirect('menu');
			}
		}
	}
