<?php

	class CRUD_Transaction extends CI_Model
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
		}

		private function get_CurrentMonth()
		{
			$this->load->helper('date');

			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{
			$this->load->helper('date');

			$year = date("Y");

			return $year;
		}

		private function get_current_date()
		{

			return date('Y-m-d');
		}

		public function create($detail_trans, $item, $price, $quantity, $subtotal, $total)
		{
			$status = "Success";
			$tipe = "Create";
			$detail = "Purchases";

			$ID_TB = $this->read_ID_TB();
			$ID_TB = ++$ID_TB;

			$this->db->insert('Detail_Transaksi', $detail_trans);

			$TB_Data = array(

				'ID_TB' => $ID_TB,
				'Tgl' => $this->get_current_date(),
				'FO_ID_Peg' => $this->session->userdata('user_fo_id'),
				'FO_ID_DT' => $this->read_ID_DT(),
			);

			$this->db->insert('Transaksi_Beli', $TB_Data);

			for ($i=0; $i < $total; $i++) { 
				
				$id_tb_item = $this->read_ID_TB_Item();

				$data = array(

					'ID_TB_Item' => ++$id_tb_item,
					'Item' => $item[$i],
					'Harga' => $price[$i],
					'Quantity' => $quantity[$i],
					'Subtotal' => $subtotal[$i],
					'FO_ID_TB' => $ID_TB
				);

				echo '<script type="text/javascript">alert("'.$data['ID_TB_Item'].', '.$data['Item'].', '.$data['Harga'].', '.$data['Quantity'].', '.$data['Subtotal'].', '.$data['FO_ID_TB'].',")</script>';

				$this->db->insert('TB_Item', $data);
			}

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		public function read_ID_DT()
		{

			if (empty($this->db->query("SELECT ID_DT FROM Detail_Transaksi ORDER BY ID_DT DESC LIMIT 1")->row()->ID_DT)) {

				return "DT00000000";
			}

			return $this->db->query("SELECT ID_DT FROM Detail_Transaksi ORDER BY ID_DT DESC LIMIT 1")->row()->ID_DT;
		}
		public function read_ID_TB()
		{
			
			if (empty($this->db->query("SELECT ID_TB FROM Transaksi_Beli ORDER BY ID_TB DESC LIMIT 1")->row()->ID_TB)) {
				
				return "TB00000000";
			}

			return $this->db->query("SELECT ID_TB FROM Transaksi_Beli ORDER BY ID_TB DESC LIMIT 1")->row()->ID_TB;
		}

		public function read_ID_TB_Item()
		{

			if (empty($this->db->query("SELECT ID_TB_Item FROM TB_Item ORDER BY ID_TB_Item DESC LIMIT 1")->row()->ID_TB_Item)) {
				
				return "TBI0000000";
			}

			return $this->db->query("SELECT ID_TB_Item FROM TB_Item ORDER BY ID_TB_Item DESC LIMIT 1")->row()->ID_TB_Item;
		}

		public function read_trans_sales_detail($id)
		{

			$this->db->select('TJ.ID_TJ, TJ.Tgl, N.Nama_Depan, O.*, DT.*');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'ON TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->join('Pegawai AS P', 'TJ.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');

			$this->db->where('TJ.ID_TJ', $id);

			return $this->db->get()->row_array();
		}

		public function read_trans_sales_list($id)
		{

			$this->db->select('M.Nama AS Nama, M.Harga AS Harga, OI.Quantity AS Quantity, OI.Subtotal AS Subtotal');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M', 'ON OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('TJ.ID_TJ', $id);

			return $this->db->get()->result_array();
		}

		public function read_trans_purchases_detail($id)
		{

			$this->db->select('TB.ID_TB, TB.Tgl, N.Nama_Depan, DT.*, COUNT(TBI.Item) AS Total');
			$this->db->from('TB_Item AS TBI');
			$this->db->join('Transaksi_Beli AS TB', 'TBI.FO_ID_TB = TB.ID_TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->join('Pegawai AS P', 'TB.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');

			$this->db->where('TBI.FO_ID_TB', $id);

			return $this->db->get()->row_array();
		}

		public function read_trans_purchases_list($id)
		{

			$this->db->select('TBI.Item AS Item, TBI.Harga AS Harga, TBI.Quantity AS Quantity, TBI.Subtotal AS Subtotal');
			$this->db->from('TB_Item AS TBI');
			$this->db->join('Transaksi_Beli AS TB', 'TBI.FO_ID_TB = TB.ID_TB');
			$this->db->where('TBI.FO_ID_TB', $id);

			return $this->db->get()->result_array();
		}

		public function read_overview_dataset()
		{

			$query = $this->db->query('

				SELECT Income.Month, Income.Year, Income.Income, IFNULL(Spending.Spending, 0) AS Spending, (Income.Income - IFNULL(Spending.Spending, 0)) - (SELECT SUM(Gaji) FROM Pegawai WHERE Status = "Active") AS Profit FROM (SELECT * FROM (SELECT TJ.Tgl AS Tgl, DATE_FORMAT(TJ.Tgl, "%b") AS Month, DATE_FORMAT(TJ.Tgl, "%Y") AS Year, SUM(DT.Total) AS Income FROM Transaksi_Jual AS TJ INNER JOIN Detail_Transaksi AS DT ON TJ.FO_ID_DT = DT.ID_DT WHERE DT.Tipe = "Jual" GROUP BY DATE_FORMAT(TJ.Tgl, "%b"), DATE_FORMAT(TJ.Tgl, "%Y") ORDER BY TJ.Tgl DESC LIMIT 12) AS Monthly_Income GROUP BY Monthly_Income.Month ORDER BY `Tgl` ASC) AS Income LEFT OUTER JOIN (SELECT * FROM (SELECT TB.Tgl AS Tgl, DATE_FORMAT(TB.Tgl, "%b") AS Month_Spending, DATE_FORMAT(TB.Tgl, "%Y") AS Year_Spending, SUM(DT.Total) AS Spending FROM Transaksi_Beli AS TB INNER JOIN Detail_Transaksi AS DT ON TB.FO_ID_DT = DT.ID_DT GROUP BY DATE_FORMAT(TB.Tgl, "%b"), DATE_FORMAT(TB.Tgl, "%Y") ORDER BY TB.Tgl DESC LIMIT 12) AS Monthly_Spending GROUP BY Monthly_Spending.Month_Spending ORDER BY `Tgl` ASC) AS Spending ON Income.Month = Spending.Month_Spending GROUP BY Income.Month, Income.Year ORDER BY Income.Tgl ASC LIMIT 12

				');

			return $query->result_array();
		}

		public function read_monthly_spending()
		{

			$month = get_CurrentMonth();
			$year = get_CurrentYear();
			
			$this->db->select('SUM(DT.Total) AS Spending');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('MONTH(TB.Tgl)', $month);
			$this->db->where('YEAR(TB.Tgl)', $year);

			$query = $this->db->get();

			if (empty($query->row()->Spending)) {
				
				return "0";
			}

			return $query->row()->Spending;

		}

		public function read_monthly_income()
		{
			$month = get_CurrentMonth();
			$year = get_CurrentYear();

			$this->db->select('SUM(DT.Total) AS Income');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('MONTH(TJ.Tgl)', $month);
			$this->db->where('YEAR(TJ.Tgl)', $year);

			$query = $this->db->get();

			if (empty($query->row()->Income)) {
				
				return "0";
			}

			return $query->row()->Income;
		}

		public function read_monthly_profit()
		{

			$query = $this->db->query('SELECT (Income-IFNULL(Spending, 0)) AS profit FROM 
			(SELECT TJ.Tgl AS Tgl_TJ, DATE_FORMAT(TJ.Tgl, "%b") AS Month, DATE_FORMAT(TJ.Tgl, "%Y") 
			AS Year, SUM(DT.Total) AS Income FROM Transaksi_Jual AS TJ INNER JOIN Detail_Transaksi 
			AS DT ON TJ.FO_ID_DT = DT.ID_DT WHERE DT.Tipe = "Jual" GROUP BY DATE_FORMAT(TJ.Tgl, "%b"), 
			DATE_FORMAT(TJ.Tgl, "%Y") ORDER BY TJ.Tgl DESC LIMIT 12) AS Monthly_Income LEFT JOIN 
			(SELECT TB.Tgl AS Tgl_TB, DATE_FORMAT(TB.Tgl, "%b") AS Month_Spending, 
			DATE_FORMAT(TB.Tgl, "%Y") AS Year_Spending, SUM(DT.Total) AS Spending FROM 
			Transaksi_Beli AS TB INNER JOIN Detail_Transaksi AS DT ON TB.FO_ID_DT = DT.ID_DT 
			GROUP BY DATE_FORMAT(TB.Tgl, "%b"), DATE_FORMAT(TB.Tgl, "%Y") ORDER BY TB.Tgl DESC LIMIT 12) 
			AS Monthly_Spending ON Monthly_Income.Month = Monthly_Spending.Month_Spending 
			ORDER BY `Tgl_TJ` ASC');

			return $query->result();
		}
	}
