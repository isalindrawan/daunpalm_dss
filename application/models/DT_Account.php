<?php

	class DT_Account extends CI_Model
	{
		var $column_order = array('A.ID_Account', 'N.Nama_Depan', 'A.Username', 'A.Password', 'A.Tipe', 'A.Status');

		var $column_search = array('A.ID_Account', 'N.Nama_Depan', 'A.Username', 'A.Password', 'A.Tipe', 'A.Status');

		var $order = array('ID_Account' => 'asc');
		var $table = 'Account';

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
		}

		private function get_datatables_query()
		{

			$this->db->select('A.*, N.Nama_Depan, N.Nama_Belakang');
			$this->db->from($this->table.' AS A');
			$this->db->join('Pegawai AS P', 'A.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');

			$i = 0;

			foreach($this->column_search AS $item) {

	            if($_POST['search']['value'])
	            {
	                 
	                if($i===0)
	                {

	                    $this->db->group_start();
	                    $this->db->like($item, $_POST['search']['value']);
	                
	                }

	                else
	                {

	                    $this->db->or_like($item, $_POST['search']['value']);
	                }
	 
	            	if(count($this->column_search) - 1 == $i)
	                    $this->db->group_end();
	            }
	            
	            $i++;
	        }
	         
	        if(isset($_POST['order']))
	        {

	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

	        }

	        else if(isset($this->order))
	        {

	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
		}

		public function get_datatables()
		{

	        $this->get_datatables_query();
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_filtered()
	    {

	        $this->get_datatables_query();
	        $query = $this->db->get();
	        return $query->num_rows();
	    }
	 
	    public function count_all()
	    {

	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
	}