<?php

	class CRUD_Employee extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Category');
		}

		public function create($name, $address, $employee)
		{

			$status = "Success";
			$tipe = "Create";
			$detail = "Employee";

			$this->db->insert('Nama', $name);
			$this->db->insert('Alamat', $address);
			$this->db->insert('Pegawai', $employee);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
			
			return $status;
		}

		public function read($id)
		{

			return $this->db->query("SELECT P.*, N.*, A.* FROM Pegawai AS P INNER JOIN Nama AS N ON P.FO_ID_Nama = N.ID_Nama INNER JOIN Alamat AS A ON P.FO_ID_Alamat = A.ID_Alamat WHERE P.ID_Peg = '".$id."'")->row_array();
		}

		public function update($nama, $alamat, $pegawai, $where)
		{

			$this->load->helper('array');

			$status = "Success";
			$tipe = "Update";
			$detail = "Employee";

			$this->db->where('ID_Nama', element('ID_Nama', $where));
			$this->db->update('Nama',$nama);

			$this->db->where('ID_Alamat', element('ID_Alamat', $where));
			$this->db->update('Alamat',$alamat);

			$this->db->where('ID_Peg', element('ID_Peg', $where));
			$this->db->update('Pegawai',$pegawai);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		public function delete($nama, $alamat, $pegawai)
		{

			$status = "Success";
			$tipe = "Delete";
			$detail = "Employee";

			$this->db->delete('Pegawai', array('ID_Peg' => $pegawai));
			$this->db->delete('Nama', array('ID_Nama' => $nama));
			$this->db->delete('Alamat', array('ID_Alamat' => $alamat));

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

		    return $status;
		}

		public function get_peg_id()
		{
			
			if (empty($this->db->query("SELECT ID_Peg FROM Pegawai ORDER BY ID_Peg DESC LIMIT 1")->row()->ID_Peg)) {
				
				return "PEG0000000";
			}

			return $this->db->query("SELECT ID_Peg FROM Pegawai ORDER BY ID_Peg DESC LIMIT 1")->row()->ID_Peg;
		}

		public function get_alamat_id()
		{
			
			if (empty($this->db->query("SELECT ID_Alamat FROM Alamat ORDER BY ID_Alamat DESC LIMIT 1")->row()->ID_Alamat)) {
				
				return "ALM0000000";
			}

			return $this->db->query("SELECT ID_Alamat FROM Alamat ORDER BY ID_Alamat DESC LIMIT 1")->row()->ID_Alamat;
		}

		public function get_nama_id()
		{

			if (empty($this->db->query("SELECT ID_Nama FROM Nama ORDER BY ID_Nama DESC LIMIT 1")->row()->ID_Nama)) {

				return "NAM0000000";
			}
			
			return $this->db->query("SELECT ID_Nama FROM Nama ORDER BY ID_Nama DESC LIMIT 1")->row()->ID_Nama;
		}

		public function get_total()
		{

			return $this->db->count_all_results('Pegawai');
		}

		public function get_active()
		{

			return $this->db->where('Status', 'Active')->get('Pegawai')->num_rows();
		}

		public function get_deactive()
		{

			return $this->db->where('Status', 'Deactive')->get('Pegawai')->num_rows();
		}

		public function get_salary()
		{

			return $this->db->select('SUM(Gaji) AS salary')->where('Status', 'Active')->get('Pegawai')->row_array();
		}
	}