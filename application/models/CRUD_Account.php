<?php
	class CRUD_Account extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
		}

		private function get_current_timestamp()
		{

			return date('Y-m-d H:i:s');
		}

		public function create($data)
		{

			$status = "Success";
			$tipe = "Create";
			$detail = "Account";

			$this->db->insert('Account', $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
			
			return $status;
		}

		public function update($id, $data = NULL)
		{

			if (empty($data)) {
				
				$temp = array(
					'Last' => $this->get_current_timestamp()
				);

				$this->db->where('ID_Account', $id);
				$this->db->update('Account', $temp);
			
			} else {

				$status = "Success";
				$tipe = "Update";
				$detail = "Account";

				$this->db->where('ID_Account', $id);
				$this->db->update('Account', $data);

				$error = $this->db->error();

				if ($error['code'] !== 0) {

					$status = "Failed";
				}

				$this->CRUD_Activity->create($tipe, $detail, $status);

				return $status;
			}
		}

		public function get_id()
		{
			if (empty($this->db->query("SELECT ID_Account FROM Account ORDER BY ID_Account DESC LIMIT 1")->row()->ID_Account)) {
				
				return "AID000000";
			}

			return $this->db->query("SELECT ID_Account FROM Account ORDER BY ID_Account DESC LIMIT 1")->row()->ID_Account;
		}

		public function get_pegawai()
			{
				
				$this->db->select('P.ID_Peg, N.Nama_Depan, N.Nama_Belakang, A.ID_Account');
				$this->db->from('Pegawai AS P');
				$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');
				$this->db->join('Account AS A', 'A.FO_ID_Peg = P.ID_Peg', 'Left');
				$this->db->where('P.Status', 'Active');
				$this->db->where('ID_Account IS NULL');
				
				return $this->db->get()->result_array();
			}
	}