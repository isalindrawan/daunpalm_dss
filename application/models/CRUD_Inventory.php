<?php

	class CRUD_Inventory extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->helper('array');
			$this->load->model('CRUD_Activity');
		}

		public function create($item, $quantity, $total)
		{

			$status = "Success";
			$tipe = "Create";
			$detail = "Inventory";

			$id = $this->get_id();
			
			for ($i=0; $i < $total; $i++) { 
				
				$data = array(
					'ID_Inventory' => ++$id,
					'Item' => $item[$i],
					'Sisa' => $quantity[$i]
				);

				$this->db->insert('Inventory', $data);

				$error = $this->db->error();

			    if ($error['code'] !== 0) {
			    	
			        $status = "Failed";

			        break;
			    }
			}

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		public function update($inventory, $where)
		{

			$status = "Success";
			$tipe = "Update";
			$detail = "Inventory";

			$this->db->where('ID_Inventory', element('ID_Inventory', $where));
			$this->db->update('Inventory', $inventory);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		public function get_id()
		{

			if (empty($this->db->query("SELECT ID_Inventory FROM Inventory ORDER BY ID_Inventory DESC LIMIT 1")->row_array())) {
				
				return "INV0000000";
			}
			
			return $this->db->query("SELECT ID_Inventory FROM Inventory ORDER BY ID_Inventory DESC LIMIT 1")->row()->ID_Inventory;
		}

		public function get_total()
		{
			return $this->db->count_all_results('Inventory');
		}

		public function get_last_update()
		{

			if (empty($this->db->query("SELECT Tgl_Update FROM Inventory ORDER BY Tgl_Update DESC LIMIT 1")->row()->Tgl_Update)) {
				
				return NULL;
			}
			
			return $this->db->query("SELECT Tgl_Update FROM Inventory ORDER BY Tgl_Update DESC LIMIT 1")->row()->Tgl_Update;
		}

		public function get_low()
		{
			
			$this->db->where('Sisa <', '5');
			$this->db->where('Sisa <>', '0');

			return $this->db->get('Inventory')->num_rows();
		}

		public function get_stock()
		{
			
			return $this->db->where('Sisa', '0')->get('Inventory')->num_rows();
		}
	}