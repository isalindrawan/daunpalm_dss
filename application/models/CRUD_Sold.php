<?php 
	
	class CRUD_Sold extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function get_CurrentMonth()
		{
			$this->load->helper('date');

			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{
			$this->load->helper('date');

			$year = date("Y");

			return $year;
		}

		public function get_datasets()
		{
			
			$this->db->select('M.Nama AS Nama, SUM(OI.Quantity) AS Quantity');
			$this->db->group_by('M.ID_Menu');
			$this->db->order_by('Quantity', 'desc');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->limit('10');

			return $this->db->get()->result_array();
		}

		public function get_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_menu_sales()
		{

			$this->db->select('M.ID_Menu');
			$this->db->group_by('M.ID_Menu');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');

			$query = $this->db->get();

			if (empty($query->num_rows())) {
				
				return "0";
			}

			return $query->num_rows();
		}

		public function get_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');

			return $this->db->get()->num_rows();
		}

		public function get_daily_datasets()
		{
			
			$this->db->select('M.Nama AS Nama, SUM(OI.Quantity) AS Quantity');
			$this->db->group_by('M.ID_Menu');
			$this->db->order_by('Quantity', 'desc');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('TJ.Tgl = CURDATE()');
			$this->db->limit('10');

			return $this->db->get()->result_array();
		}

		public function get_daily_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl = CURDATE()');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_daily_menu_sales()
		{

			$this->db->select('M.Nama');
			$this->db->group_by('M.Nama');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('TJ.Tgl = CURDATE()');

			return $this->db->get()->num_rows();
		}

		public function get_daily_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl = CURDATE()');

			return $this->db->get()->num_rows();
		}

		public function get_monthly_datasets()
		{

			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();
			
			$this->db->select('M.Nama AS Nama, SUM(OI.Quantity) AS Quantity');
			$this->db->group_by('M.ID_Menu');
			$this->db->order_by('Quantity', 'desc');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);
			$this->db->limit('10');

			return $this->db->get()->result_array();
		}

		public function get_monthly_sales()
		{

			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_monthly_menu_sales()
		{
			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();

			$this->db->select('M.ID_Menu');
			$this->db->group_by('M.ID_Menu');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);

			return $this->db->get()->num_rows();
		}

		public function get_monthly_transaction()
		{

			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);

			return $this->db->get()->num_rows();
		}
	}