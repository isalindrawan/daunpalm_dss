<?php
	class CRUD_Activity extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->database();
		}

		private function get_current_timestamp()
		{

			return date('Y-m-d H:i:s');
		}

		public function create($Tipe, $Detail, $Status)
		{

			$uid = $this->session->userdata('user_id');

			$id = $this->get_ID();
			$date = $this->get_current_timestamp();

			$data = array(
				'ID_Activity' => ++$id,
				'Tgl' => $date,
				'Tipe' => $Tipe,
				'Detail' => $Detail,
				'Status' => $Status,
				'FO_ID_Account' => $uid
			);

			$this->db->insert('Activity', $data);
		}

		public function get_ID()
		{
			if (empty($this->db->query("SELECT ID_Activity FROM Activity ORDER BY ID_Activity DESC LIMIT 1")->row_array())) {

				return "LOG0000000";
			}
			
			return $this->db->query("SELECT ID_Activity FROM Activity ORDER BY ID_Activity DESC LIMIT 1")->row()->ID_Activity;
		}
	}