<?php

	class CRUD_Widget_Sales extends CI_Model
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
		}

		private function get_CurrentMonth()
		{
			$this->load->helper('date');

			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{
			$this->load->helper('date');

			$year = date("Y");

			return $year;
		}

		public function read_monthly_income_dataset()
		{

			$query = $this->db->query('SELECT * FROM (SELECT TJ.Tgl AS Tgl, DATE_FORMAT(TJ.Tgl, "%b") AS Month, DATE_FORMAT(TJ.Tgl, "%Y") AS Year, SUM(DT.Total) AS Income FROM Transaksi_Jual AS TJ INNER JOIN Detail_Transaksi AS DT ON TJ.FO_ID_DT = DT.ID_DT WHERE DT.Tipe = "Jual" GROUP BY DATE_FORMAT(TJ.Tgl, "%b"), DATE_FORMAT(TJ.Tgl, "%Y") ORDER BY TJ.Tgl DESC LIMIT 12) AS Monthly_Income ORDER BY `Tgl` ASC');

			return $query->result_array();
		}

		public function get_today_income()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('TJ.Tgl = CURDATE()');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_yesterday_income()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_last_income()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -2 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_week_income()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -6 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_today_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl = CURDATE()');

			return $this->db->get()->num_rows();
		}

		public function get_yesterday_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_last_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -2 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_week_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -6 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_today_menu_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl = CURDATE()');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_yesterday_menu_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_last_menu_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -2 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_week_menu_sales()
		{

			$this->db->select('TJ.Tgl, SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl = DATE_ADD(CURDATE(), INTERVAL -6 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_income()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('month(TJ.Tgl)', $this->get_CurrentMonth());
			$this->db->where('year(TJ.Tgl)', $this->get_CurrentYear());

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_transaction()
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('month(TJ.Tgl)', $this->get_CurrentMonth());
			$this->db->where('year(TJ.Tgl)', $this->get_CurrentYear());

			return $this->db->get()->num_rows();
		}
	}
