<?php

class DT_Sales extends CI_Model
{

	var $column_order = array('M.ID_Menu', 'M.Nama', 'Quantity', 'Subtotal', 'Tgl');

	var $column_search = array('M.ID_Menu', 'M.Nama', 'Quantity', 'Subtotal', 'Tgl');

	var $order = array('Tgl' => 'desc');
	var $table = 'Transaksi_Jual';

	public function __construct()
	{

		parent::__construct();
		$this->load->database();
	}

	private function get_CurrentMonth()
	{
		$this->load->helper('date');

		$month = date("m");

		return $month;
	}

	private function get_CurrentYear()
	{
		$this->load->helper('date');

		$year = date("Y");

		return $year;
	}

	private function get_datatables_query($value)
	{

		$this->db->select('M.ID_Menu, M.Nama, SUM(OI.Quantity) AS Quantity, SUM(OI.Subtotal) AS Subtotal, MAX(TJ.Tgl) AS Tgl');
		$this->db->group_by('M.ID_Menu');
		$this->db->from($this->table.' AS TJ');
		$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
		$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
		$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');

		if ($value == 'monthly') {

			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();

			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);
		}

		else if ($value == 'daily') {

			$this->db->where('TJ.Tgl = CURDATE()');
		}

		$i = 0;

		foreach($this->column_search AS $item) {

			if($_POST['search']['value'])
			{

				if($i===0)
				{

					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);

				}

				else
				{

					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}

			$i++;
		}

		if(isset($_POST['order']))
		{

			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

		}

		else if(isset($this->order))
		{

			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables($value)
	{

		$params = $value;

		$this->get_datatables_query($params);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered($value)
	{

		$this->get_datatables_query($value);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{

		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
}
