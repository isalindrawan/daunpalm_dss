<?php

	class CRUD_Dashboard extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();

			$this->load->database();
		}

		public function get_current_date()
		{
			$this->load->helper('date');

			return date('F jS Y');
		}

		public function get_current_time()
		{

			return date('H:i A');
		}

		public function read_income_spending_datasets()
		{

			$query = $this->db->query('

				SELECT Income.Month, Income.Year, Income.Income, IFNULL(Spending.Spending, 0) AS Spending FROM (SELECT * FROM (SELECT TJ.Tgl AS Tgl, DATE_FORMAT(TJ.Tgl, "%b") AS Month, DATE_FORMAT(TJ.Tgl, "%Y") AS Year, SUM(DT.Total) AS Income FROM Transaksi_Jual AS TJ INNER JOIN Detail_Transaksi AS DT ON TJ.FO_ID_DT = DT.ID_DT WHERE DT.Tipe = "Jual" GROUP BY DATE_FORMAT(TJ.Tgl, "%b"), DATE_FORMAT(TJ.Tgl, "%Y") ORDER BY TJ.Tgl DESC LIMIT 12) AS Monthly_Income GROUP BY Monthly_Income.Month ORDER BY `Tgl` ASC) AS Income LEFT OUTER JOIN (SELECT * FROM (SELECT TB.Tgl AS Tgl, DATE_FORMAT(TB.Tgl, "%b") AS Month_Spending, DATE_FORMAT(TB.Tgl, "%Y") AS Year_Spending, SUM(DT.Total) AS Spending FROM Transaksi_Beli AS TB INNER JOIN Detail_Transaksi AS DT ON TB.FO_ID_DT = DT.ID_DT GROUP BY DATE_FORMAT(TB.Tgl, "%b"), DATE_FORMAT(TB.Tgl, "%Y") ORDER BY TB.Tgl DESC LIMIT 12) AS Monthly_Spending GROUP BY Monthly_Spending.Month_Spending ORDER BY `Tgl` ASC) AS Spending ON Income.Month = Spending.Month_Spending GROUP BY Income.Month, Income.Year ORDER BY Income.Tgl ASC LIMIT 12

				');

			return $query->result_array();
		}
	}

	// 