<?php

	class CRUD_Report extends CI_Model
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
			$this->load->helper('date');
			$this->load->model('CRUD_Activity');

		}

		private function get_CurrentMonth()
		{
			
			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{

			$year = date("Y");

			return $year;
		}

		private function get_current_date()
		{

			return date('Y-m-d');
		}

		public function generate_report($start, $end)
		{

			// $id = $this->get_report_id();

			// if(date('d', strtotime($this->get_current_date() .' +1 day')) == '01') {
				
				$data = array(

					'Res_Total' => $this->get_res_total($start, $end),
					'Res_Done' => $this->get_res_finished($start, $end),
					'Res_Cancelled' => $this->get_res_cancelled($start, $end),
					'In_Menu_Sold' => $this->get_in_menu_sold($start, $end),
					'In_Total_Sold' => $this->get_in_total_sold($start, $end),
					'In_Sales_Trans' => $this->get_in_sales_trans($start, $end),
					'In_Total' => $this->get_in_total($start, $end),
					'Spend_Item' => $this->get_spend_item($start, $end),
					'Spend_Qty' => $this->get_spend_item_qty($start, $end),
					'Spend_Trans' => $this->get_spend_trans($start, $end),
					'Spend_Purchase' => $this->get_spend_purchase($start, $end),
					'Spend_Salary' => $this->get_salary($start, $end),
					'Spend_Total' => $this->get_spend_purchase($start, $end)+$this->get_salary($start, $end),
					'Profit' => $this->get_profit($start, $end)
					
				);

				return $data;
		}

		public function get_report_hist()
		{

			$this->db->select('Report_Date, ID_Report');
			$this->db->from('Report');

			$query = $this->db->get();
			
			return $query->result_array();
			
		}

		public function get_report($id)
		{
			$this->db->where('ID_Report', $id);
			$this->db->from('Report');

			$query = $this->db->get();
			
			return $query->result_array();
		}

		public function get_report_id()
		{
			if (empty($this->db->query("SELECT ID_Report FROM Report ORDER BY ID_Report DESC LIMIT 1")->row()->ID_Report)) {
				
				return "REP0000000";
			}

			return $this->db->query("SELECT ID_Report FROM Report ORDER BY ID_Report DESC LIMIT 1")->row()->ID_Report;
		}

		public function get_res_total($start, $end)
		{

			$this->db->where('Tgl_Kunjungan BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$this->db->from('Reservasi');
			
			return $this->db->get()->num_rows();
		}

		public function get_res_finished($start, $end)
		{

			$this->db->where('Status', 'Finished');
			$this->db->where('Tgl_Kunjungan BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$this->db->from('Reservasi');

			return $this->db->get()->num_rows();
		}

		public function get_res_cancelled($start, $end)
		{

			$this->db->where('Status', 'Cancelled');
			$this->db->where('Tgl_Kunjungan BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$this->db->from('Reservasi');

			return $this->db->get()->num_rows();
		}

		public function get_in_total_sold($start, $end)
		{

			$this->db->select('SUM(OI.Quantity) AS Quantity');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			$query = $this->db->get();

			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_in_menu_sold($start, $end)
		{

			$this->db->select('M.ID_Menu');
			$this->db->group_by('M.ID_Menu');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			return $this->db->get()->num_rows();
		}


		public function get_in_sales_trans($start, $end)
		{

			$this->db->select('TJ.ID_TJ');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			return $this->db->get()->num_rows();
		}

		public function get_in_total($start, $end)
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_spend_purchase($start, $end)
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_spend_trans($start, $end)
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			return $this->db->get()->num_rows();
		}

		public function get_spend_item($start, $end)
		{
			
			$this->db->select('TB_Item.Item');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('TB_Item', 'TB_Item.FO_ID_TB = TB.ID_TB');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			return $this->db->get()->num_rows();
		}

		public function get_spend_item_qty($start, $end)
		{
			
			$this->db->select('SUM(TB_Item.Quantity) AS Quantity');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('TB_Item', 'TB_Item.FO_ID_TB = TB.ID_TB');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$query = $this->db->get();
			
			if (empty($query->row()->Quantity)) {
				
				return "0";
			}

			return $query->row()->Quantity;
		}

		public function get_salary($start, $end)
		{

			return $this->db->select('SUM(Gaji) AS salary')->where('Status', 'Active')->get('Pegawai')->row()->salary;
		}

		public function get_profit($start, $end)
		{

			$profit = $this->get_in_total($start, $end) - ($this->get_spend_purchase($start, $end) + $this->get_salary($start, $end));
			
			return $profit;
		}

		public function get_res($start, $end)
		{

			$this->db->where('Tgl_Kunjungan BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			$this->db->from('Reservasi');
			
			return $this->db->get()->result_array();
		}

		public function get_trans_sales($start, $end) {

			$this->db->select('TJ.ID_TJ AS ID, O.Customer AS Customer, TJ.Tgl AS Date, DT.Total AS Total');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->join('Pegawai AS P', 'TJ.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->where('DT.Tipe', 'Jual');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			return $this->db->get()->result_array();
		}

		public function get_trans_purchases($start, $end) {

			$this->db->select('TB.ID_TB AS ID, N.Nama_Depan AS Name, TB.Tgl As Date, DT.Total AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->join('Pegawai AS P', 'TB.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');
			$this->db->where('DT.Tipe', 'Beli');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');

			return $this->db->get()->result_array();

		}

		public function get_menu_sales($start, $end) {

			$this->db->select('M.ID_Menu AS ID, M.Nama AS Menu, SUM(OI.Quantity) AS Quantity, SUM(OI.Subtotal) AS Subtotal, MAX(TJ.Tgl) AS Date');
			$this->db->group_by('M.ID_Menu');
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');
			$this->db->where('TJ.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			return $this->db->get()->result_array();
		}

		public function get_purchase_item($start, $end) {

			$this->db->select('TBI.Item AS Item, TBI.Harga AS Harga, TBI.Quantity AS Quantity, TBI.Subtotal AS Subtotal, TB.Tgl AS Date');
			$this->db->from('TB_Item AS TBI');
			$this->db->join('Transaksi_Beli AS TB', 'TBI.FO_ID_TB = TB.ID_TB');
			$this->db->where('TB.Tgl BETWEEN "'. date('Y-m-d', strtotime($start)). '" and "'. date('Y-m-d', strtotime($end)).'"');
			
			return $this->db->get()->result_array();
		}
	}
