<?php

	class DT_Transaction_Sales extends CI_Model
	{

		var $column_order = array('TJ.ID_TJ',	'N.Nama_Depan', 'O.Customer', 'TJ.Tgl', 'DT.Total');

		var $column_search = array('TJ.ID_TJ',	'N.Nama_Depan', 'O.Customer', 'TJ.Tgl', 'DT.Total');

		var $order = array('TJ.ID_TJ' => 'desc');
		var $table = 'Transaksi_Jual';

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
		}

		private function get_datatables_query()
		{

			$this->db->select('TJ.ID_TJ, N.Nama_Depan, O.Customer, TJ.Tgl, DT.Total');
			$this->db->from($this->table.' AS TJ');
			$this->db->join('Detail_Transaksi AS DT', 'TJ.FO_ID_DT = DT.ID_DT');
			$this->db->join('Pegawai AS P', 'TJ.FO_ID_Peg = P.ID_Peg');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->where('DT.Tipe', 'Jual');
			
			$i = 0;

			foreach($this->column_search AS $item) {

	            if($_POST['search']['value'])
	            {

	                if($i===0)
	                {

	                    $this->db->group_start();
	                    $this->db->like($item, $_POST['search']['value']);

	                }

	                else
	                {

	                    $this->db->or_like($item, $_POST['search']['value']);
	                }

	            	if(count($this->column_search) - 1 == $i)
	                    $this->db->group_end();
	            }

	            $i++;
	        }

	        if(isset($_POST['order']))
	        {

	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

	        }

	        else if(isset($this->order))
	        {

	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
		}

		public function get_datatables()
		{

	        $this->get_datatables_query();
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }

	    public function count_filtered()
	    {

	        $this->get_datatables_query();
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all()
	    {

	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
	}
