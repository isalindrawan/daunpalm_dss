<?php

	class CRUD_Register extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->helper('date');
			$this->load->helper('array');
			$this->load->model('CRUD_Activity');
		}

		private function get_current_date()
		{

			return date('Y-m-d');
		}

		private function get_current_timestamp()
		{

			return date('Y-m-d H:i:s');
		}

		public function create($data, $dataID, $dataQty, $dataSub)
		{
			$status = "Success";
			$tipe = "Create";
			$detail = "Sales";
			
			
			$totaldata = count($dataID);

			$date = $this->get_current_date();
			$timestamp = $this->get_current_timestamp();
			$id_order = $this->read_order_id();
			$ID_DT = $this->read_detail_trans_id();
			$id_tj = $this->read_tj_id();

			$Order = array(
				'ID_Order' => ++$id_order,
				'Customer' => $data['Customer'],
				'Meja' => $data['Meja'],
				'Waktu' => $timestamp
			);

			$Detail = array(
				
				'ID_DT' => ++$ID_DT,
				'Total' => $data['Total'],
				'Bayar' => $data['Bayar'],
				'Kembali' => $data['Kembali'],
				'Tipe' => "Jual"
			);

			$this->db->insert('Order', $Order);
			$this->db->insert('Detail_Transaksi', $Detail);

			for ($i=0; $i < $totaldata; $i++) { 
				
				$id_order_item = $this->read_order_item_id();
				$FO_ID_Order = $this->read_order_id();

				$OrderItem = array(

					'ID_Order_Item' => ++$id_order_item,
					'FO_ID_Order' => $FO_ID_Order,
					'FO_ID_Menu' => $dataID[$i],
					'Quantity' => $dataQty[$i],
					'Subtotal' => $dataSub[$i]
				);

				$this->db->insert('Order_Item', $OrderItem);
			}

			$id_order = $this->read_order_id();
			$FO_ID_DT = $this->read_detail_trans_id();

			$Transaction = array(

				'ID_TJ' => ++$id_tj,
				'FO_ID_Peg' => $this->session->userdata('user_fo_id'),
				'Tgl' => $date,
				'FO_ID_Order' => $id_order,
				'FO_ID_DT' => $FO_ID_DT
			);

			$this->db->insert('Transaksi_Jual', $Transaction);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
		}

		public function read_Menu()
		{

			$query = $this->db->query('SELECT Menu.ID_Menu AS ID_Menu, Menu.Nama AS Nama, Menu.Harga AS Harga, Menu.Status AS Status, Kategori.ID_Kategori AS ID_Kategori, Kategori.Nama AS Head FROM Menu INNER JOIN Kategori ON Menu.FO_ID_Kategori = Kategori.ID_Kategori ORDER BY Kategori.Nama, Menu.Nama ASC');

			return $query->result_array();
		}

		public function read_Category()
		{

			return $this->db->get('Kategori')->result_array();
		}

		public function read_order_id()
		{

			if (empty($this->db->query("SELECT `ID_Order` FROM `Order` ORDER BY `ID_Order` DESC LIMIT 1")->row()->ID_Order)) {
				
				return "OR00000000";
			}

			return $this->db->query("SELECT `ID_Order` FROM `Order` ORDER BY `ID_Order` DESC LIMIT 1")->row()->ID_Order;
		}

		public function read_order_item_id()
		{

			if (empty($this->db->query("SELECT ID_Order_Item FROM Order_Item ORDER BY ID_Order_Item DESC LIMIT 1")->row()->ID_Order_Item)) {
			
				return "OI00000000";
			}

			return $this->db->query("SELECT ID_Order_Item FROM Order_Item ORDER BY ID_Order_Item DESC LIMIT 1")->row()->ID_Order_Item;
		}

		public function read_tj_id()
		{

			if (empty($this->db->query("SELECT ID_TJ FROM Transaksi_Jual ORDER BY ID_TJ DESC LIMIT 1")->row()->ID_TJ)) {
				
				return "TJ00000000";
			}

			return $this->db->query("SELECT ID_TJ FROM Transaksi_Jual ORDER BY ID_TJ DESC LIMIT 1")->row()->ID_TJ;
		}

		public function read_detail_trans_id()
		{

			if (empty($this->db->query("SELECT ID_DT FROM Detail_Transaksi ORDER BY ID_DT DESC LIMIT 1")->row()->ID_DT)) {
				
				return "DT00000000";
			}
			
			return $this->db->query("SELECT ID_DT FROM Detail_Transaksi ORDER BY ID_DT DESC LIMIT 1")->row()->ID_DT;
		}

	}