<?php

	class CRUD_Menu extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
		}

		public function create_Menu($data)
		{

			$status = "Success";
			$tipe = "Create";
			$detail = "Menu";

			$this->db->insert('Menu', $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		// public function create_Ingredient($data)
		// {

		// 	$status = "Success";
		// 	$tipe = "Create";
		// 	$detail = "Ingredient";

		// 	$this->db->insert('Bahan', $data);

		// 	$error = $this->db->error();

		//     if ($error['code'] !== 0) {
		    	
		//         $status = "Failed";
		//     }

		// 	$this->CRUD_Activity->create($tipe, $detail, $status);

		// 	return $status;
		// }

		public function read_Menu($id)
		{

			return $this->db->query("SELECT Menu.ID_Menu, Menu.Nama, Kategori.Nama AS Nama_Kategori, Menu.Harga, Menu.Status FROM Menu INNER JOIN Kategori ON Menu.FO_ID_Kategori = Kategori.ID_Kategori WHERE Menu.ID_Menu = '".$id."'")->row_array();
		}

		public function read_Category()
		{

			return $this->db->select('ID_Kategori, Nama AS Nama_Kategori')->get('Kategori')->result_array();
		}

		// public function read_Inventory()
		// {

		// 	return $this->db->select('ID_Inventory, Item')->get('Inventory')->result_array();
		// }

		// public function read_Ingredient($id)
		// {

		// 	$this->db->select('ID_Bahan, FO_ID_Menu, I.Item AS Item, I.Sisa AS Sisa, I.ID_Inventory AS ID_Inventory');
		// 	$this->db->where('Bahan.FO_ID_Menu', $id);
		// 	$this->db->from('Bahan');
		// 	$this->db->join('Inventory AS I', 'Bahan.Prime_ID_Inventory = I.ID_Inventory');

		// 	return $this->db->get()->result_array();
		// }

		public function update_Menu($data, $where, $table)
		{

			$status = "Success";
			$tipe = "Update";
			$detail = "Menu";
			
			$this->db->where($where);
			$this->db->update($table,$data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		public function update_Status($data, $where, $table)
		{

			$status = "Success";
			$tipe = "Update";
			$detail = "Menu";
			
			$this->db->where($where);
			$this->db->update($table, $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		// public function update_Ingredients($data, $id_menu)
		// {

		// 	$status = "Success";
		// 	$tipe = "Update";
		// 	$detail = "Ingredients";
		// 	$total = count($data);

		// 	for ($i=0; $i < $total ; $i++) { 
				
		// 		$id = $this->get_ingredient_id();

		// 		$ingredients = array(
		// 			'ID_Bahan' => ++$id,
		// 			'FO_ID_Menu' => $id_menu,
		// 			'Prime_ID_Inventory' => $data[$i] 
		// 		);

		// 		$this->db->insert('Bahan', $ingredients);
				
		// 		$error = $this->db->error();

		// 	    if ($error['code'] !== 0) {
			    	
		// 	        $status = "Failed";

		// 	        break;
		// 	    }
		// 	}

		// 	$this->CRUD_Activity->create($tipe, $detail, $status);

		// 	return $status;
		// }

		// public function update_Status()
		// {
		// 	$query_1 = $this->db->query("SELECT M.ID_Menu AS IDM, M.Nama, I.Sisa from Bahan inner join Menu AS M on Bahan.FO_ID_Menu = M.ID_Menu inner join Inventory AS I on Bahan.Prime_ID_Inventory = I.ID_Inventory where I.Sisa = 0")->result();

		// 	if ($query_1) {
				
		// 		foreach ($query_1 AS $row) {

		// 			$data = array(
					    
		// 			    'Status' => 'Unavailable'
		// 			);

		// 			$this->db->where('ID_Menu', $row->IDM);
		// 			$this->db->update('Menu', $data);
		// 		}
		// 	}

		// 	$query_2 = $this->db->query("SELECT M.ID_Menu AS ID, M.Nama, I.Sisa AS Sisa from Bahan inner join Menu AS M on Bahan.FO_ID_Menu = M.ID_Menu inner join Inventory AS I on Bahan.Prime_ID_Inventory = I.ID_Inventory where M.Status = "."'Unavailable'")->result();

		// 	if ($query_2) {
				
		// 		foreach ($query_2 AS $row) {

		// 			$data = $row->ID;
		// 			$up = array(
		// 				'Status' => 'Available'
		// 			);

		// 			$check = $this->db->query("SELECT ID, Sisa FROM (SELECT M.ID_Menu AS ID, M.Nama, I.Sisa AS Sisa from Bahan inner join Menu AS M on Bahan.FO_ID_Menu = M.ID_Menu inner join Inventory AS I on Bahan.Prime_ID_Inventory = I.ID_Inventory where M.Status = '"."Unavailable"."') AS sample where ID = '".$data."' AND Sisa = '".'0'."'")->result();

		// 			if (empty($check)) {
						
		// 				$this->db->where('ID_Menu', $data);
		// 				$this->db->update('Menu', $up);
		// 			}
		// 		}
		// 	}
		// }

		public function delete_Menu($id)
		{

			$status = "Success";
			$tipe = "Delete";
			$detail = "Menu";
			
			$this->db->delete('Menu', array('ID_Menu' => $id));

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

			return $status;
		}

		// public function delete_Ingredients($id)
		// {

		// 	$status = "Success";
		// 	$tipe = "Delete";
		// 	$detail = "Ingredient";
			
		// 	$this->db->delete('Bahan', array('ID_Bahan' => $id));

		// 	$error = $this->db->error();

		//     if ($error['code'] !== 0) {
		    	
		//         $status = "Failed";
		//     }

		// 	$this->CRUD_Activity->create($tipe, $detail, $status);

		// 	return $status;
		// }

		private function get_CurrentMonth()
		{
			$this->load->helper('date');

			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{
			$this->load->helper('date');

			$year = date("Y");

			return $year;
		}

		public function get_menu_id()
		{

			if (empty($this->db->query("SELECT ID_Menu FROM Menu ORDER BY ID_Menu DESC LIMIT 1")->row()->ID_Menu)) {
				
				return "MEN0000000";
			}

			return $this->db->query("SELECT ID_Menu FROM Menu ORDER BY ID_Menu DESC LIMIT 1")->row()->ID_Menu;
		}

		// public function check_ingredient_id()
		// {
			
		// 	return $this->db->get('Bahan')->num_rows();
		// }

		// public function get_ingredient_id()
		// {

		// 	if (empty($this->db->query("SELECT ID_Bahan FROM Bahan ORDER BY ID_Bahan DESC LIMIT 1")->row()->ID_Bahan)) {
				
		// 		return "BHN0000000";
		// 	}

		// 	return $this->db->query("SELECT ID_Bahan FROM Bahan ORDER BY ID_Bahan DESC LIMIT 1")->row()->ID_Bahan;
		// }

		public function get_total()
		{
			return $this->db->count_all_results('Menu');
		}

		public function get_best_seller()
		{
			
			$month = $this->get_CurrentMonth();
			$year = $this->get_CurrentYear();

			$this->db->select('M.ID_Menu, M.Nama, SUM(OI.Quantity) AS Quantity, SUM(OI.Subtotal) AS Subtotal');
			$this->db->group_by('M.ID_Menu');
			$this->db->order_by('Quantity', 'desc');
			$this->db->limit(1);
			$this->db->from('Transaksi_Jual AS TJ');
			$this->db->join('Order AS O', 'TJ.FO_ID_Order = O.ID_Order');
			$this->db->join('Order_Item AS OI', 'O.ID_Order = OI.FO_ID_Order');
			$this->db->join('Menu AS M ', 'OI.FO_ID_Menu = M.ID_Menu');

			$this->db->where('month(TJ.Tgl)', $month);
			$this->db->where('year(TJ.Tgl)', $year);

			return $this->db->get()->row_array();
		}

		public function get_available()
		{
			return $this->db->where('Status', 'Available')->get('Menu')->num_rows(); 
		}

		public function get_unavailable()
		{
			return $this->db->where('Status', 'Unavailable')->get('Menu')->num_rows();
		}	
	}