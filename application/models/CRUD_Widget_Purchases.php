<?php

	class CRUD_Widget_Purchases extends CI_Model
	{
		
		public function __construct()
		{
			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
		}

		private function get_CurrentMonth()
		{
			$this->load->helper('date');

			$month = date("m");

			return $month;
		}

		private function get_CurrentYear()
		{
			$this->load->helper('date');

			$year = date("Y");

			return $year;
		}

		public function read_monthly_purchases_dataset()
		{

			$query = $this->db->query('SELECT * FROM (SELECT TB.Tgl AS Tgl, DATE_FORMAT(TB.Tgl, "%b") AS Month_Spending, DATE_FORMAT(TB.Tgl, "%Y") AS Year_Spending, SUM(DT.Total) AS Spending FROM Transaksi_Beli AS TB INNER JOIN Detail_Transaksi AS DT ON TB.FO_ID_DT = DT.ID_DT GROUP BY DATE_FORMAT(TB.Tgl, "%b"), DATE_FORMAT(TB.Tgl, "%Y") ORDER BY TB.Tgl DESC LIMIT 12) AS Monthly_Income ORDER BY `Tgl` ASC');

			return $query->result_array();
		}

		public function get_today_spending()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('TB.Tgl = CURDATE()');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_yesterday_spending()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_last_spending()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -2 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_week_spending()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -6 DAY)');

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_today_transaction()
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('TB.Tgl = CURDATE()');

			return $this->db->get()->num_rows();
		}

		public function get_yesterday_transaction()
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -1 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_last_transaction()
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -2 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_week_transaction()
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('TB.Tgl = DATE_ADD(CURDATE(), INTERVAL -6 DAY)');

			return $this->db->get()->num_rows();
		}

		public function get_spending()
		{

			$this->db->select('SUM(DT.Total) AS Total');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->join('Detail_Transaksi AS DT', 'TB.FO_ID_DT = DT.ID_DT');
			$this->db->where('month(TB.Tgl)', $this->get_CurrentMonth());
			$this->db->where('year(TB.Tgl)', $this->get_CurrentYear());

			$query = $this->db->get();

			if (empty($query->row()->Total)) {
				
				return "0";
			}

			return $query->row()->Total;
		}

		public function get_transaction()
		{

			$this->db->select('TB.ID_TB');
			$this->db->from('Transaksi_Beli AS TB');
			$this->db->where('month(TB.Tgl)', $this->get_CurrentMonth());
			$this->db->where('year(TB.Tgl)', $this->get_CurrentYear());

			return $this->db->get()->num_rows();
		}

		public function get_total_item()
		{
			$query = $this->db->query('SELECT SUM(TBL.Total_Item) FROM Transaksi_Beli AS TB INNER JOIN TB_List AS TBL ON TB.FO_ID_TB_List = TBL.ID_TB_List WHERE MONTH(TB.Tgl) = MONTH(CURDATE())');
		}
	}
