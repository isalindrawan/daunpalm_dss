<?php

	class CRUD_Category extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
			$this->load->helper('array');
		}

		public function create($data)
		{

			$status = "Success";
			$tipe = "Create";
			$detail = "Category";

			$this->db->insert('Kategori', $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
		}

		public function update($data)
		{

			$status = "Success";
			$tipe = "Update";
			$detail = "Category";

			$this->db->where('ID_Kategori', element('ID_Kategori', $data));
			$this->db->update('Kategori', $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);

		}

		public function get_id()
		{

			if (empty($this->db->query("SELECT ID_Kategori FROM Kategori ORDER BY ID_Kategori DESC LIMIT 1")->row()->ID_Kategori)) {
				
				return "IDK0000000";
			}

			return $this->db->query("SELECT ID_Kategori FROM Kategori ORDER BY ID_Kategori DESC LIMIT 1")->row()->ID_Kategori;
		}

		public function get_total()
		{
			return $this->db->count_all_results('Kategori');
		}
	}