<?php

	class CRUD_Reservation extends CI_Model
	{

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
			$this->load->model('CRUD_Activity');
			
		}

		private function get_current_timestamp()
		{

			$this->load->helper('date');

			return date('Y-m-d H:i:s');
		}

		public function create($res)
		{
			$status = "Success";
			$tipe = "Create";
			$detail = "Reservation";

			$this->db->insert('Reservasi', $res);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
			return $status;
		}

		public function read($id)
		{
			
			return $this->db->query("SELECT * FROM Reservasi WHERE ID_Reservasi ='".$id."'")->row_array();
		}

		public function read_all()
		{
			$this->db->from('Reservasi');
			$this->db->limit(10);

			return $this->db->get()->result_array();
		}

		public function update($data, $where)
		{
			$status = "Success";
			$tipe = "Update";
			$detail = "Reservation";

			$this->db->where($where);
			$this->db->update("Reservasi", $data);

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
			return $status;
		}

		public function auto_update()
		{
			$this->db->where('Tgl_Kunjungan < ', $this->get_current_timestamp());
			$this->db->where('Status', 'Upcoming');
			$this->db->set('Status', 'Finished');
			$this->db->update('Reservasi');
		}

		public function delete($id_res)
		{
			$status = "Success";
			$tipe = "Delete";
			$detail = "Reservation";
			
			$this->db->delete('Reservasi', array('ID_Reservasi' => $id_res));

			$error = $this->db->error();

		    if ($error['code'] !== 0) {
		    	
		        $status = "Failed";
		    }

			$this->CRUD_Activity->create($tipe, $detail, $status);
			return $status;
		}

		public function get_id_res()
		{

			if (empty($this->db->query("SELECT ID_Reservasi FROM Reservasi ORDER BY ID_Reservasi DESC LIMIT 1")->row()->ID_Reservasi)) {
				return "RES0000000";
			}

			return $this->db->query("SELECT ID_Reservasi FROM Reservasi ORDER BY ID_Reservasi DESC LIMIT 1")->row()->ID_Reservasi;
		}

		public function get_done()
		{
			
			$this->db->select('ID_Reservasi');
			$this->db->from('Reservasi');
			$this->db->where('Status', 'Finished');

			$query = $this->db->get();

			if (empty($query->num_rows())) {
				
				return "0";
			}

			return $query->num_rows();
		}

		public function get_pending()
		{
			
			$this->db->select('ID_Reservasi');
			$this->db->from('Reservasi');
			$this->db->where('Status', 'Upcoming');

			$query = $this->db->get();

			if (empty($query->num_rows())) {
				
				return "0";
			}

			return $query->num_rows();
		}

		public function get_cancel()
		{
			
			$this->db->select('ID_Reservasi');
			$this->db->from('Reservasi');
			$this->db->where('Status', 'Cancelled');

			$query = $this->db->get();

			if (empty($query->num_rows())) {
				
				return "0";
			}

			return $query->num_rows();
		}
	}