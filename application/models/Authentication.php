<?php 

	class Authentication extends CI_Model
	{
		
		function __construct()
		{
			parent::__construct();
			
			$this->load->database();
		}

		public function check_login($username, $password)
		{

			$this->db->select('A.*, P.Posisi, P.Status AS Stat');
	        $this->db->from('Account AS A');
	        $this->db->join('Pegawai AS P', 'A.FO_ID_Peg = P.ID_Peg');
	        $this->db->where('Username', $username);
	        $this->db->where('Password', $password);
	        $this->db->limit(1);

	        $query = $this->db->get();

	        if ($query->num_rows() == 0) {

	            return 0;

	        } else {

	            return $query->row_array();
	        }
		}

		public function check_recovery($username, $password)
		{

			$this->db->select('A.*, A.Security AS Security, P.Posisi, P.Status AS Stat');
	        $this->db->from('Account AS A');
	        $this->db->join('Pegawai AS P', 'A.FO_ID_Peg = P.ID_Peg');
	        $this->db->where('Username', $username);
	        $this->db->where('Recovery', $password);
	        $this->db->limit(1);

	        $query = $this->db->get();

	        if ($query->num_rows() == 0) {

	            return 0;

	        } else {

	            return $query->row_array();
	        }
		}

		public function check_security($username, $answer)
		{

			$this->db->select('A.*, P.Posisi, P.Status AS Stat');
	        $this->db->from('Account AS A');
	        $this->db->join('Pegawai AS P', 'A.FO_ID_Peg = P.ID_Peg');
	        $this->db->where('Username', $username);
			$this->db->where('Answer', $answer);
	        $this->db->limit(1);

	        $query = $this->db->get();

	        if ($query->num_rows() == 0) {

	            return 0;

	        } else {

	            return $query->row_array();
	        }
		}

		public function update_password($username, $password) {

			$this->db->set('Password', $password);
			$this->db->where('Username', $username);
			$this->db->update('Account');

		}

		public function get_user_details($username)
		{

			$this->db->select('A.*, A.Security AS Security, P.Posisi, P.Status AS Stat');
	        $this->db->from('Account AS A');
	        $this->db->join('Pegawai AS P', 'A.FO_ID_Peg = P.ID_Peg');
	        $this->db->where('Username', $username);
	        $this->db->limit(1);

	        $query = $this->db->get();

	        if ($query->num_rows() == 0) {

	            return 0;

	        } else {

	            return $query->row_array();
	        }
		}
	}