<?php

	class DT_Category extends CI_Model
	{

		var $column_order = array('K.ID_Kategori', 'K.Nama', 'Total');

		var $column_search = array('K.ID_Kategori', 'K.Nama');

		var $order = array('ID_Kategori' => 'asc');
		var $table = 'Kategori';

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
		}

		private function get_datatables_query()
		{

			$this->db->select('K.ID_Kategori, K.Nama, COUNT(M.FO_ID_Kategori) AS Total');
			$this->db->from($this->table.' AS K');
			$this->db->join('Menu AS M', 'K.ID_Kategori = M.FO_ID_Kategori', 'left');
			$this->db->group_by('K.ID_Kategori');
			
			$i = 0;

			foreach($this->column_search AS $item) {

	            if($_POST['search']['value'])
	            {

	                if($i===0)
	                {

	                    $this->db->group_start();
	                    $this->db->like($item, $_POST['search']['value']);

	                }

	                else
	                {

	                    $this->db->or_like($item, $_POST['search']['value']);
	                }

	            	if(count($this->column_search) - 1 == $i)
	                    $this->db->group_end();
	            }

	            $i++;
	        }

	        if(isset($_POST['order']))
	        {

	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

	        }

	        else if(isset($this->order))
	        {

	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
		}

		public function get_datatables()
		{

	        $this->get_datatables_query();
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }

	    public function count_filtered()
	    {

	        $this->get_datatables_query();
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all()
	    {

	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
	}
