<?php

	class DT_Employee extends CI_Model
	{

		var $column_order = array('P.ID_Peg', 'N.Nama_Depan', 'P.Tlp', 'P.Posisi', 'P.Gaji', 'P.Masuk', 'P.Keluar', 'P.Status');

		var $column_search = array('P.ID_Peg', 'N.Nama_Depan', 'P.Tlp', 'P.Posisi', 'P.Gaji', 'P.Masuk', 'P.Keluar', 'P.Status');

		var $order = array('P.ID_Peg' => 'asc');
		var $table = 'Pegawai';

		public function __construct()
		{

			parent::__construct();
			$this->load->database();
		}

		private function get_datatables_query()
		{

			$this->db->select('P.*, N.*, A.*');
			$this->db->from($this->table.' AS P');
			$this->db->join('Nama AS N', 'P.FO_ID_Nama = N.ID_Nama');
			$this->db->join('Alamat AS A', 'P.FO_ID_Alamat = A.ID_Alamat');

			$i = 0;

			foreach($this->column_search AS $item) {

	            if($_POST['search']['value'])
	            {
	                 
	                if($i===0)
	                {

	                    $this->db->group_start();
	                    $this->db->like($item, $_POST['search']['value']);
	                
	                }

	                else
	                {

	                    $this->db->or_like($item, $_POST['search']['value']);
	                }
	 
	            	if(count($this->column_search) - 1 == $i)
	                    $this->db->group_end();
	            }
	            
	            $i++;
	        }
	         
	        if(isset($_POST['order']))
	        {

	            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

	        }

	        else if(isset($this->order))
	        {

	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
		}

		public function get_datatables()
		{

	        $this->get_datatables_query();
	        if($_POST['length'] != -1)
	        $this->db->limit($_POST['length'], $_POST['start']);
	        $query = $this->db->get();
	        return $query->result();
	    }
	 
	    public function count_filtered()
	    {

	        $this->get_datatables_query();
	        $query = $this->db->get();
	        return $query->num_rows();
	    }
	 
	    public function count_all()
	    {

	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
	}