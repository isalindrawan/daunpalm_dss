$(document).ready(function() {

    $(window).keydown(function(event) {

        if (event.keyCode == 13) {

            event.preventDefault();

            add();
            reset();

            return false;
        }
    });

    $('#list').on('click', '.delete', function() {

        $(this).closest('tr').remove();
    });

    $('#clear-button').on('click', function() {

        $('#list tbody tr').remove();
        $('#submit-button').prop('disabled', true);
    });

    $("#list").bind("DOMSubtreeModified", function() {

        $('#submit-button').prop('disabled', false);
    });
});

function reset() {

    document.getElementById("item").value = "";
    document.getElementById("item").placeholder = "Item";
    document.getElementById("qty").value = "";
    document.getElementById("qty").placeholder = "Quantity";
}

function add() {

    var item = document.getElementById('item').value;
    var quantity = document.getElementById('qty').value;

    if (!item || !quantity) {

        swal({
            title: "",
            text: "Please input required field.",
            type: "warning"
        });

    } else if(isNaN(quantity)) {

        swal({
            title: "",
            text: "Invalid input on quantity field, only number allowed.",
            type: "warning"
        });
    
    } else {

        $('#list tbody').append(

            '<tr><td class="align-center"><a class="btn btn-xs bg-pink waves-effect delete"><i class="material-icons">clear</i></a></td><td><input type="hidden" name="create_item[]" value="' + item + '">' + item + '</td><td><input type="hidden" name="create_quantity[]" value="' + quantity + '">' + quantity + '</td></tr>'
        );

        reset();
    }

}