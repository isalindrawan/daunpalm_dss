$(function() {

    $.ajax({

        url: "http://localhost/daunpalm_dss/datatable_api/transaction_api/get_json_data_dashboard_chart",
        method: "GET",
        success: function(data) {

            var myLabel = [];
            var myValue = [];
            var myValue_2 = [];

            var jsonData = JSON.parse(data);

            for (i in jsonData) {

                myLabel.push(jsonData[i].Month + ", " + jsonData[i].Year);
                myValue.push(jsonData[i].Income);
                myValue_2.push(jsonData[i].Spending);
            }

            var chartdata = {
                labels: myLabel,
                datasets: [{
                    label: 'Income',
                    data: myValue,
                    borderColor: 'rgba(0, 188, 212, 0.75)',
                    backgroundColor: 'rgba(0, 188, 212, 0.8)',
                    pointBorderColor: 'rgba(0, 188, 212, 0)',
                    pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                    pointBorderWidth: 1
                },{
                    label: 'Spending',
                    data: myValue_2,
                    borderColor: 'rgba(233,30,99, 0.75)',
                    backgroundColor: 'rgba(233,30,99, 0.8)',
                    pointBorderColor: 'rgba(233,30,99, 0)',
                    pointBackgroundColor: 'rgba(233,30,99, 0.8)',
                    pointBorderWidth: 1
                }]
            };

            new Chart(document.getElementById("dashboard_chart").getContext("2d"), {

                type: 'bar',
                data: chartdata,
                options: {
                    responsive: true,
                    legend: true,
                    hover: {
                        mode: 'dataset'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepValue: 100000
                            }
                        }]
                    },
                    title: {
                        display: false,
                        text: 'MONTHLY INCOME'
                    }
                }
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});