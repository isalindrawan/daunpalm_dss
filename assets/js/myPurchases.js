$(document).ready(function() {

    $(window).keydown(function(event) {

        if (event.keyCode == 13) {

            event.preventDefault();

            add();
            reset_item();

            return false;
        }
    });

    $('#clear-button').on('click', function() {

        $('#purchases-list tbody tr').remove();
        $('#submit-button').prop('disabled', true);

        reset();
    });

    $("#purchases-list").bind("DOMSubtreeModified", function() {

        $('#submit-button').prop('disabled', false);
    });

    $('#purchases-list').on('click', '.delete', function() {

        $(this).closest('tr').remove();

        count_total();

        reset();
    });

    $("#paid").change(function() {

        var paid = parseInt(document.getElementById("paid").value);
        var total = parseInt(document.getElementById("total").value);

        var change = paid - total;

        if (change < 0) {

            swal({
                title: "",
                text: "Please enter correct payment.",
                type: "warning"
            });

            reset();

        } else {

            if (!isNaN(change)) {

                document.getElementById("paid").value = paid.toString();
                document.getElementById("change").value = change.toString();

            } else {

                reset();
            }
        }
    });
});

function reset() {

    document.getElementById("paid").value = "";
    document.getElementById("paid").placeholder = "0";
    document.getElementById("change").value = "";
    document.getElementById("change").value = "0";
    document.getElementById("item").placeholder = "";
    document.getElementById("item").value = "";
    document.getElementById("quantity").placeholder = "";
    document.getElementById("quantity").value = "";
    document.getElementById("price").placeholder = "";
    document.getElementById("price").value = "";

}

function reset_item() {

    document.getElementById("item").placeholder = "";
    document.getElementById("item").value = "";
    document.getElementById("quantity").placeholder = "";
    document.getElementById("quantity").value = "";
    document.getElementById("price").placeholder = "";
    document.getElementById("price").value = "";

}

function add() {

    var item = document.getElementById('item').value;
    var price = parseInt(document.getElementById('price').value);
    var quantity = parseInt(document.getElementById('quantity').value);
    var subtotal = price * quantity;

    if (isNaN(price) || isNaN(quantity)) {

        swal({
            title: "",
            text: "Please input required field.",
            type: "warning"
        });

    } else {

        $('#purchases-list').append(

            '<tr><td><a class="myButton btn btn-sm bg-pink waves-effect delete">X</a></td><td><input type="hidden" name="item_list[]" value="' + item + '"><h5>' + item + '</h5></td><td><input type="hidden" name="item_price[]" value="' + price + '"><h5>' + price + '</h5></td><td><input type="hidden" name="item_quantity[]" value="' + quantity + '"><h5>' + quantity + '</h5></td><td><input type="hidden" name="item_subtotal[] placeholder="' + subtotal + '" value="' + subtotal + '"><h5>' + subtotal + '</h5></td></tr>'
        );

        count_total();
        reset_item();
    }

}

function count_total() {

    var table = document.getElementById("purchases-list");
    var total = 0;

    for (var i = 1; i < table.rows.length; i++) {

        var temp = $(table.rows[i].cells[4].innerHTML);

        total = total + parseInt(temp.attr("value"));
    }

    document.getElementById("total").placeholder = total.toString();
    document.getElementById("total").value = total.toString();
}