$(function() {

    $.ajax({
        url: "http://localhost/daunpalm_dss/datatable_api/sold_api/get_chart_data",
        method: "GET",
        success: function(data) {

            var myLabel = [];
            var myValue = [];

            var jsonData = JSON.parse(data);

            for (i in jsonData) {

                // myLabel = jsonData.Name[i];

                myLabel.push(jsonData[i].Nama);
                myValue.push(jsonData[i].Quantity);
            }

            var chartdata = {
                labels: myLabel,
                datasets: [{
                    label: 'Sold',
                    backgroundColor: 'rgba(33, 150, 243, 0.75)',
                    borderColor: 'rgba(33, 150, 243, 1)',
                    hoverBackgroundColor: 'rgba(33, 150, 243, 1)',
                    hoverBorderColor: 'rgba(33, 150, 243, 1)',
                    data: myValue
                }]
            };

            new Chart(document.getElementById("all_chart").getContext("2d"), {

                type: 'horizontalBar',
                data: chartdata,
                options: {
                    responsive: true,
                    legend: false,
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepValue: 1
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'TOP 10 MENU SOLD ALL TIME'
                    }
                }
            });

        },
        error: function(data) {
            console.log(data);
        }
    });

    $.ajax({
        url: "http://localhost/daunpalm_dss/datatable_api/sold_api/get_daily_chart_data",
        method: "GET",
        success: function(data) {

            var myLabel = [];
            var myValue = [];

            var jsonData = JSON.parse(data);

            for (i in jsonData) {

                // myLabel = jsonData.Name[i];

                myLabel.push(jsonData[i].Nama);
                myValue.push(jsonData[i].Quantity);
            }

            var chartdata = {
                labels: myLabel,
                datasets: [{
                    label: 'Sold',
                    backgroundColor: 'rgba(96, 125, 139, 0.75)',
                    borderColor: 'rgba(96, 125, 139, 1)',
                    hoverBackgroundColor: 'rgba(96, 125, 139, 1)',
                    hoverBorderColor: 'rgba(96, 125, 139, 1)',
                    data: myValue
                }]
            };

            new Chart(document.getElementById("daily_chart").getContext("2d"), {

                type: 'horizontalBar',
                data: chartdata,
                options: {
                    responsive: true,
                    legend: false,
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepValue: 1
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'TOP 10 MENU SOLD TODAY'
                    }
                }
            });

        },
        error: function(data) {
            console.log(data);
        }
    });

    $.ajax({
        url: "http://localhost/daunpalm_dss/datatable_api/sold_api/get_monthly_chart_data",
        method: "GET",
        success: function(data) {

            var myLabel = [];
            var myValue = [];

            var jsonData = JSON.parse(data);

            for (i in jsonData) {

                // myLabel = jsonData.Name[i];

                myLabel.push(jsonData[i].Nama);
                myValue.push(jsonData[i].Quantity);
            }

            var chartdata = {
                labels: myLabel,
                datasets: [{
                    label: 'Sold',
                    backgroundColor: 'rgba(33, 150, 243, 0.75)',
                    borderColor: 'rgba(33, 150, 243, 1)',
                    hoverBackgroundColor: 'rgba(33, 150, 243, 1)',
                    hoverBorderColor: 'rgba(33, 150, 243, 1)',
                    data: myValue
                }]
            };

            new Chart(document.getElementById("monthly_chart").getContext("2d"), {

                type: 'horizontalBar',
                data: chartdata,
                options: {
                    responsive: true,
                    legend: false,
                    hover: {
                        mode: 'label'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepValue: 1
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: 'TOP 10 MENU SOLD THIS MONTH'
                    }
                }
            });

        },
        error: function(data) {
            console.log(data);
        }
    });
});