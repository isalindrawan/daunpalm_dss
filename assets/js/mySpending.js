$(function() {

    $.ajax({

        url: "http://localhost/daunpalm_dss/datatable_api/transaction_api/get_json_data_monthly_chart_spending",
        method: "GET",
        success: function(data) {

            var myLabel = [];
            var myValue = [];

            var jsonData = JSON.parse(data);

            for (i in jsonData) {

                myLabel.push(jsonData[i].Month_Spending);
                myValue.push(jsonData[i].Spending);
            }

            var chartdata = {
                labels: myLabel,
                datasets: [{
                    label: 'Spending',
                    data: myValue,
                    borderColor: 'rgba(0, 188, 212, 0.75)',
                    backgroundColor: 'rgba(0, 188, 212, 0.3)',
                    pointBorderColor: 'rgba(0, 188, 212, 0)',
                    pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
                    pointBorderWidth: 1
                }]
            };

            new Chart(document.getElementById("monthly_chart").getContext("2d"), {

                type: 'line',
                data: chartdata,
                options: {
                    responsive: true,
                    legend: true,
                    hover: {
                        mode: 'dataset'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            ticks: {
                                beginAtZero: true,
                                steps: 1,
                                stepValue: 100000
                            }
                        }]
                    },
                    title: {
                        display: false,
                        text: 'MONTHLY SPENDING'
                    }
                }
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});