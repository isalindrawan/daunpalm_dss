$(document).ready(function() {

    $('#order-list').on('click', '.delete', function() {

        $(this).closest('tr').remove();

        count_total();

        reset();
    });

    $("#paid").change(function() {

        var paid = parseInt(document.getElementById("paid").value);
        var total = parseInt(document.getElementById("total").value);

        var change = paid - total;

        if (change < 0) {

            swal({
                title: "",
                text: "Please enter correct payment.",
                type: "warning"
            });

            reset();

        } else {

            if (!isNaN(change)) {

                document.getElementById("paid").value = paid.toString();
                document.getElementById("change").value = change.toString();
            
            } else {

                reset();
            }
        }
    });
});

function updateSub(id, price) {

    var qty = parseInt(document.getElementById('qty' + id).value);
    var temp = parseInt(price);

    var updated = qty * temp;

    document.getElementById('sub' + id).setAttribute("value", updated.toString());
    document.getElementById('qty' + id).setAttribute("value", qty.toString());
    count_total();
    reset();
}

function reset() {

    document.getElementById("paid").value = "";
    document.getElementById("paid").placeholder = "0";
    document.getElementById("change").value = "";
    document.getElementById("change").value = "0";
}

function add(id, name, price) {

    if (document.getElementById("order-list").rows.namedItem(id)) {

        var qty = parseInt(document.getElementById('qty' + id).value);
        var sub = parseInt(document.getElementById('sub' + id).value);
        
        var price = sub / qty;

        var new_price = sub + price;
        var new_qty = qty + 1;

        document.getElementById('sub' + id).setAttribute("value", new_price.toString());
        document.getElementById('qty' + id).setAttribute("value", new_qty.toString());

        count_total();
        reset();

    } else {

        $('#order-list').append('<tr id="' + id + '"><td><input class="align-center" type="hidden" name="id[]" value="' + id + '"><a class="myButton btn bg-pink waves-effect delete">X</a></td><td><input class="align-center" type="hidden" name="name[]" value="' + name + '"><h5 class="align-left">' + name + '</h5></td><td><input id="qty' + id + '" onchange="updateSub(\'' + id + '\', \'' + price + '\')" class="align-center" type="text" name="quantity[]" placeholder="0" value="1"></td><td class="counter"><input id="sub' + id + '" class="align-center" type="text" name="subtotal[]" placeholder="' + price + '" value="' + price + '" readonly></td></tr>');
        count_total();
        reset();
    }
}

function count_total() {

    var table = document.getElementById("order-list");
    var total = 0;

    for (var i = 1; i < table.rows.length; i++) {

        var test = $(table.rows[i].cells[3].innerHTML);

        total = total + parseInt(test.attr("value"));
    }

    document.getElementById("total").placeholder = total.toString();
    document.getElementById("total").value = total.toString();
}